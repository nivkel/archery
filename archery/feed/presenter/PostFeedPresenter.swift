//
//  PostFeedPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 10/15/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostFeedPresenter: ViewToPresenterPostFeedProtocol {
    var view: PresenterToViewPostFeedProtocol?
    
    var interactor: PresenterToInteractorPostFeedProtocol?
    
    var router: PresenterToRouterPostFeedProtocol?
    
    func startFetchPosts(limit: Int?, nextToken: String?) {
        interactor?.fetchPosts(limit: limit, nextToken: nextToken)
    }
    
    func startSearchPosts(filters: [ModelPostFilterInput], limit: Int?, nextToken: String?) {
        interactor?.searchPosts(filters: filters, limit: limit, nextToken: nextToken)
    }
    
    func showPostDetailController(navigationController: UINavigationController, cell: PostFeedCell, post: Post) {
        router?.pushPostDetailController(navigationController: navigationController, cell: cell, post: post)
    }
}

extension PostFeedPresenter: InteractorToPresenterPostFeedProtocol {
    func postFeedFetchSuccess(response: [Post?], nextToken: String?) {
        view?.postFeed(response: response, nextToken: nextToken)
    }
    
    func postFeedFetchFailed() {
        view?.showError()
    }
    
    func postFeedSearchSuccess(response: [Post], nextToken: String?) {
        view?.postFeedSearch(response: response, nextToken: nextToken)
    }
}
