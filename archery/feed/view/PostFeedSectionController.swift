//
//  PostFeedSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 10/13/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import AWSAppSync

class PostFeedSectionController: ListSectionController {
    
    var post: Post!
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard var width = self.collectionContext?.containerSize.width else {
            return CGSize(width: 0, height: 100)
        }
        width = floor(width / 2)
        return CGSize(width: width, height: 260.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: PostFeedCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! PostFeedCell
        cell.titleLabel.text = post.title
        if let price = post.price {
            cell.priceLabel.isHidden = false
            cell.priceLabel.text = "$\(price)"
            if let salesPrice = post.salePrice {
                cell.priceLabel.text = "Sale $\(salesPrice)"
            }
//            if price.count <= 3 {
//                cell.priceLabel.frame.size.width = 80
//            } else {
//                cell.priceLabel.frame.size.width = 120
//            }
        } else {
            cell.priceLabel.isHidden = true
            cell.priceLabel.text = "$ -"
        }
        cell.priceLabel.clipsToBounds = true
        cell.priceLabel.layer.cornerRadius = 8.0
        cell.layer.borderWidth = 0.4
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowRadius = 0.2
        debugPrint("Post file:\(post.file?.key)")

        if let file2 = post.file2 {
            debugPrint("file2:\(file2)")
        }
        guard let file = post.file else {
            return cell
        }
        let s3Object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region)
        cell.setup(s3Object: s3Object)
        return cell
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        self.post = object as? Post
    }
    
    override func didSelectItem(at index: Int) {
        guard let postFeedViewController = self.viewController as? PostFeedViewController else {
            return
        }
       let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as! PostFeedCell
        
        debugPrint("cell for item:\(cell.titleLabel.text)")

        postFeedViewController.presenter?.showPostDetailController(navigationController: postFeedViewController.navigationController!, cell: cell, post: self.post)
        
        AWSPinpointManager.shared.logEvent(eventType: .postOpen, attributeValue: post.id, attributeKey: .postId, metric: NSNumber(value: 1))
    }
}
