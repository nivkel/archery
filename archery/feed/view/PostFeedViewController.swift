//
//  PostFeedViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import PromiseKit
import AWSCognitoIdentityProvider
import AWSUserPoolsSignIn
import AWSMobileClient
import RxSwift
import RxCocoa
//import Firebase
import SwiftMessages
import SkeletonView
import RevealingSplashView

class PostFeedViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: ViewToPresenterPostFeedProtocol?
    
    var posts = [Any]()
    var nextToken: String?
    
    var user: AWSCognitoIdentityUser?
    var pool: AWSCognitoIdentityUserPool?
    var identityManager: AWSIdentityProviderManager!
    
    var signInVC: SignInViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = Common.backgroundColor
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(PostFeedViewController.handleRefresh), for: .valueChanged)
        return refreshControl
    }()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()
    
    let disposeBag = DisposeBag()
    
    let searchController = UISearchController(searchResultsController: nil)
    
//    //// Firebase
//    /// The ad unit ID from the AdMob UI.
//    let adUnitID = "ca-app-pub-3940256099942544/8407707713"
//    /// The number of native ads to load (between 1 and 5 for this example).
//    let numAdsToLoad = 5
//    /// The native ads.
//    var nativeAds = [GADUnifiedNativeAd]()
//    /// The ad loader that loads the native ads.
//    var adLoader: GADAdLoader!
//    ////

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Airya"
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchBar.barStyle = .blackTranslucent
        searchController.searchBar.tintColor = Common.primaryColor
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Airya"
        searchController.searchBar.delegate = self
        
        searchController.searchBar.rx.text.orEmpty
            .debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter({ !$0.isEmpty })
            .subscribe(onNext: { [unowned self] query in
                debugPrint("searching query...:\(query.lowercased())")
//                var filters = [ModelPostFilterInput]()
                let filterQuery = ModelStringFilterInput(contains: query.lowercased())
                let postFilterSearchInput = ModelPostFilterInput(searchField: filterQuery)
//                let filterCategory = ModelStringFilterInput(contains: "Long Bow")
//                let postFilterCategoryInput = ModelPostFilterInput(category: filterCategory)
                
                self.presenter?.startSearchPosts(filters: [postFilterSearchInput], limit: 20, nextToken: nil)
            }).disposed(by: self.disposeBag)
        
        let layout = ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
//        layout.minimumLineSpacing = 2.0
//        layout.minimumInteritemSpacing = 2.0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
        
        self.collectionView.addSubview(self.refreshControl)
        
        presenter?.startFetchPosts(limit: 10, nextToken: nil)
        
//        let options = GADMultipleAdsAdLoaderOptions()
//        options.numberOfAds = numAdsToLoad
//
//        // Prepare the ad loader and start loading ads.
//        adLoader = GADAdLoader(adUnitID: adUnitID,
//                               rootViewController: self,
//                               adTypes: [.unifiedNative],
//                               options: [options])
//        adLoader.load(GADRequest())
        
        handleSplash()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let image = UIImage(icon: .FAList, size: CGSize(width: 30.0, height: 30.0))
        let menuButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(PostFeedViewController.showFilterMenu(sender:)))
        menuButton.tintColor = Common.secondaryColor
        self.navigationItem.setRightBarButton(menuButton, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PushNotifications.registerForPushNotifications()
        
        debugPrint("PostFeed isSignedIn :\(AWSMobileClient.sharedInstance().isSignedIn)")
//        if !AWSMobileClient.sharedInstance().isSignedIn {
//            AWSMobileClient.sharedInstance().showSignIn(navigationController: self.navigationController!) { (userState, error) in
//                debugPrint("showSignIn userState:\(userState), error:\(error)")
//            }
//        }
    }
    
    func handleSplash() {
        //Initialize a revealing Splash with with the iconImage, the initial size and the background color
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "splash")!,iconInitialSize: CGSize(width: 70, height: 70), backgroundColor: UIColor(red:0.11, green:0.56, blue:0.95, alpha:1.0))
        revealingSplashView.duration = 3.0
        revealingSplashView.animationType = SplashAnimationType.woobleAndZoomOut
//        revealingSplashView.useCustomIconColor = true
//        revealingSplashView.iconColor = UIColor.red
        //Adds the revealing splash view as a sub view
        let window = UIApplication.shared.keyWindow
        window?.addSubview(revealingSplashView)
//        self.view.addSubview(revealingSplashView)
        
        //Starts animation
        revealingSplashView.startAnimation(){
            print("Completed")
        }
    }
    
    @objc func handleRefresh() {
        debugPrint("Refresh")
        
        self.posts.removeAll()
        presenter?.startFetchPosts(limit: 10, nextToken: nil)
    }
    
    @objc func showFilterMenu(sender: Any) {
//        Common().showMenu(sender, delegate: self, type: .feed, layout: .topCard)
        let selectCategoryViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        selectCategoryViewController.postPageCategoryDelegate = self
        let navigationController = UINavigationController(rootViewController: selectCategoryViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
}

extension PostFeedViewController: PostPageCategoryDelegate {
    func setCategory(value: String) {
        if value == "Clear" {
            self.handleRefresh()
        }
        let filterCategory = ModelStringFilterInput(contains: value)
        let postFilterCategoryInput = ModelPostFilterInput(category: filterCategory)
        self.presenter?.startSearchPosts(filters: [postFilterCategoryInput], limit: 20, nextToken: nil)
    }
}

extension PostFeedViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension PostFeedViewController: PresenterToViewPostFeedProtocol {
    func postFeed(response: [Post?], nextToken: String?) {
//        let posts = response as! [Post]
        debugPrint("Response posts:\(response.count)")
        for post in response {
            debugPrint("response post:\(post?.title)")
        }
        let posts = response.compactMap { $0 }
        if nextToken != nil {
            self.posts = self.posts + posts
        }
        debugPrint("nextToken isEmpty:\(nextToken?.isEmpty)")
        self.nextToken = nextToken
        
        self.adapter.performUpdates(animated: true, completion: { (finished) in
            
        })
        self.refreshControl.endRefreshing()
    }
    
    func showError() {
        debugPrint("Fetch error")
    }
    
    func postFeedSearch(response: [Post], nextToken: String?) {
        debugPrint("Response posts:\(response.count)")
        self.posts = response
        self.nextToken = nextToken
        
        self.adapter.performUpdates(animated: true, completion: { (finished) in
            
        })
        self.refreshControl.endRefreshing()
    }
}

extension PostFeedViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {
        debugPrint("willDisplay index:\(index)")
        
        if self.posts.count - 1 == index {
            if let nextToken = self.nextToken {
                debugPrint("willDisplay index:\(index) pagination token:")
                self.presenter?.startFetchPosts(limit: 10, nextToken: nextToken)
            }
        }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension PostFeedViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.posts as! [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        default:
            return PostFeedSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let customView = SkeletonView(frame: self.collectionView.frame)
//        customView.loadingIndicator.startAnimating()
//        customView.isSkeletonable = true
//        customView.showAnimatedGradientSkeleton()
        
        return customView
    }
}

extension PostFeedViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("selectedScopeButtonIndexDidChange")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("search cancelled")
        self.handleRefresh()
    }
}

//extension GADUnifiedNativeAd: ListDiffable {
//    public func diffIdentifier() -> NSObjectProtocol {
//        return advertiser! as NSObjectProtocol
//    }
//
//    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
//        if let object = object as? GADUnifiedNativeAd {
//            return headline == object.headline
//        }
//        return false
//    }
//}

//extension PostFeedViewController: GADUnifiedNativeAdLoaderDelegate {
//
//    func addNativeAds() {
//        if nativeAds.count <= 0 {
//            return
//        }
//
//        let adInterval = (posts.count / nativeAds.count) + 1
//        var index = 0
//        for nativeAd in nativeAds {
//            if index < posts.count {
//                posts.insert(nativeAd, at: index)
//                index += adInterval
//            } else {
//                break
//            }
//        }
//    }
//
//    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
//        print("\(adLoader) failed with error: \(error.localizedDescription)")
//    }
//
//    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
//        print("Received native ad: \(nativeAd)")
//
//        // Add the native ad to the list of native ads.
//        nativeAds.append(nativeAd)
//    }
//
//    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
//        addNativeAds()
//    }
//}
