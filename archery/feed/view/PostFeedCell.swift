//
//  PostFeedCell.swift
//  archery
//
//  Created by Kelvin Lee on 10/14/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher
import AWSAppSync

class PostFeedCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.layer.cornerRadius = 6.0
        self.imageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
    }
    
    func setup(s3Object: AWSS3Object) {
        guard let key = s3Object.key else {
            return
        }
        let width = Int(self.imageView.frame.size.width) * 2
        let height = Int(self.imageView.frame.size.height) * 2
        debugPrint("imageView size height:\(height), width:\(width)")
        let fitInResolution = "\(width)x\(height)/"
        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
        debugPrint("s3 urlString:\(urlString)")
        guard let url = URL(string: urlString) else {
            return
        }
        debugPrint("s3 url:\(url)")
        let imageResource = ImageResource(downloadURL: url)
        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: .lightGray, backgroundColor: Common.backgroundColor)
        self.imageView.kf.indicatorType = .activity
        self.imageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
            debugPrint("Progress received:\(received), total:\(total)")
        }) { (result) in
            switch result {
            case .success(let value):
                // The image was set to image view:
                print(value.image)
                self.imageView.image = value.image
                // From where the image was retrieved:
                // - .none - Just downloaded.
                // - .memory - Got from memory cache.
                // - .disk - Got from disk cache.
                print(value.cacheType)
                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                // The source object which contains information like `url`.
                print(value.source)
            case .failure(let error):
                print("error \(error)") // The error happens
            }
        }
    }
    
    func setupWithAWS(s3Object: AWSS3Object) {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: AWSLoginProvider.shared.identityPoolId, identityProviderManager: AWSLoginProvider.shared.pool)
        debugPrint("credentialsProvider identityId: \(credentialsProvider.identityId)")
        AWSLoginProvider.shared.getIdentityId { (identityId) in
            let url = URL(fileURLWithPath: "private/\(identityId)/\(s3Object.getKeyName())")
            AWSS3Manager.shared.downloadCompletionHandler = { (task, URL, data, error) -> Void in
                // Do something e.g. Alert a user for transfer completion.
                // On failed downloads, `error` contains the error object.
                debugPrint("Download transfer complete:\(task.progress), error:\(error)")
                guard let imageData = data else {
                    return
                }
                guard let image = UIImage(data: imageData) else {
                    return
                }
                DispatchQueue.main.async {
                    self.imageView.image = image
                }
            }
            AWSS3Manager.shared.download(s3Object: s3Object, toURL: url) { (complete, error) in
                debugPrint("AWSS3Manager download:\(complete)")
            }
        }
    //        AWSS3Manager.shared.getIdentityIdFromCredentialProvider { (credential, error) in
    //            guard let credential = credential else {
    //                return
    //            }
    //            debugPrint("Credential :\(credential), error:\(error)")
    //            let url = URL(fileURLWithPath: "private/\(credential)/\(s3Object.getKeyName())")
    //            AWSS3Manager.shared.downloadCompletionHandler = { (task, URL, data, error) -> Void in
    //                // Do something e.g. Alert a user for transfer completion.
    //                // On failed downloads, `error` contains the error object.
    //                debugPrint("Download transfer complete:\(task.progress), error:\(error)")
    //                guard let imageData = data else {
    //                    return
    //                }
    //                guard let image = UIImage(data: imageData) else {
    //                    return
    //                }
    //                DispatchQueue.main.async {
    //                    cell.imageView.image = image
    //                }
    //            }
    //            AWSS3Manager.shared.download(s3Object: s3Object, toURL: url) { (complete, error) in
    //                debugPrint("AWSS3Manager download:\(complete)")
    //            }
    //        }
    }
}
