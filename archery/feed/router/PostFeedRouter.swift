//
//  PostFeedRouter.swift
//  archery
//
//  Created by Kelvin Lee on 10/15/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftMessages

class PostFeedRouter: PresenterToRouterPostFeedProtocol {
    static func createModule() -> PostFeedViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "PostFeedViewController") as! PostFeedViewController
        
        let presenter: ViewToPresenterPostFeedProtocol & InteractorToPresenterPostFeedProtocol = PostFeedPresenter()
        let interactor: PresenterToInteractorPostFeedProtocol = PostFeedInteractor()
        let router:PresenterToRouterPostFeedProtocol = PostFeedRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushPostDetailController(navigationController: UINavigationController, cell: PostFeedCell, post: Post) {
        let postDetailPageViewController = PostDetailRouter.createModule()
        postDetailPageViewController.cell = cell
        postDetailPageViewController.post = post
        
        let presentationNavigationController = UINavigationController(rootViewController: postDetailPageViewController)
//        presentationNavigationController.transitioningDelegate = postDetailPageViewController
//        presentationNavigationController.modalPresentationStyle = .fullScreen
        
//        let tabbar = navigationController.viewControllers.first as! UITabBarController
//        let postFeed = tabbar.viewControllers?.first as! PostFeedViewController
        let postFeed = navigationController.viewControllers.first as! PostFeedViewController
        let segue = SwiftMessagesSegue(identifier: nil, source: postFeed, destination: presentationNavigationController)
        segue.configure(layout: .centered)
        segue.perform()
        
//        postFeed.present(presentationNavigationController, animated: true, completion: nil)
        
//        navigationController.pushViewController(postDetailViewController, animated: true)
    }
    

}
