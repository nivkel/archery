//
//  Post.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import AWSAppSync

struct PostLike {
    var id: String
    var userId: String
    var postId: String
    var type: String
    var post: Post
    
    init(id: String, userId: String, postId: String, type: String, post: GetLikesForUserQuery.Data.GetLikesForUser.Mapping.Post) {
        self.id = id
        self.userId = userId
        self.postId = postId
        self.type = type
        
        let post = Post(id: post.id, title: post.title, price: post.price, salePrice: post.salePrice, location: post.location, category: post.category, condition: post.condition, content: post.content, tags: post.tags, isSold: post.isSold, isPublished: post.isPublished, createdAt: post.createdAt, updatedAt: post.updatedAt, promotedAt: post.promotedAt, promotedCount: post.promotedCount, dateSold: post.dateSold, isNegotiable: post.isNegotiable, willShip: post.willShip, willMeet: post.willMeet, file: nil, file2: nil, file3: nil, file4: nil, userId: post.userId, user: nil, typename: post.typename)
        self.post = post
    }
}

class Post {
    var id: GraphQLID
    var title: String?
    var price: String?
    var salePrice: String?
    var location: String?
    var category: String?
    var condition: String?
    var content: String?
    var tags: String?
    var isSold: Bool?
    var isPublished: Bool?
    var createdAt: String?
    var updatedAt: String?
    var promotedAt: String?
    var promotedCount: String?
    var dateSold: String?
    var isNegotiable: Bool?
    var willShip: Bool?
    var willMeet: Bool?
    var file: ListPostsQuery.Data.ListPost.Item.File?
    var file2: ListPostsQuery.Data.ListPost.Item.File2?
    var file3: ListPostsQuery.Data.ListPost.Item.File3?
    var file4: ListPostsQuery.Data.ListPost.Item.File4?
    var presignedUrl: URL? // Not used
    var userId: GraphQLID
    var user: ListPostsQuery.Data.ListPost.Item.User?
    var typename: String?
    
    init(id: GraphQLID, title: String?, price: String?, salePrice: String?, location: String?, category: String?, condition: String?, content: String?, tags: String?, isSold: Bool?, isPublished: Bool?, createdAt: String?, updatedAt: String?, promotedAt: String?, promotedCount: String?, dateSold: String?, isNegotiable: Bool?, willShip: Bool?, willMeet: Bool?, file: ListPostsQuery.Data.ListPost.Item.File?, file2: ListPostsQuery.Data.ListPost.Item.File2?, file3: ListPostsQuery.Data.ListPost.Item.File3?, file4: ListPostsQuery.Data.ListPost.Item.File4?, userId: GraphQLID, user: ListPostsQuery.Data.ListPost.Item.User?, typename: String?) {
        self.id = id
        self.title = title
        self.price = price
        self.salePrice = salePrice
        self.location = location
        self.category = category
        self.condition = condition
        self.content = content
        self.tags = tags
        self.isSold = isSold
        self.isPublished = isPublished
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.promotedAt = promotedAt
        self.promotedCount = promotedCount
        self.dateSold = dateSold
        self.isNegotiable = isNegotiable
        self.willShip = willShip
        self.willMeet = willMeet
        self.file = file
        self.file2 = file2
        self.file3 = file3
        self.file4 = file4
        self.userId = userId
        self.user = user
        self.typename = typename
    }
    
    init(emptyTitle: String) {
        self.id = "empty"
        self.title = emptyTitle
        self.price = nil
        self.location = nil
        self.category = nil
        self.condition = nil
        self.content = nil
        self.tags = nil
        self.isSold = nil
        self.isNegotiable = nil
        self.willShip = nil
        self.willMeet = nil
        self.file = nil
        self.file2 = nil
        self.userId = "empty"
        self.user = nil
    }
    
    init(id: String, title: String, content: String, typename: String) {
        self.id = id
        self.title = title
        self.price = nil
        self.location = nil
        self.category = nil
        self.condition = nil
        self.content = content
        self.tags = nil
        self.isSold = nil
        self.isNegotiable = nil
        self.willShip = nil
        self.willMeet = nil
        self.file = nil
        self.file2 = nil
        self.userId = ""
        self.user = nil
        self.typename = typename
    }
}

extension Post: ListDiffable {
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? Post {
            return title == object.title
        }
        return false
    }
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
}
