//
//  PostFeedProtocol.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAppSync

protocol ViewToPresenterPostFeedProtocol: class {
    var view: PresenterToViewPostFeedProtocol? {get set}
    var interactor: PresenterToInteractorPostFeedProtocol? {get set}
    var router: PresenterToRouterPostFeedProtocol? {get set}
    func startFetchPosts(limit: Int?, nextToken: String?)
    func startSearchPosts(filters: [ModelPostFilterInput], limit: Int?, nextToken: String?)
    func showPostDetailController(navigationController:UINavigationController, cell: PostFeedCell, post: Post)
}

protocol PresenterToViewPostFeedProtocol: class {
    func postFeed(response: [Post?], nextToken: String?)
    func showError()
    func postFeedSearch(response: [Post], nextToken: String?)
}

protocol PresenterToRouterPostFeedProtocol: class {
    static func createModule() -> PostFeedViewController
    func pushPostDetailController(navigationController:UINavigationController, cell: PostFeedCell, post: Post)
}

protocol PresenterToInteractorPostFeedProtocol: class {
    var presenter:InteractorToPresenterPostFeedProtocol? {get set}
    func fetchPosts(limit: Int?, nextToken: String?)
    func searchPosts(filters: [ModelPostFilterInput], limit: Int?, nextToken: String?)
}

protocol InteractorToPresenterPostFeedProtocol: class {
    func postFeedFetchSuccess(response: [Post?], nextToken: String?)
    func postFeedFetchFailed()
    func postFeedSearchSuccess(response: [Post], nextToken: String?)
}
