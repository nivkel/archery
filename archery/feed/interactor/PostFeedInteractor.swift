//
//  PostFeedInteractor.swift
//  archery
//
//  Created by Kelvin Lee on 10/15/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAppSync

class PostFeedInteractor: PresenterToInteractorPostFeedProtocol {
    var presenter: InteractorToPresenterPostFeedProtocol?
    
    func fetchPosts(limit: Int?, nextToken: String?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let dataService = appDelegate.dataService
        dataService?.fetchPosts(presenter: presenter!, limit: limit, nextToken: nextToken)
    }
    
    func searchPosts(filters: [ModelPostFilterInput], limit: Int?, nextToken: String?) {
        PostServicesManager.shared.fetchPostsFilterSearchQuery(filters: filters, presenter: presenter!, limit: limit, nextToken: nextToken)
    }
}
