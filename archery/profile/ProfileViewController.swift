//
//  ProfileViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftMessages
import AWSAuthCore
import AWSAuthUI
import AWSFacebookSignIn
import AWSMobileClient
import IGListKit
import PromiseKit
import NVActivityIndicatorView

enum ProfileTabNames: String {
    case selling = "Selling"
    case sold = "Sold"
    case favorites = "Favorites"
}

class ProfileViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()
    
    var objects = [ListDiffable]()
    
    var selectedTab: ProfileTabNames = .selling
    
    let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Working...", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Airya"
        
//        let layout = ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
//        layout.showHeaderWhenEmpty = true
//        layout.stickyHeaderYOffset = 3.0
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        self.collectionView.collectionViewLayout = layout
        self.collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
//        self.collectionView.register(ProfileHeaderTabsCell.nib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ProfileHeaderTabsCell.reuseIdentifier)
        self.collectionView.register(ProfileHeaderCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProfileHeaderCell.reuseIdentifier)
        
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let image = UIImage(icon: .FAGear, size: CGSize(width: 30.0, height: 30.0))
        let menuButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(ProfileViewController.showProfileMenu(sender:)))
        menuButton.tintColor = Common.primaryColor
        self.navigationItem.setRightBarButton(menuButton, animated: true)
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = Common.primaryColor
        
        self.loadSellingData()
    }
    
    func loadLikedData() {
//        let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.audioEqualizer, color: Common.primaryColor, padding: 1.0)
//        self.view.addSubview(activity)
//        activity.startAnimating()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        firstly {
            ChatServicesManager.shared.getUser()
            }.get { user in
                self.objects = [user]
            }.then { user in
                PostServicesManager.shared.getUserPostLikes(userId: user.id)
            }.get { postLikes in
                var posts = [Post]()
                let userPosts = UserPosts()
                if postLikes.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "You don't have any favorites")]
                } else {
                    for postLike in postLikes {
                        posts.append(postLike.post)
                    }
                    userPosts.posts = posts
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .favorites
            }.ensure {
                self.adapter.performUpdates(animated: true) { (complete) in
//                    activity.removeFromSuperview()
//                    activity.stopAnimating()
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            }.catch { error in
                debugPrint("getUserPostLikes error:\(error)")
        }
    }
    
    func loadSellingData() {
        debugPrint("Profile load data")
//        let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.audioEqualizer, color: Common.primaryColor, padding: 1.0)
//        self.view.addSubview(activity)
//        activity.startAnimating()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        firstly {
            ChatServicesManager.shared.getUser()
            }.get { user in
                self.objects = [user]
            }.then { user in
                PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: false, limit: 20, nextToken: nil)
            }.get { postsSold in
                let userPosts = UserPosts()
                if postsSold.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "You're not selling anything")]
                } else {
                    userPosts.posts = postsSold
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .selling
            }.ensure {
                self.adapter.reloadData(completion: { (complete) in
//                    activity.removeFromSuperview()
//                    activity.stopAnimating()
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                })
//                self.adapter.performUpdates(animated: true) { (complete) in
//
//                }
            }.catch { error in
                debugPrint("fetchPostsIsSold error:\(error)")
        }
    }
    
    func loadSoldData() {
        debugPrint("Profile Sold load data")
//        let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.audioEqualizer, color: Common.primaryColor, padding: 1.0)
//        self.view.addSubview(activity)
//        activity.startAnimating()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        firstly {
            ChatServicesManager.shared.getUser()
            }.get { user in
                self.objects = [user]
            }.then { user in
                PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: true, limit: 20, nextToken: nil)
            }.get { postsSold in
                let userPosts = UserPosts()
                if postsSold.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "You haven't sold anything")]
                } else {
                    userPosts.posts = postsSold
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .sold
            }.ensure {
                self.adapter.performUpdates(animated: true) { (complete) in
//                    activity.removeFromSuperview()
//                    activity.stopAnimating()
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            }.catch { error in
                debugPrint("fetchPostsIsSold error:\(error)")
        }
    }
    
    @objc func showProfileMenu(sender: Any) {
//        Common().showMenu(sender, delegate: self, type: .profile, layout: .centered)
        let settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
    
    func showUpdateLocation() {
        let locationViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        let navigationController = UINavigationController(rootViewController: locationViewController)
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
}

extension ProfileViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension ProfileViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {
        
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension ProfileViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.objects
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        case is UserPosts:
            return ProfileTabsSectionController()
        default:
            return ProfileSectionController()
        }
        
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let customView = SkeletonView(frame: self.collectionView.frame)
        
        return customView
    }
}
