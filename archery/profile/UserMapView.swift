//
//  UserMapView.swift
//  archery
//
//  Created by Kelvin Lee on 11/21/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit

class UserMapView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let annotation = newValue as? UserMapAnnotation else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: 0, y: 72)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(icon: .FAUser, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), for: UIControl.State())
            rightCalloutAccessoryView = mapsButton
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = annotation.name
            detailCalloutAccessoryView = detailLabel
//            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            if let annotationImage = annotation.image {
                image = annotationImage
            } else {
                image = nil
            }
        }
    }
}
