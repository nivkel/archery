//
//  PostShortCell.swift
//  archery
//
//  Created by Kelvin Lee on 1/22/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class PostShortCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var salePriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 8.0
        imageView.layer.cornerRadius = 8.0
        
        iconImageView.setFAIconWithName(icon: .FATag, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40.0, height: 40.0))
        
        imageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        salePriceLabel.text = nil
        subtitleLabel.text = nil
        subtitleLabel.attributedText = nil
    }
    
    func setup(s3Object: AWSS3Object) {
        guard let key = s3Object.key else {
            return
        }
        let width = Int(self.imageView.frame.size.width) * 2
        let height = Int(self.imageView.frame.size.height) * 2
        debugPrint("imageView size height:\(height), width:\(width)")
        let fitInResolution = "\(width)x\(height)/"
        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
        debugPrint("s3 urlString:\(urlString)")
        guard let url = URL(string: urlString) else {
            return
        }
        debugPrint("s3 url:\(url)")
        let imageResource = ImageResource(downloadURL: url)
        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
        self.imageView.kf.indicatorType = .activity
        self.imageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
            debugPrint("Progress received:\(received), total:\(total)")
        }) { (result) in
            switch result {
            case .success(let value):
                // The image was set to image view:
                print(value.image)
                self.imageView.image = value.image
                // From where the image was retrieved:
                // - .none - Just downloaded.
                // - .memory - Got from memory cache.
                // - .disk - Got from disk cache.
                print(value.cacheType)
                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                // The source object which contains information like `url`.
                print(value.source)
            case .failure(let error):
                print("error \(error)") // The error happens
            }
        }
    }
}
