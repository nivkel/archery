//
//  UserMapAnnotation.swift
//  archery
//
//  Created by Kelvin Lee on 11/21/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit

class UserMapAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    let title: String?
    let name: String
    let image: UIImage?
    
    init(title: String, name: String, coordinate: CLLocationCoordinate2D, image: UIImage?) {
        self.title = title
        self.name = name
        self.coordinate = coordinate
        self.image = image
    }
}
