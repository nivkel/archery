//
//  ProfileHeaderTabsCell.swift
//  archery
//
//  Created by Kelvin Lee on 12/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class ProfileHeaderTabsCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var sellingButton: UIButton!
    @IBOutlet weak var soldButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    func setSoldTabColors() {
        self.sellingButton.backgroundColor = .darkGray
        self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
        self.soldButton.backgroundColor = Common.primaryColor
        self.soldButton.setTitleColor(.white, for: .normal)
        self.favoriteButton.backgroundColor = .darkGray
        self.favoriteButton.setTitleColor(Common.tertiaryColor, for: .normal)
    }
    
    func setSellingTabColors() {
        self.sellingButton.backgroundColor = Common.primaryColor
        self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
        self.soldButton.backgroundColor = .darkGray
        self.soldButton.setTitleColor(Common.tertiaryColor, for: .normal)
        self.favoriteButton.backgroundColor = .darkGray
        self.favoriteButton.setTitleColor(Common.tertiaryColor, for: .normal)
    }
    
    func setFavoriteTabColors() {
        self.sellingButton.backgroundColor = .darkGray
        self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
        self.soldButton.backgroundColor = .darkGray
        self.soldButton.setTitleColor(Common.tertiaryColor, for: .normal)
        self.favoriteButton.backgroundColor = Common.primaryColor
        self.favoriteButton.setTitleColor(Common.tertiaryColor, for: .normal)
    }
}
