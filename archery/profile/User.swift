//
//  User.swift
//  archery
//
//  Created by Kelvin Lee on 12/2/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAppSync
import IGListKit

class User {
    var cognitoId: GraphQLID
    var id: GraphQLID
    var username: String
    var email: String
    var location: String?
    var verified: Bool
    var phoneVerified: Bool?
    var score: String?
    var facebookId: String?
    
    init(id: GraphQLID, cognitoId: String, username: String, email: String, location: String?, verified: Bool, phoneVerified: Bool?, score: String?, facebookId: String?) {
        self.id = id
        self.cognitoId = cognitoId
        self.username = username
        self.email = email
        self.location = location
        self.verified = verified
        self.phoneVerified = phoneVerified
        self.score = score
        self.facebookId = facebookId
    }
}

extension User: ListDiffable {
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? User {
            return username == object.username
        }
        return false
    }
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
}

class UserPosts: ListDiffable {
    var id = UUID().uuidString
    
    var posts = [Post]()
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? UserPosts {
            return id == object.id
        }
        return false
    }
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
}
