//
//  UserProfileViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/2/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import PromiseKit
import NVActivityIndicatorView

class UserProfileViewController: ProfileViewController {

    @IBOutlet weak var userCollectionView: UICollectionView!
    
    var user: ListPostsQuery.Data.ListPost.Item.User!
    
    var me: User?
    
    override func viewDidLoad() {
        

        // Do any additional setup after loading the view.
//        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(UserProfileViewController.close))
//        closeButton.tintColor = UIColor.black
//        self.navigationItem.rightBarButtonItem = closeButton
        
        self.navigationItem.title = "Profile"
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            topItem.backBarButtonItem?.tintColor = Common.primaryColor
        }
        
//        let layout = ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
//        layout.showHeaderWhenEmpty = true
//        self.userCollectionView.collectionViewLayout = layout
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        self.userCollectionView.collectionViewLayout = layout
        self.userCollectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        self.userCollectionView.register(ProfileHeaderCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProfileHeaderCell.reuseIdentifier)
        
        self.adapter.collectionView = self.userCollectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        
        
        let image = UIImage(icon: .FAListAlt, size: CGSize(width: 30.0, height: 30.0))
        let menuButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(UserProfileViewController.showProfileMenu(sender:)))
        menuButton.tintColor = Common.primaryColor
        self.navigationItem.setRightBarButton(menuButton, animated: true)
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = Common.primaryColor
        
        debugPrint("post user:\(self.user.email)")
        
        self.loadSellingData()
//        let user = User(id: self.user.id, cognitoId: self.user.cognitoId, username: self.user.username, email: self.user.email, location: self.user.location!, verified: self.user.verified!)
//        self.objects = [user]
//        let userPosts = UserPosts()
//        self.objects = self.objects + [userPosts]
//        self.adapter.performUpdates(animated: true) { (complete) in
//
//        }
    }
    
//    func loadUser() {
//        firstly {
//            ChatServicesManager.shared.getUser()
//            }.get { user in
//                self.objects = [user]
//            }.ensure {
//                self.adapter.performUpdates(animated: true) { (complete) in
//
//                }
//            }.catch { error in
//                debugPrint("getUser error:\(error)")
//        }
//    }
    
    override func loadLikedData() {
        let user = User(id: self.user.id, cognitoId: self.user.cognitoId, username: self.user.username, email: self.user.email, location: self.user.location, verified: self.user.verified!, phoneVerified: self.user.phoneVerified, score: self.user.score, facebookId: self.user.facebookId)
        self.objects = [user]
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
        ChatServicesManager.shared.getUser().get { me in
            self.me = me
            debugPrint("getUser user:\(user.id)")
            }.then { me in
                PostServicesManager.shared.getUserPostLikes(userId: user.id)
            }.get { postLikes in
                var posts = [Post]()
                let userPosts = UserPosts()
                if postLikes.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "\(user.username) don't have any favorites")]
                } else {
                    for postLike in postLikes {
                        posts.append(postLike.post)
                    }
                    userPosts.posts = posts
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .favorites
            }.ensure {
                self.adapter.performUpdates(animated: true) { (complete) in
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            }.catch { error in
                debugPrint("getUserPostLikes error:\(error)")
        }
        
//        PostServicesManager.shared.getUserPostLikes(userId: user.id).get { postLikes in
//            var posts = [Post]()
//            let userPosts = UserPosts()
//            if postLikes.isEmpty {
//                userPosts.posts = [Post(emptyTitle: "\(user.username) don't have any favorites")]
//            } else {
//                for postLike in postLikes {
//                    posts.append(postLike.post)
//                }
//                userPosts.posts = posts
//            }
//            self.objects = self.objects + [userPosts]
//            self.selectedTab = .favorites
//            }.ensure {
//                self.adapter.performUpdates(animated: true) { (complete) in
//
//                }
//            }.catch { error in
//                debugPrint("getUserPostLikes error:\(error)")
//        }
    }
    
    override func loadSellingData() {
        debugPrint("Profile load data")
        let user = User(id: self.user.id, cognitoId: self.user.cognitoId, username: self.user.username, email: self.user.email, location: self.user.location, verified: self.user.verified!, phoneVerified: self.user.phoneVerified, score: self.user.score, facebookId: self.user.facebookId)
        self.objects = [user]
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
        ChatServicesManager.shared.getUser().get { me in
            self.me = me
            debugPrint("getUser user:\(user.id)")
            }.then { me in
              PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: false, limit: nil, nextToken: nil)
            }.get { postsSold in
                let userPosts = UserPosts()
                if postsSold.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "\(user.username) not selling anything")]
                } else {
                    userPosts.posts = postsSold
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .selling
            }.ensure {
                self.adapter.performUpdates(animated: true) { (complete) in
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            }.catch { error in
                debugPrint("fetchPostsIsSold error:\(error)")
        }
//        PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: false, limit: nil, nextToken: nil).get { postsSold in
//            let userPosts = UserPosts()
//            if postsSold.isEmpty {
//                userPosts.posts = [Post(emptyTitle: "\(user.username) not selling anything")]
//            } else {
//                userPosts.posts = postsSold
//            }
//            self.objects = self.objects + [userPosts]
//            self.selectedTab = .selling
//            }.ensure {
//                self.adapter.performUpdates(animated: true) { (complete) in
//
//                }
//            }.catch { error in
//                debugPrint("fetchPostsIsSold error:\(error)")
//        }
    }
    
    override func loadSoldData() {
        debugPrint("Profile Sold load data")
        let user = User(id: self.user.id, cognitoId: self.user.cognitoId, username: self.user.username, email: self.user.email, location: self.user.location, verified: self.user.verified!, phoneVerified: self.user.phoneVerified, score: self.user.score, facebookId: self.user.facebookId)
        self.objects = [user]
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
        ChatServicesManager.shared.getUser().get { me in
            self.me = me
            debugPrint("getUser user:\(user.id)")
            }.then { me in
                PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: true, limit: nil, nextToken: nil)
            }.get { postsSold in
                let userPosts = UserPosts()
                if postsSold.isEmpty {
                    userPosts.posts = [Post(emptyTitle: "You haven't sold anything")]
                } else {
                    userPosts.posts = postsSold
                }
                self.objects = self.objects + [userPosts]
                self.selectedTab = .sold
            }.ensure {
                self.adapter.performUpdates(animated: true) { (complete) in
                    NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            }.catch { error in
                debugPrint("fetchPostsIsSold error:\(error)")
        }
        
//        PostServicesManager.shared.fetchPostsFilterIsSold(user: user, isSold: true, limit: nil, nextToken: nil).get { postsSold in
//            let userPosts = UserPosts()
//            if postsSold.isEmpty {
//                userPosts.posts = [Post(emptyTitle: "You haven't sold anything")]
//            } else {
//                userPosts.posts = postsSold
//            }
//            self.objects = self.objects + [userPosts]
//            self.selectedTab = .sold
//            }.ensure {
//                self.adapter.performUpdates(animated: true) { (complete) in
//
//                }
//            }.catch { error in
//                debugPrint("fetchPostsIsSold error:\(error)")
//        }
    }
    
    @objc override func showProfileMenu(sender: Any) {
        Common().showMenu(sender, delegate: self, type: .userPostProfile, layout: .topCard)
    }
    
//    @objc func close() {
//        self.dismiss(animated: true, completion: nil)
//    }
    
    override func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        case is UserPosts:
            return ProfileTabsSectionController()
        default:
            return ProfileSectionController(isUser: true)
        }
    }
    
    override func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let customView = SkeletonView(frame: self.userCollectionView.frame)
        
        return customView
    }
}
