//
//  UserMapMarkerView.swift
//  archery
//
//  Created by Kelvin Lee on 11/21/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit

class UserMapMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let annotation = newValue as? UserMapAnnotation else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            // 2
            markerTintColor = UIColor.red
            glyphText = annotation.name
        }
    }
}
