//
//  ProfileSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 12/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import RxCocoa
import SwiftMessages
import Kingfisher

class ProfileSectionController: ListSectionController {
    
    var object: ListDiffable?
    
    let disposeBag = DisposeBag()
    
    var isUser: Bool = false
    
    override init() {
        super.init()
//        supplementaryViewSource = self
    }
    
    init(isUser: Bool) {
        super.init()
        self.isUser = isUser
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = self.collectionContext!.containerSize.width
        return CGSize(width: width, height: 260)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: ProfileHeaderCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! ProfileHeaderCell
        guard let user = self.object as? User else {
            return cell
        }
        if isUser {
            cell.changeLocationButton.isHidden = true
            cell.scoreDetailButton.isHidden = true
            cell.changeLocationButton.isHidden = true
        }
        if let facebookId = user.facebookId, let url = URL(string: "https://graph.facebook.com/" + facebookId + "/picture?type=large") {
            let imageResource = ImageResource(downloadURL: url)
            let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 30.0, height: 30.0), orientation: .up, textColor: UIColor.black, backgroundColor: UIColor.white)
            cell.profileImageView.kf.indicatorType = .activity
            cell.profileImageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
                debugPrint("Progress received:\(received), total:\(total)")
            }) { (result) in
                switch result {
                case .success(let value):
                    // The image was set to image view:
                    print(value.image)
                    DispatchQueue.main.async {
                        cell.profileImageView.image = value.image
                    }
                    print(value.cacheType)
                    debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                    print("download image source:\(value.source.cacheKey)")
                case .failure(let error):
                    print("error \(error)") // The error happens
                }
            }
        } else {
            cell.profileImageView.setImageForName(Common.usernameInitials(value: user.username), gradientColors: (Common.primaryColor, UIColor.white), circular: true, textAttributes: nil)
        }
        
        if let score = user.score {
            cell.scoreLabel.text = score
            if let scoreNumber = Double(score) {
                cell.updateChart(value: scoreNumber)
            }
        } else {
            cell.scoreLabel.text = "—"
            cell.updateChart(value: 0)
        }
//        cell.profileImageView.setFAIconWithName(icon: .FAUserCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 30.0, height: 30.0))
        cell.profileImageView.layer.cornerRadius = 60.0
        cell.profileImageView.clipsToBounds = true
        cell.changeLocationButton.layer.cornerRadius = 10.0
        cell.scoreDetailButton.layer.cornerRadius = 10.0
        cell.scoreDetailButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.showScoreDetail(user: user)
            }).disposed(by: self.disposeBag)
        cell.changeLocationButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.presentUpdateLocation()
            }).disposed(by: self.disposeBag)
        cell.nameLabel.text = user.username
        
        if let location = user.location {
            LocationServicesManager.shared.getCityFromCoordinate(location: location) { (city) in
                cell.locationLabel.text = city
            }
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.object = object as? ListDiffable
    }
    
    override func didSelectItem(at index: Int) {
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        
    }
    
    func presentUpdateLocation() {
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        profileViewController.showUpdateLocation()
    }
    
    func showScoreDetail(user: User) {
        guard !self.isUser else {
            return
        }
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        let scoreViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "ScoreViewController") as! ScoreViewController
        scoreViewController.user = user
        let navigationController = UINavigationController(rootViewController: scoreViewController)
        let segue = SwiftMessagesSegue(identifier: nil, source: profileViewController, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
}

extension ProfileSectionController: ListSupplementaryViewSource {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader/*, UICollectionElementKindSectionFooter*/]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
    //        case UICollectionElementKindSectionFooter:
//            return userFooterView(atIndex: index)
        default:
            fatalError()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 260)
    }
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, nibName: "ProfileHeaderCell", bundle: nil, at: index) as? ProfileHeaderCell else {
            fatalError()
        }
        
        return view
    }
}
