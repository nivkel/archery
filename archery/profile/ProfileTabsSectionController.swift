//
//  ProfileTabsSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 12/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import RxCocoa
import SwiftMessages

class ProfileTabsSectionController: ListSectionController {
    
    var object: ListDiffable?
    
    let disposeBag = DisposeBag()

    override init() {
        super.init()
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        guard let userPosts = object as? UserPosts else {
            return 0
        }
        return userPosts.posts.count
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = self.collectionContext!.containerSize.width
        if let userPosts = object as? UserPosts {
            if userPosts.posts[index].id == "empty" {
                return CGSize(width: width, height: 200)
            }
        }
        return CGSize(width: width, height: 80)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: PostShortCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! PostShortCell
        debugPrint("cellForItem object:\(self.object)")
        guard let userPosts = object as? UserPosts else {
            return cell
        }
        if userPosts.posts[index].id == "empty" {
            let emptyCell = self.collectionContext!.dequeueReusableCell(withNibName: EmptyCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! EmptyCell
            emptyCell.iconImageView.setFAIconWithName(icon: .FACloud, textColor: Common.secondaryColor, orientation: .up, backgroundColor: Common.tertiaryColor, size: CGSize(width: 100.0, height: 100.0))
            emptyCell.backgroundColor = Common.tertiaryColor
            emptyCell.titleLabel.text = userPosts.posts[index].title!
            emptyCell.titleLabel.textColor = Common.secondaryColor
            return emptyCell
        }
        let post = userPosts.posts[index]
        cell.titleLabel.text = post.title
        
        if let createdAt = post.createdAt {
            cell.dateLabel.text = Common.createdAtRelativeDate(value: createdAt) + " ago"
        } else {
            cell.dateLabel.text = ""
        }
        
//        if let price = post.price {
//            cell.subtitleLabel.text = "$\(price)"
//        }
        if let salePrice = post.salePrice, let price = post.price {
            cell.salePriceLabel.isHidden = false
            let attributeString = NSMutableAttributedString(string: "$\(price)")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            attributeString.addAttribute(NSAttributedString.Key.strokeColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
            attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
            
            cell.subtitleLabel.attributedText = attributeString
            cell.salePriceLabel.text = "$\(salePrice)"
        } else {
            if let price = post.price {
                cell.salePriceLabel.text = nil
                cell.subtitleLabel.attributedText = nil
                cell.subtitleLabel.text = "$\(price)"
            }
        }
        debugPrint("post file:\(post.file)")
        if let file = post.file {
            let s3Object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region)
            cell.setup(s3Object: s3Object)
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.object = object as? ListDiffable
    }
    
    override func didSelectItem(at index: Int) {
        guard let userPosts = object as? UserPosts else {
            return
        }
        if userPosts.posts[index].id == "empty" {
            return
        }
        let post = userPosts.posts[index]
        debugPrint("post in profile:\(post.title)")
        let postDetailPageViewController = PostDetailRouter.createModule()
        postDetailPageViewController.post = post
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        if let user = profileViewController.objects.first as? User {
            debugPrint("profile user:\(user.id), post user:\(post.userId)")
            if user.id == post.userId {
                postDetailPageViewController.isSeller = true
            }
        }
        if let userProfileViewController = self.viewController as? UserProfileViewController {
            if let me = userProfileViewController.me {
                if me.id != post.userId {
                    postDetailPageViewController.isSeller = false
                }
            }
            postDetailPageViewController.isFromUserProfile = true
        }
        let presentationNavigationController = UINavigationController(rootViewController: postDetailPageViewController)
//        presentationNavigationController.transitioningDelegate = postDetailPageViewController
//        presentationNavigationController.modalPresentationStyle = .fullScreen
//        profileViewController.present(presentationNavigationController, animated: true, completion: nil)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: profileViewController, destination: presentationNavigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
    
    func handleSellingButtonTap() {
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        profileViewController.loadSellingData()
    }
    
    func handleSoldButtonTap() {
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        profileViewController.loadSoldData()
    }
    
    func handleFavoriteButtonTap() {
        guard let profileViewController = self.viewController as? ProfileViewController else {
            return
        }
        profileViewController.loadLikedData()
    }
}

extension ProfileTabsSectionController: ListSupplementaryViewSource {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader/*, UICollectionElementKindSectionFooter*/]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            fatalError()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 50)
    }
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, nibName: "ProfileHeaderTabsCell", bundle: nil, at: index) as? ProfileHeaderTabsCell else {
            fatalError()
        }
        if let profileViewController = self.viewController as? ProfileViewController {
            switch profileViewController.selectedTab {
            case .favorites:
                view.setFavoriteTabColors()
                break
            case .selling:
                view.setSellingTabColors()
                break
            case .sold:
                view.setSoldTabColors()
                break
            }
        }
        view.sellingButton.layer.cornerRadius = 10.0
        view.sellingButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleSellingButtonTap()
            }).disposed(by: self.disposeBag)
        view.soldButton.layer.cornerRadius = 10.0
        view.soldButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleSoldButtonTap()
            }).disposed(by: self.disposeBag)
        view.favoriteButton.layer.cornerRadius = 10.0
        view.favoriteButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleFavoriteButtonTap()
            }).disposed(by: self.disposeBag)
        
        return view
    }
}
