//
//  ProfileHeaderCell.swift
//  archery
//
//  Created by Kelvin Lee on 12/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable
import SwiftChart

class ProfileHeaderCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var scoreDetailButton: UIButton!
    @IBOutlet weak var changeLocationButton: UIButton!
    @IBOutlet weak var scoreContainerView: UIView!
    @IBOutlet weak var chartView: Chart!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scoreLabel.layer.cornerRadius = 10.0
        scoreLabel.clipsToBounds = true
        
        scoreContainerView.layer.cornerRadius = 10.0
        
        updateChart(value: 0.0)
    }
    
    func updateChart(value: Double) {
        chartView.removeAllSeries()
        
        let data = [
            (x: 0, y: 3.0),
            (x: value, y: 3.0)
        ]
        let data1 = [
            (x: 0, y: 3.0),
            (x: 30.0, y: 3.0)
        ]
        let series1 = ChartSeries(data: data1)
        series1.color = Common.primaryColor
        series1.area = false
        
        let series = ChartSeries(data: data)
        series.area = true
        series.color = Common.primaryColor
        chartView.minY = 0.0
        chartView.maxY = 5.0
        chartView.xLabels = [0, 5, 10, 15, 20, 25, 30]
        chartView.labelColor = Common.secondaryColor
        chartView.showYLabelsAndGrid = false
        chartView.highlightLineColor = Common.primaryColor
//        chartView.xLabelsFormatter = { String(Int(round($1))) + "h" }
        chartView.add([series, series1])
    }
}
