//
//  CustomPresentTransitionController.swift
//  archery
//
//  Created by Kelvin Lee on 10/25/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class CustomPresentTransitionController: NSObject, UIViewControllerAnimatedTransitioning {
//    private let feedCell: PostFeedCell
    weak var context: UIViewControllerContextTransitioning?
    
    override init() {
        
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 2.0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // 1
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to),
            let fromView = transitionContext.view(forKey: .from),
            let toView = transitionContext.view(forKey: .to),
            let snapshot = toVC.view.snapshotView(afterScreenUpdates: true)
            else {
                return
        }
        self.context = transitionContext
        // 2
        let containerView = transitionContext.containerView
        let finalFrame = transitionContext.finalFrame(for: toVC)
        
        // 3
//        snapshot.frame = self.feedCell.frame
//        snapshot.layer.cornerRadius = 10.0
//        snapshot.layer.masksToBounds = true
        
        // 1
        containerView.addSubview(toView)
        containerView.addSubview(snapshot)
        toView.isHidden = true
        
        // 2
        debugPrint("snapshot rect:\(snapshot.frame)")
//        snapshot.layer.transform = CATransform3DMakeScale(0.25, 0.25, 0.25)
//        snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width, 0.0, 0.0)
//        snapshot.layer.transform = CATransform3DMakeTranslation(0.0, 0.0, 0.0)
        snapshot.alpha = 0.0
        
        
//        let circle = UIBezierPath(arcCenter: CGPoint(x: snapshot.frame.midX, y: snapshot.frame.midY), radius: snapshot.frame.size.height, startAngle: -.pi / 2, endAngle: .pi + .pi / 2, clockwise: true)
//        circle.addClip()
//        let mask = CAShapeLayer()
//        mask.path = circle.cgPath
//        snapshot.layer.mask = mask
        
        
//        AnimationHelper.perspectiveTransform(for: containerView)
//        snapshot.layer.transform = AnimationHelper.yRotation(.pi / 2)
        // 3
        
        /////////
//        let trianglePath = UIBezierPath()
//        let side = containerView.bounds.width
//        let startX = feedCell.frame.midX - side / 2
//        let startY = feedCell.frame.midY - side / 2
//        trianglePath.move(to: CGPoint(x: startX, y: startY))
//        trianglePath.addLine(to: CGPoint(x: startX, y: startY + side))
//        trianglePath.addLine(to: CGPoint(x: startX + side, y: startY + side / 2))
//        trianglePath.close()
        ////
        let initialMask = UIBezierPath(arcCenter: CGPoint(x: snapshot.frame.midX, y: snapshot.frame.midY), radius: 100, startAngle: -.pi / 2, endAngle: .pi + .pi / 2, clockwise: true)
//        let initialMask = UIBezierPath(ovalIn: snapshot.frame)
        let maskLayer = CAShapeLayer()
        maskLayer.path = initialMask.cgPath
        snapshot.layer.mask = maskLayer

        let finalMask = UIBezierPath(arcCenter: CGPoint(x: snapshot.frame.midX, y: snapshot.frame.midY), radius: 600.0 , startAngle: -.pi / 2, endAngle: .pi + .pi / 2, clockwise: true)
//        let finalMask = UIBezierPath(ovalIn: snapshot.frame.offsetBy(dx: 100.0, dy: 100.0))
        
        let duration = transitionDuration(using: transitionContext)

        let maskLayerAnimation = CABasicAnimation(keyPath: "path")
        maskLayerAnimation.fromValue = initialMask.cgPath
        maskLayerAnimation.toValue = finalMask.cgPath
        maskLayerAnimation.duration = duration
        maskLayerAnimation.delegate = self
        maskLayer.add(maskLayerAnimation, forKey: "path")
        /////////
        
        // 1
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0,
            options: .calculationModeCubicPaced,
            animations: {
                // 2
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1/3) {
//                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.75, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
//                    snapshot.frame = self.feedCell.frame
                    snapshot.alpha = 0.50
                }
                
                // 3
                UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3) {
//                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.5, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(0.75, 0.75, 0.75)
//                    snapshot.frame = self.feedCell.frame
                    snapshot.alpha = 0.75
                }
                
                // 4
                UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3) {
//                    snapshot.frame = finalFrame
//                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.2, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
                    snapshot.alpha = 1.0
//                    snapshot.layer.cornerRadius = 0
                }
        },
            // 5
            completion: { _ in
                snapshot.removeFromSuperview()
                toView.isHidden = false
//                fromView.layer.transform = CATransform3DIdentity
//                self.context?.completeTransition(!self.context!.transitionWasCancelled)
        })
    }
    

}

extension CustomPresentTransitionController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        context?.completeTransition(true)
    }
}
