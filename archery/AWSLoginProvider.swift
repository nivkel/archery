//
//  AWSLoginProvider.swift
//  archery
//
//  Created by Kelvin Lee on 11/12/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSCore
import FBSDKLoginKit
import AWSAuthCore
import AWSCognitoIdentityProvider

class AWSLoginProvider: NSObject, AWSIdentityProviderManager, AWSSignInProvider {
    
    static let shared = AWSLoginProvider()
    
    static let cliendId = "4dj15ofvcbh29t36lgpuhbbvou"
    static let clientSecret = "1grmo3g94ntottat5tdaq3ue07dja6meaadfcksou1lbifguc5js"
    static let poolId = "us-east-1_Emz4S6lu0"
    let identityPoolId = "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6"
    static let AWSCognitoUserPoolsSignInProviderKey = "UserPool"
    
    lazy var pool: AWSCognitoIdentityUserPool = {
        let serviceConfiguration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: nil)
        let poolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: AWSLoginProvider.cliendId, clientSecret: AWSLoginProvider.clientSecret, poolId: AWSLoginProvider.poolId)
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: poolConfiguration, forKey: AWSLoginProvider.AWSCognitoUserPoolsSignInProviderKey)
        let pool = AWSCognitoIdentityUserPool(forKey: AWSLoginProvider.AWSCognitoUserPoolsSignInProviderKey)
//        pool.delegate = self
        
        
        return pool
    }()
    
    var identityProviderName: String {
        return pool.identityProviderName
    }
    
    func token() -> AWSTask<NSString> {
        return pool.token()
    }
    
    
    
    func logins() -> AWSTask<NSDictionary> {
        if FBSDKAccessToken.currentAccessTokenIsActive() {
            if let token = FBSDKAccessToken.current().tokenString {
                return AWSTask(result: [AWSIdentityProviderFacebook:token])
            }
        }
        if let poolToken = pool.token().result {
            debugPrint("cog pool token:\(poolToken)")
//            let login: NSDictionary = ["cognito-identity.amazonaws.com": credential.token]
            return AWSTask(result: [AWSIdentityProviderAmazonCognitoIdentity: poolToken])
        }
        
        
        return AWSTask(error:NSError(domain: "Facebook Login", code: -1 , userInfo: ["Facebook" : "No current Facebook access token"]))
    }
    
    func getIdentityId(completionHandler: @escaping (String?) -> Void) {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: AWSLoginProvider.shared.identityPoolId, identityProviderManager: AWSLoginProvider.shared.pool)
        credentialsProvider.getIdentityId().continueOnSuccessWith { (task) -> Any? in
            debugPrint("getIdentityId task:\(task.result)")
            guard let identityId = task.result as? String else {
                return nil
            }
            debugPrint("credentialsProvider identityId:\(credentialsProvider.identityId)")
            completionHandler(identityId)
            return identityId
        }
    }
    
    //// AWSSignInProvider
    
    var isLoggedIn: Bool {
        get {
            return pool.currentUser()?.isSignedIn ?? false
        }
    }
    
    func login(_ completionHandler: @escaping (Any?, Error?) -> Void) {
        pool.currentUser()
    }
    
    func logout() {
        debugPrint("Logout ...")
        pool.currentUser()?.signOut()
    }
    
    func reloadSession() {
        pool.currentUser()?.getSession()
    }

}
