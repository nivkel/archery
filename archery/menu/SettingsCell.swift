//
//  SettingsCell.swift
//  archery
//
//  Created by Kelvin Lee on 3/23/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class SettingsCell: UITableViewCell, NibReusable {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toggleSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
