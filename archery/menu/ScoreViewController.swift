//
//  ScoreViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/16/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import RxSwift
import RxCocoa

enum ScoreType {
    case facebook
    case email
    case phone
    case sold
}

class ScoreViewController: UIViewController {
    
    var user: User!
    
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: 400)
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(ScoreViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationItem.title = "Score Details"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
}

extension ScoreViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScoreCell.reuseIdentifier, for: indexPath) as! ScoreCell
        switch indexPath.row {
        case 0:
            if let facebookId = self.user.facebookId {
                cell.titleLabel.text = "Facebook verified"
                cell.titleLabel.isHidden = false
                cell.fbButton.isHidden = true
                cell.scoreLabel.text = "+20"
                cell.scoreLabel.textColor = .green
            } else {
                cell.titleLabel.isHidden = true
                cell.fbButton.isHidden = false
                cell.scoreLabel.text = "0"
                cell.scoreLabel.textColor = .lightGray
            }
            cell.iconImageView.setFAIconWithName(icon: .FAFacebook, textColor: .blue, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40, height: 40))
            cell.fbButton.rx.tap
                .subscribe(onNext: { [unowned self] in
                    self.handleFacebookLogin()
                }).disposed(by: self.disposeBag)
            break
        case 1:
            cell.titleLabel.text = "Email verified"
            if self.user.verified {
                cell.scoreLabel.text = "+10"
                cell.scoreLabel.textColor = .green
            } else {
                cell.scoreLabel.text = "0"
                cell.scoreLabel.textColor = .lightGray
            }
            cell.iconImageView.setFAIconWithName(icon: .FAFileText, textColor: .blue, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40, height: 40))
            break
        case 2:
            cell.titleLabel.text = "Phone verified"
            if self.user.verified {
                cell.scoreLabel.text = "+10"
                cell.scoreLabel.textColor = .green
            } else {
                cell.scoreLabel.text = "0"
                cell.scoreLabel.textColor = .lightGray
            }
            cell.iconImageView.setFAIconWithName(icon: .FAPhone, textColor: .blue, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40, height: 40))
            break
//        case 3:
//            cell.titleLabel.text = "Items sold"
//            if let score = self.user.score {
//                cell.scoreLabel.text = score
//            } else {
//                cell.scoreLabel.text = "0"
//            }
//            cell.iconImageView.setFAIconWithName(icon: .FATags, textColor: .blue, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40, height: 40))
//            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            
            break
        case 1:
            
            break
        case 2:
            
            break
        case 3:
            
            break
        default:
            break
        }
    }
    
    func handleFacebookLogin() {
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                debugPrint("fb logIn, error:\(error)")
            }
            guard let result = result else {
                return
            }
            if result.isCancelled {
                debugPrint("fb logIn, cancelled:\(result)")
            } else {
                debugPrint("fb logIn, result:\(result.token.userID)")
                guard let fbToken = result.token.tokenString, let fbUserId = result.token.userID else {
                    return
                }
                debugPrint("self.user.id:\(self.user.id)")
                self.updateUserFacebookGraph()
            }
        }
    }
    
    func updateUserFacebookGraph() {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
            debugPrint("FBSDKGraphRequest conn:\(connection), result:\(result), error:\(error)")
            guard let graph = result as? [String: Any], let id = graph["id"] as? String, let email = graph["email"] as? String else {
                return
            }
            let updateUserInput = UpdateUserInput(id: self.user.id, facebookId: id)
            UserServicesManager.shared.editUser(input: updateUserInput).done {
                debugPrint("editUser facebook done")
                }.ensure {
                    UserServicesManager.shared.getUserForId(id: self.user.id).done { user in
                        self.user = user
                    }.ensure {
                        self.tableView.reloadData()
                    }.catch { error in
                        debugPrint("getUserForId error:\(error)")
                    }
                }.catch { error in
                    debugPrint("editUser facebook error:\(error)")
            }
        })
    }
}

//extension ScoreViewController: FBSDKLoginButtonDelegate {
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//        debugPrint("login result:\(result.token) :\(result)")
//        guard result.token != nil else {
//            return
//        }
//        guard let fbToken = result.token.tokenString, let fbUserId = result.token.userID else {
//            return
//        }
//        debugPrint("FB login didComplete token:\(fbToken), userId:\(fbUserId), error:\(error)")
//
//        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
//            debugPrint("FBSDKGraphRequest conn:\(connection), result:\(result), error:\(error)")
//            guard let graph = result as? [String: Any], let id = graph["id"] as? String, let email = graph["email"] as? String else {
//                return
//            }
//            let updateUserInput = UpdateUserInput(id: self.user.id, facebookId: id)
//            UserServicesManager.shared.editUser(input: updateUserInput).done {
//
//                }.ensure {
//
//                }.catch { error in
//
//            }
//        })
//    }
//
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//
//    }
//}
