//
//  SettingsViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/23/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSMobileClient
import FBSDKLoginKit

class SettingsViewController: UIViewController {
    
    enum SettingsType {
        case bio
        case push
        case privacy
        case terms
        case signout
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(SettingsViewController.close))
        closeButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = closeButton
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationItem.title = "Settings"
        
        let height = self.view.frame.size.height - 120
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    func signout() {
        let alertController = UIAlertController(title: "", message: "Sign Out", preferredStyle: .actionSheet)
        let signOutAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            FBSDKLoginManager().logOut()
            AWSMobileClient.sharedInstance().signOut()
            self.dismiss(animated: true, completion: {
                Common.showOnboarding(mainNavigationController: nil)
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(signOutAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func handlePushNotificationToggle(sender: UISwitch) {
        debugPrint("switch isOn:\(sender.isOn)")
        if sender.isOn {
            PushNotifications.registerForPushNotifications()
        } else {
            DispatchQueue.main.async {
                sender.setOn(true, animated: true)
            }
            Common.showConfirmationMessage(title: "", message: "Turn off Push Notifications?", theme: .warning, style: .center) {
                UIApplication.shared.unregisterForRemoteNotifications()
                DispatchQueue.main.async {
                    sender.setOn(false, animated: true)
                }
            }
        }
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingsCell.reuseIdentifier, for: indexPath) as! SettingsCell
        
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "Push notifications"
            cell.toggleSwitch.isHidden = false
            cell.toggleSwitch.addTarget(self, action: #selector(SettingsViewController.handlePushNotificationToggle(sender:)), for: .valueChanged)
            PushNotifications.notificationsAuthorized { (result) in
                debugPrint("notificationsAuthorized: \(result)")
                DispatchQueue.main.async {
                    cell.toggleSwitch.setOn(result, animated: true)
                }
            }
            break
        case 1:
            cell.titleLabel.text = "Privacy"
            break
        case 2:
            cell.titleLabel.text = "Terms of service"
            break
        case 3:
            cell.iconImageView.setFAIconWithName(icon: .FASignOut, textColor: .black, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40.0, height: 40.0))
            cell.titleLabel.text = "Sign out"
            break
        case 4:
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                cell.titleLabel.text = "Version \(version)"
            }
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.row == 0 || indexPath.row == 4 {
            return nil
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            //// Push row
            break
        case 1:
            let webViewController = SettingsWebViewController()
            self.navigationController?.pushViewController(webViewController, animated: true)
            break
        case 2:
            let webViewController = SettingsWebViewController()
            self.navigationController?.pushViewController(webViewController, animated: true)
            break
        case 3:
            signout()
            break
        default:
            break
        }
    }
    
}
