//
//  MenuViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/2/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSMobileClient
import SwiftMessages
import NVActivityIndicatorView
import FBSDKLoginKit
import MessageUI

enum MenuType {
    case feed
    case post
    case profile
    case userPost
    case userPostProfile
}

class MenuViewController: UIViewController {
    
    enum UserProfileMenuItems: String {
        case report = "Report"
    }
    
    enum ProfileMenuItems: String {
        case settings = "Settings"
        case signOut = "Sign Out"
    }
    
    enum ProfileSettingsMenuItems: String {
        case name = "Name"
        
    }
    
    enum UserPostMenuItems: String {
        case share = "Share"
        case report = "Report"
    }
    
    enum PostMenuItems: String {
        case share = "Share"
        case promote = "Promote"
        case unpublish = "Unpublish"
        case sold = "Mark Sold"
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var type: MenuType = .profile
    
    var post: Post!
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(MenuViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        self.navigationController?.navigationBar.barTintColor = .white
        
        self.navigationItem.title = ""
        
        // Need to set this so SwiftMessagesSegue displays properly
        switch self.type {
        case .userPost:
            preferredContentSize = CGSize(width: 300, height: 180)
            break
        case .userPostProfile:
            preferredContentSize = CGSize(width: 300, height: 120)
            break
        default:
            preferredContentSize = CGSize(width: 300, height: 300)
            break
        }
        
        guard self.post != nil else {
            return
        }
        PostServicesManager.shared.getPost(id: self.post.id).done { post in
            debugPrint("fetchPost promoteCount:\(post.promotedCount)")
            }.ensure {
                self.tableView.reloadData()
            }.catch { error in
                debugPrint("fetchPost error:\(error)")
        }
    }

    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    func sendEmailSupport() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["hello@airya.co"])
            if let title = post.title {
                mail.setMessageBody("<p>Reporting this post \(title) id:\(post.id) </p>", isHTML: true)
            } else {
                mail.setMessageBody("<p>Reporting this post </p>", isHTML: true)
            }
            present(mail, animated: true)
        } else {
            // show failure alert
            Common.showNoticeMessage(title: "Error", message: "Can't send mail", theme: .error, style: .center)
        }
    }
    
    func handlePromotePost() {
        Common.showConfirmationMessage(title: "Promote", message: "Promote this post?", theme: .warning, style: .center) {
            let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballPulse, color: Common.primaryColor, padding: 1.0)
            self.view.addSubview(activity)
            activity.startAnimating()
            
            if let post = self.post {
                var updatePostInput: UpdateUserPostInput!
                PostServicesManager.shared.getPost(id: post.id).done { post in
                    self.post = post
                    let promotedAt = Common.createdAtDate()
                    var salePrice: String?
                    if let count = post.promotedCount {
                        //// User has used promote post at least once
                        if var promotedCount = Int(count) {
                            promotedCount += 1
                            if let price = post.price, let priceValue = Float(price) {
                                var salePriceValue: Float!
                                if let postSalePrice = post.salePrice, let postSalePriceValue = Float(postSalePrice) {
                                    salePriceValue = postSalePriceValue / 1.10
                                    salePrice = Common.handlePriceFormat(price: salePriceValue)
                                    debugPrint("salePrice:\(salePriceValue), salePrice:\(salePrice)")
                                    //// Don't update if sale price below threshold
                                    if salePriceValue <= 0 {
                                        debugPrint("salePrice below threshold")
                                        Common.showNoticeMessage(title: "Promote price", message: "Promote price below threshold", theme: .warning, style: .center)
                                        return
                                    }
                                } else {
                                    salePriceValue = priceValue / 1.10
                                    salePrice = Common.handlePriceFormat(price: salePriceValue)
                                    debugPrint("salePrice:\(salePriceValue), salePrice:\(salePrice)")
                                }
                            }
                            updatePostInput = UpdateUserPostInput(id: post.id, salePrice: salePrice, promotedAt: promotedAt, promotedCount: String(promotedCount))
                        } else {
                            updatePostInput = UpdateUserPostInput(id: post.id, promotedAt: promotedAt, promotedCount: "0")
                        }
                    } else {
                        //// User hasn't used promote post yet
                        if let price = post.price, let priceValue = Float(price) {
                            let salePriceValue = priceValue / 1.10
                            salePrice = Common.handlePriceFormat(price: salePriceValue)
                            debugPrint("salePrice:\(salePriceValue), salePrice:\(salePrice)")
                            //// Don't update if sale price below threshold
                            if salePriceValue <= 0 {
                                debugPrint("salePrice below threshold")
                                Common.showNoticeMessage(title: "Promote price", message: "Promote price below threshold", theme: .warning, style: .center)
                                return
                            }
                        }
                        updatePostInput = UpdateUserPostInput(id: post.id, salePrice: salePrice, promotedAt: promotedAt, promotedCount: "1")
                    }
                    }.then {
                        PostServicesManager.shared.editPost(input: updatePostInput, file: nil, file2: nil, file3: nil, file4: nil)
                    }.done { updatedPost in
                        debugPrint("editPost promotedAt")
                    }.ensure {
                        activity.removeFromSuperview()
                        activity.stopAnimating()
                        Common.showNoticeMessage(title: "", message: "Post promotedAt changed", theme: .success, style: .center)
                        self.dismiss(animated: true, completion: nil)
                    }.catch { error in
                        debugPrint("editPost promotedAt error:\(error)")
                }
            }
        }
    }
    
    func handlePublishStatus() {
        Common.showConfirmationMessage(title: "Publish", message: "Change publish status?", theme: .warning, style: .center) {
            if let isPublished = self.post.isPublished {
                var isPublishedInput = false
                if isPublished {
                    isPublishedInput = false
                    debugPrint("mark not published")
                } else {
                    debugPrint("mark published")
                    isPublishedInput = true
                }
                let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballPulse, color: Common.primaryColor, padding: 1.0)
                self.view.addSubview(activity)
                activity.startAnimating()
                
                let updatePostInput = UpdateUserPostInput(id: self.post.id, isPublished: isPublishedInput)
                PostServicesManager.shared.editPost(input: updatePostInput, file: nil, file2: nil, file3: nil, file4: nil).done { updatedPost in
                    debugPrint("editPost isPublishedInput:\(isPublishedInput)")
                    }.ensure {
                        activity.removeFromSuperview()
                        activity.stopAnimating()
                        Common.showNoticeMessage(title: "", message: "Post state changed", theme: .success, style: .center)
                        self.dismiss(animated: true, completion: nil)
                    }.catch { error in
                        debugPrint("editPost error:\(error)")
                }
            }
        }
    }
    
    func handleSoldStatus() {
        Common.showConfirmationMessage(title: "Sold", message: "Change sold status?", theme: .warning, style: .center) {
            if let isSold = self.post.isSold {
                var isSoldInput = false
                if isSold {
                    isSoldInput = false
                    debugPrint("mark not sold")
                } else {
                    debugPrint("mark sold")
                    isSoldInput = true
                }
                let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballPulse, color: Common.primaryColor, padding: 1.0)
                self.view.addSubview(activity)
                activity.startAnimating()
                
                let updatePostInput = UpdateUserPostInput(id: self.post.id, isSold: isSoldInput)
                PostServicesManager.shared.editPost(input: updatePostInput, file: nil, file2: nil, file3: nil, file4: nil).done { updatedPost in 
                    debugPrint("editPost isSoldInput:\(isSoldInput)")
                    }.ensure {
                        activity.removeFromSuperview()
                        activity.stopAnimating()
                        Common.showNoticeMessage(title: "", message: "Post state changed", theme: .success, style: .center)
                        self.dismiss(animated: true, completion: nil)
                    }.catch { error in
                        debugPrint("editPost error:\(error)")
                }
            }
        }
    }
    
    func showSettings() {
        let settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
    
    func handleSignout() {
        let alertController = UIAlertController(title: "", message: "Sign Out", preferredStyle: .actionSheet)
        let signOutAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            FBSDKLoginManager().logOut()
            AWSMobileClient.sharedInstance().signOut()
            self.dismiss(animated: true, completion: {
                Common.showOnboarding(mainNavigationController: nil)
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(signOutAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func handleShare() {
        // image to share
        if let image = self.image {
            // set up activity view controller
            let imageToShare = [ image ]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}

extension MenuViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        debugPrint("mailComposeController result:\(result), error:\(error)")
        self.dismiss(animated: true, completion: nil)
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
        case .feed:
            return 6
        case .post:
            return 4
        case .profile:
            return 2
        case .userPost:
            return 2
        case .userPostProfile:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.reuseIdentifier, for: indexPath) as! MenuCell
        
        switch type {
        case .feed:
            cell.titleLabel.text = "Category"
            break
        case .post:
            switch indexPath.row {
            case 0:
                cell.iconImageView.setFAIconWithName(icon: .FAShare, textColor: .black, orientation: .up, backgroundColor: .clear, size: CGSize(width: 40.0, height: 40.0))
                cell.titleLabel.text = PostMenuItems.share.rawValue
                break
            case 1:
                if let promoteCount = self.post.promotedCount {
                    cell.titleLabel.text = "\(PostMenuItems.promote.rawValue): \(promoteCount) used"
                } else {
                    cell.titleLabel.text = "\(PostMenuItems.promote.rawValue): 0 used"
                }
                break
            case 2:
                if let isPublished = self.post.isPublished {
                    if isPublished {
                        cell.titleLabel.text = PostMenuItems.unpublish.rawValue
                    } else {
                        cell.titleLabel.text = "Mark published"
                    }
                } else {
                    cell.titleLabel.text = PostMenuItems.unpublish.rawValue
                }
                break
            case 3:
                if let isSold = self.post.isSold {
                    if isSold {
                        cell.titleLabel.text = "Mark not sold"
                    } else {
                        cell.titleLabel.text = PostMenuItems.sold.rawValue
                    }
                } else {
                    cell.titleLabel.text = PostMenuItems.sold.rawValue
                }
                break
            default:
                break
            }
            break
        case .profile:
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = ProfileMenuItems.settings.rawValue
                break
            case 1:
                cell.titleLabel.text = ProfileMenuItems.signOut.rawValue
                break
            default:
                break
            }
            
            break
        case .userPost:
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = UserPostMenuItems.share.rawValue
                break
            case 1:
                cell.titleLabel.text = UserPostMenuItems.report.rawValue
                break
            default:
                break
            }
            break
        case .userPostProfile:
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = UserProfileMenuItems.report.rawValue
                break
            default:
                break
            }
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch type {
        case .feed:
            debugPrint("category")
            break
        case .post:
            switch indexPath.row {
            case 0:
                debugPrint("share")
                handleShare()
                break
            case 1:
                debugPrint("promote")
                //// Promote status
                handlePromotePost()
                break
            case 2:
                debugPrint("publish")
                //// Publish status
                handlePublishStatus()
                break
            case 3:
                //// Sold status
                handleSoldStatus()
                break
            default:
                break
            }
            break
        case .profile:
            switch indexPath.row {
            case 0:
                debugPrint("settings")
                showSettings()
                break
            case 1:
                debugPrint("signout")
                handleSignout()
                break
            default:
                break
            }
            
            break
        case .userPost:
            switch indexPath.row {
            case 0:
                debugPrint("share")
                handleShare()
                break
            case 1:
                debugPrint("report")
                sendEmailSupport()
                break
            default:
                break
            }
            break
        case .userPostProfile:
            switch indexPath.row {
            case 0:
                debugPrint("report")
                sendEmailSupport()
                break
            default:
                break
            }
            break
        }
    }
}
