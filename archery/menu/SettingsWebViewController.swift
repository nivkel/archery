//
//  SettingsWebViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/25/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class SettingsWebViewController: UIViewController {
    
    var webView: WKWebView!
    
    let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Loading", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.view.frame, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        loadPage()
    }

    func loadPage() {
        let urlString = "https://www.airya.co/"
        if let url = URL(string: urlString) {
            webView.load(URLRequest(url: url))
        }
        webView.allowsBackForwardNavigationGestures = true
        
    }
    
}

extension SettingsWebViewController: WKUIDelegate, WKNavigationDelegate {
    func webViewDidClose(_ webView: WKWebView) {
        debugPrint("WebView DidClose")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("WebView DidFail:\(error)")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        debugPrint("WebView DidStart")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("WebView DidFinish")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            return
        }
        print("url:\(url.absoluteString) scheme:\(String(describing: url.scheme)), host:\(String(describing: url.host))")
        guard url.scheme != nil else {
            return
        }
        // check redirect url matches
        let redirect = ""
        if url.host == redirect {
            print("Url opened:\(url)")
            
        }
        decisionHandler(.allow)
    }
}
