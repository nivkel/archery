//
//  PostDetailPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostDetailPresenter: ViewToPresenterPostDetailProtocol {
    var view: PresenterToViewPostDetailProtocol?
    
    var interactor: PresenterToInteractorPostDetailProtocol?
    
    var router: PresenterToRouterPostDetailProtocol?
    
    func startFetchPostDetail() {
        
    }
    
    func showPostDetailController(navigationController: UINavigationController) {
        
    }
}

extension PostDetailPresenter: InteractorToPresenterPostDetailProtocol {
    func postDetailFetchSuccess(response: Post?) {
        view?.postDetail(response: response)
    }
    
    func postDetailFetchFailed() {
        view?.showError()
    }
    
    
}
