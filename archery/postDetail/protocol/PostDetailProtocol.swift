//
//  PostDetailProtocol.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

protocol ViewToPresenterPostDetailProtocol: class {
    var view: PresenterToViewPostDetailProtocol? {get set}
    var interactor: PresenterToInteractorPostDetailProtocol? {get set}
    var router: PresenterToRouterPostDetailProtocol? {get set}
    func startFetchPostDetail()
    func showPostDetailController(navigationController:UINavigationController)
}

protocol PresenterToViewPostDetailProtocol: class {
    func postDetail(response: Post?)
    func showError()
}

protocol PresenterToRouterPostDetailProtocol: class {
    static func createModule()-> PostDetailPageViewController
    func pushPostDetailController(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorPostDetailProtocol: class {
    var presenter:InteractorToPresenterPostDetailProtocol? {get set}
    func fetchPostDetail()
}

protocol InteractorToPresenterPostDetailProtocol: class {
    func postDetailFetchSuccess(response: Post?)
    func postDetailFetchFailed()
}
