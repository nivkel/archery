//
//  PostDetailPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftMessages

class PostDetailPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var presenter: ViewToPresenterPostDetailProtocol?
    
    var pages = [UIViewController]()
    
    var swipeInteractionController: SwipeInteractionController?
    
    var cell: PostFeedCell!
     
    var post: Post!
    
    var isSeller: Bool = false
    
    var isFromUserProfile: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(PostDetailPageViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        
        self.navigationItem.title = post.title
        
        let image = UIImage(icon: .FAListAlt, size: CGSize(width: 30.0, height: 30.0))
        let menuButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(PostDetailPageViewController.showPostMenu(sender:)))
        menuButton.tintColor = Common.primaryColor
        self.navigationItem.setLeftBarButton(menuButton, animated: true)
        
        var fileObject1: AWSS3Object? = nil
        var fileObject2: AWSS3Object? = nil
        var fileObject3: AWSS3Object? = nil
        var fileObject4: AWSS3Object? = nil
        
        if let file1 = post.file {
            if file1.key == "_" {
                fileObject1 = nil
            } else {
                fileObject1 = AWSS3Object(key: file1.key, bucket: file1.bucket, region: file1.region)
            }
        }
        if let file2 = post.file2 {
            if file2.key == "_" {
                fileObject2 = nil
            } else {
                fileObject2 = AWSS3Object(key: file2.key, bucket: file2.bucket, region: file2.region)
            }
        }
        if let file3 = post.file3 {
            if file3.key == "_" {
                fileObject3 = nil
            } else {
                fileObject3 = AWSS3Object(key: file3.key, bucket: file3.bucket, region: file3.region)
            }
        }
        if let file4 = post.file4 {
            if file4.key == "_" {
                fileObject4 = nil
            } else {
                fileObject4 = AWSS3Object(key: file4.key, bucket: file4.bucket, region: file4.region)
            }
        }
        let fileObjects = [fileObject1, fileObject2, fileObject3, fileObject4]
        debugPrint("post detail:\(fileObjects)")
        
        var count = fileObjects.filter { $0 != nil }.count
        var fileObjectsFiltered = fileObjects.filter { $0 != nil }
        
        if count != 0 {
            count -= 1
        }
        
        let totalCount = count + 2 // +1 to account for zero index, +1 for post detail page
        
        let postDetailViewController = PostDetailRouter.mainstoryboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
        postDetailViewController.pageNumber = 0
        postDetailViewController.pageCount = totalCount
        
        self.pages.append(postDetailViewController)
        
        for index in 0...count {
            let postDetailPhotoViewController = PostDetailRouter.mainstoryboard.instantiateViewController(withIdentifier: "PostDetailPhotoViewController") as! PostDetailPhotoViewController
            postDetailPhotoViewController.file = fileObjectsFiltered[index]
            postDetailPhotoViewController.pageNumber = index + 1
            postDetailPhotoViewController.pageCount = totalCount
            
            self.pages.append(postDetailPhotoViewController)
        }
        self.setViewControllers([postDetailViewController], direction: .forward, animated: true, completion: nil)
        
        swipeInteractionController = SwipeInteractionController(viewController: self)
        
//        self.getScrollView()?.delegate = self
        let height = self.view.frame.size.height - 110
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showEditPost() {
        let postPageViewController = PostRouter.createModule()
        postPageViewController.existingPost = self.post
        let navigationController = UINavigationController(rootViewController: postPageViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .topCard)
        segue.perform()
    }
    
    @objc func showPostMenu(sender: Any) {
        let image = self.view.takeScreenshot()
        if isSeller {
            Common().showMenu(sender, delegate: self, type: .post, layout: .topCard, post: self.post, image: image)
        } else {
            Common().showMenu(sender, delegate: self, type: .userPost, layout: .topCard, post: self.post, image: image)
        }
    }
    
    func showOffer(sender: Any) {
        let offerViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "OfferViewController") as! OfferViewController
        offerViewController.post = self.post
        let navigationController = UINavigationController(rootViewController: offerViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
    
    func getScrollView() -> UIScrollView? {
        var scrollView: UIScrollView?
        for view in self.view.subviews {
            if let view = view as? UIScrollView {
                scrollView = view
                break
            }
        }
        return scrollView
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    func goToDetailPage() {
        let nextPage = self.pages[0]
        self.setViewControllers([nextPage], direction: .reverse, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case 0:
            return nil
        default:
            return self.pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case (self.pages.count - 1):
            return nil
        default:
            return self.pages[currentIndex + 1]
        }
    }
}

extension PostDetailPageViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

extension PostDetailPageViewController: PresenterToViewPostDetailProtocol {
    func postDetail(response: Post?) {
        
    }
    
    func showError() {
        
    }
}

//extension PostDetailPageViewController: UIScrollViewDelegate, PanelAnimationControllerDelegate {
//    func shouldHandlePanelInteractionGesture() -> Bool {
//        debugPrint("shouldHandlePanelInteractionGesture:\(self.getScrollView()!.contentOffset.y)")
//        return self.getScrollView()!.contentOffset.y == 0
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        debugPrint("scrollViewDidScroll:\(self.getScrollView()!.contentOffset.y)")
//        self.getScrollView()?.bounces = (self.getScrollView()!.contentOffset.y > 10)
//    }
//}

extension PostDetailPageViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return CustomPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let rect = CGRect(x: 0, y: 0, width: source.view.frame.width, height: source.view.frame.height)
        return CustomPresentTransitionController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let rect = CGRect(x: 0, y: 0, width: dismissed.view.frame.width, height: dismissed.view.frame.height)
        return CustomDismissTransitionController(interactionController: swipeInteractionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let animator = animator as? CustomDismissTransitionController,
            let interactionController = animator.interactionController,
            interactionController.interactionInProgress
            else {
                return nil
        }
        return interactionController
    }
}
