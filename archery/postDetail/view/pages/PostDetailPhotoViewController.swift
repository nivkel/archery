//
//  PostDetailPhotoViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAppSync
import Kingfisher
import PromiseKit

class PostDetailPhotoViewController: UIViewController {

    @IBOutlet weak var soldLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageControlView: UIView!
    
    @IBAction func offerButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        postDetailPageViewController.showOffer(sender: sender)
    }
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBAction func messageButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        debugPrint("createConversation postId:\(postDetailPageViewController.post)")
        firstly {
            ChatServicesManager.shared.getUserPayload()
            }.then { userPayload in
                ChatServicesManager.shared.handleConversationCreation(post: postDetailPageViewController.post, subject: "buy", senderName: userPayload["username"])
            }.done { userConversationId in
                debugPrint("handleConversationCreation userConversationId:\(userConversationId)")
            }.ensure {
                // Show messages for userConversationId
                self.dismiss(animated: true, completion: {
                    Common.showChatViewControllerTab()
                })
            }.catch { error in
                debugPrint("handleConversationCreation error:\(error)")
        }
    }
    
    @IBAction func detailsButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        postDetailPageViewController.goToDetailPage()
    }
    @IBAction func editButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        postDetailPageViewController.showEditPost()
    }
    @IBOutlet weak var editButton: UIButton!
    
    var file: AWSS3Object!
    var pageNumber: Int!
    var pageCount: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.scrollView.delegate = self
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapGest)
        
        editButton.layer.cornerRadius = 10.0
        messageButton.layer.cornerRadius = 10.0
        detailsButton.layer.cornerRadius = 10.0
        offerButton.layer.cornerRadius = 10.0
        soldLabel.layer.cornerRadius = 10.0
        soldLabel.clipsToBounds = true
        
        pageControlView.layer.cornerRadius = 10.0
        pageControlView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        
        self.pageControl.numberOfPages = pageCount
        
//        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
//            return
//        }
//        guard let file = postDetailPageViewController.post.file2 else {
//            return
//        }
//        let s3Object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region)
        ////
//        self.handleImageDownload()
        
        self.downloadImage(key: self.file.key) { (complete) in
            debugPrint("downloadImage complete:\(complete)")
        }
//        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: AWSLoginProvider.shared.identityPoolId, identityProviderManager: AWSLoginProvider.shared.pool)
//        debugPrint("credentialsProvider identityId: \(credentialsProvider.identityId)")
//
//        AWSLoginProvider.shared.getIdentityId { (identityId) in
//            let url = URL(fileURLWithPath: "private/\(identityId)/\(s3Object.getKeyName())")
//            AWSS3Manager.shared.downloadCompletionHandler = { (task, URL, data, error) -> Void in
//                debugPrint("Download transfer complete:\(task.progress), error:\(error)")
//                guard let imageData = data else {
//                    return
//                }
//                guard let image = UIImage(data: imageData) else {
//                    return
//                }
//                DispatchQueue.main.async {
//                    self.imageView.image = image
//                }
//            }
//            AWSS3Manager.shared.download(s3Object: s3Object, toURL: url) { (complete, error) in
//                debugPrint("AWSS3Manager download:\(complete)")
//            }
//        }
        
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        if postDetailPageViewController.isSeller {
//            self.offerButton.isEnabled = false
//            self.messageButton.isEnabled = false
//            self.offerButton.alpha = 0
//            self.messageButton.alpha = 0
            self.offerButton.isHidden = true
            self.messageButton.isHidden = true
            self.editButton.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pageControl.currentPage = pageNumber
        
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController, let post = postDetailPageViewController.post else {
            return
        }
        if let isSold = post.isSold {
            if isSold {
                soldLabel.isHidden = false
            } else {
                soldLabel.isHidden = true
            }
        }
    }
    
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    func downloadImage(key: String, completion: @escaping (Bool) -> Void) {
        let width = Int(self.imageView.frame.size.width) * 2
        let height = Int(self.imageView.frame.size.height) * 2
        debugPrint("imageView size height:\(height), width:\(width)")
        let fitInResolution = "\(width)x\(height)/"
        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
        debugPrint("s3 urlString:\(urlString)")
        guard let url = URL(string: urlString) else {
            return
        }
        debugPrint("s3 url:\(url)")
        let imageResource = ImageResource(downloadURL: url)
        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
        self.imageView.kf.indicatorType = .activity
        self.imageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
            debugPrint("Progress received:\(received), total:\(total)")
        }) { (result) in
            switch result {
            case .success(let value):
                // The image was set to image view:
                print(value.image)
                self.imageView.image = value.image
                // From where the image was retrieved:
                // - .none - Just downloaded.
                // - .memory - Got from memory cache.
                // - .disk - Got from disk cache.
                print(value.cacheType)
                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                // The source object which contains information like `url`.
                print(value.source)
                completion(true)
            case .failure(let error):
                print("error \(error)") // The error happens
                completion(true)
            }
        }
    }
    
    func handleImageDownload() {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        guard let post = postDetailPageViewController.post else {
            return
        }
        var fileObject1: AWSS3Object? = nil
        var fileObject2: AWSS3Object? = nil
        var fileObject3: AWSS3Object? = nil
        var fileObject4: AWSS3Object? = nil
        
        if let file1 = post.file {
            fileObject1 = AWSS3Object(key: file1.key, bucket: file1.bucket, region: file1.region)
        }
        if let file2 = post.file2 {
            fileObject2 = AWSS3Object(key: file2.key, bucket: file2.bucket, region: file2.region)
        }
        if let file3 = post.file3 {
            fileObject3 = AWSS3Object(key: file3.key, bucket: file3.bucket, region: file3.region)
        }
        if let file4 = post.file4 {
            fileObject4 = AWSS3Object(key: file4.key, bucket: file4.bucket, region: file4.region)
        }
        let fileObjects = [fileObject1, fileObject2, fileObject3, fileObject4]
        
        imageView.kf.indicatorType = .activity
        
        let group = DispatchGroup()
        for file in fileObjects {
            if let file = file {
                group.enter()
                DispatchQueue.main.async {
                    AWSS3Manager.shared.downloadImage(imageView: self.imageView, key: file.key) { (complete) in
                        group.leave()
                    }
                }
//                self.downloadImage(key: file.key) { (complete) in
//                    group.leave()
//                }
            }
        }
        group.notify(queue: .main) {
            debugPrint("file downloads complete")
        }
        
//        guard let file = postDetailPageViewController.post.file else {
//            return
//        }
//        let s3Object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region)
//
//        guard let key = s3Object.key else {
//            return
//        }
//        let width = Int(self.imageView.frame.size.width) * 2
//        let height = Int(self.imageView.frame.size.height) * 2
//        debugPrint("imageView size height:\(height), width:\(width)")
//        let fitInResolution = "\(width)x\(height)/"
//        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
//        debugPrint("s3 urlString:\(urlString)")
//        guard let url = URL(string: urlString) else {
//            return
//        }
//        debugPrint("s3 url:\(url)")
//        let imageResource = ImageResource(downloadURL: url)
//        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 30.0, height: 30.0), orientation: .up, textColor: UIColor.black, backgroundColor: UIColor.white)
//        self.imageView.kf.indicatorType = .activity
//        self.imageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
//            debugPrint("Progress received:\(received), total:\(total)")
//        }) { (result) in
//            switch result {
//            case .success(let value):
//                // The image was set to image view:
//                print(value.image)
//                self.imageView.image = value.image
//                // From where the image was retrieved:
//                // - .none - Just downloaded.
//                // - .memory - Got from memory cache.
//                // - .disk - Got from disk cache.
//                print(value.cacheType)
//                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
//                // The source object which contains information like `url`.
//                print(value.source)
//            case .failure(let error):
//                print("error \(error)") // The error happens
//            }
//        }
    }
}

extension PostDetailPhotoViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
