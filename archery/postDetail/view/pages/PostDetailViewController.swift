//
//  PostDetailViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import PromiseKit
import MapKit
import SwiftDate
import SwiftMessages
import NVActivityIndicatorView

class PostDetailViewController: UIViewController {
    
    @IBOutlet weak var soldLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageControlView: UIView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBAction func likeButtonAction(_ sender: UIButton) {
        guard let post = self.post else {
            return
        }
        firstly {
            ChatServicesManager.shared.getUserId()
        }.then { userId in
            PostServicesManager.shared.getLikeForUser(userId: userId, postId: post.id)
            }.get { object in
                debugPrint("likeExists:\(object.0)")
                if !object.0 {
                    self.likeButton.isEnabled = false
                    self.likeButton.alpha = 0.5
                    firstly {
                        PostServicesManager.shared.addUserLikeForPost(userId: object.1, postId: post.id)
                        }.get {
                            debugPrint("addUserLikeForPost success")
                        }.then {
                            PostServicesManager.shared.getPostLikes(postId: post.id)
                        }.get { count in
                            self.likeCountLabel.text = "\(count)"
                        }.ensure {
                            self.likeButton.isEnabled = true
                            self.likeButton.alpha = 1.0
                        }.catch { error in
                            debugPrint("addUserLikeForPost error:\(error)")
                    }
                } else {
                    guard let likeId = object.2 else {
                        debugPrint("no likeId")
                        return
                    }
                    debugPrint("likeId:\(likeId)")
                    self.likeButton.isEnabled = false
                    self.likeButton.alpha = 0.5
                    firstly {
                        PostServicesManager.shared.removeUserLikeForPost(id: likeId)
                        }.get {
                            debugPrint("removeUserLikeForPost success")
                        }.then {
                            PostServicesManager.shared.getPostLikes(postId: post.id)
                        }.get { count in
                            self.likeCountLabel.text = "\(count)"
                        }.ensure {
                            self.likeButton.isEnabled = true
                            self.likeButton.alpha = 1.0
                        }.catch { error in
                            debugPrint("removeUserLikeForPost error")
                    }
                }
            }.ensure {
            
        }.catch { error in
                
        }
    }
    @IBOutlet weak var offerButton: UIButton!
    @IBAction func offerButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        postDetailPageViewController.showOffer(sender: sender)
    }
    
    @IBOutlet weak var messageButton: UIButton!
    @IBAction func messageActionButton(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        debugPrint("createConversation postId:\(postDetailPageViewController.post)")
        let type = 0
        var conversationId: String?
        firstly {
            ChatServicesManager.shared.getUserPayload()
            }.then { userPayload in
                ChatServicesManager.shared.handleConversationCreation(post: postDetailPageViewController.post, subject: "buy", senderName: userPayload["username"])
            }.done { userConversationId in
            debugPrint("handleConversationCreation userConversationId:\(userConversationId)")
                conversationId = userConversationId
        }.ensure {
            // Show messages for userConversationId
            self.dismiss(animated: true, completion: {
                Common.showChatViewControllerTab()
                if let conversationId = conversationId {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
                    Common.showChatroomView(navigationController: rootViewController, conversationId: conversationId, type: type)
                }
            })
        }.catch { error in
            debugPrint("handleConversationCreation error:\(error)")
        }
    }
    @IBOutlet weak var sellerImageView: UIImageView!
    @IBOutlet weak var sellerProfileButton: UIButton!
    @IBAction func sellerProfileButtonAction(_ sender: UIButton) {
        if let postDetailPageViewController = self.parent as? PostDetailPageViewController {
            if postDetailPageViewController.isSeller {
                debugPrint("This is your post")
                return
            }
        }
        let userProfileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        userProfileViewController.user = post?.user
        self.navigationController?.pushViewController(userProfileViewController, animated: true)
//        let profileNavigationController = UINavigationController(rootViewController: profileViewController)
//        self.present(profileNavigationController, animated: true, completion: nil)
    }
    @IBAction func editButtonAction(_ sender: UIButton) {
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        postDetailPageViewController.showEditPost()
    }
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var negotiableLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var willShipLabel: UILabel!
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var salePriceLabel: UILabel!
    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var negotiableIcon: UIImageView!
    @IBOutlet weak var shipIcon: UIImageView!
    @IBOutlet weak var pickupIcon: UIImageView!
    
    var post: Post? {
        get {
            guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
                return nil
            }
            return postDetailPageViewController.post
        }
    }
    var pageNumber: Int!
    var pageCount: Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        editButton.layer.cornerRadius = 10.0
        messageButton.layer.cornerRadius = 10.0
        offerButton.layer.cornerRadius = 10.0
        likeButton.layer.cornerRadius = 10.0
        sellerProfileButton.layer.cornerRadius = 10.0
        sellerImageView.layer.cornerRadius = 25.0
        sellerImageView.clipsToBounds = true
        soldLabel.layer.cornerRadius = 10.0
        soldLabel.clipsToBounds = true
    
        if let username = post?.user?.username {
            sellerImageView.setImageForName(Common.usernameInitials(value: username), gradientColors: (Common.primaryColor, Common.tertiaryColor), circular: true, textAttributes: nil)
        } else {
            sellerImageView.setFAIconWithName(icon: .FAUserCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
        }
        
        postImageView.clipsToBounds = true
        
        mapView.delegate = self
        mapView.register(UserMapView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        guard let post = self.post else {
            return
        }
        guard let title = post.title, let price = post.price, let content = post.content else {
            return
        }
        self.titleLabel.text = title
        self.priceLabel.text = "$\(price)"
        self.contentTextView.text = content
        
        if let file = post.file {
            debugPrint("post file key:\(file.key), bucket:\(file.bucket)")
            postImageView.kf.indicatorType = .activity
            AWSS3Manager.shared.downloadImage(imageView: postImageView, key: file.key) { (complete) in
                debugPrint("downloadImage:\(complete)")
            }
        }
        
        if let createdAt = post.createdAt {
            debugPrint("createdAt:\(createdAt)")
            self.createdAtLabel.text = "\(Common.createdAtRelativeDate(value: createdAt)) ago"
        }
        
        if let updatedAt = post.updatedAt {
            debugPrint("updatedAt:\(updatedAt)")
        }
        
        if let dateSold = post.dateSold {
            debugPrint("dateSold:\(dateSold)")
        }
        
        if let location = post.location, let category = post.category, let isNegotiable = post.isNegotiable, let willShip = post.willShip, let willMeet = post.willMeet, let tags = post.tags, let condition = post.condition {
            debugPrint("location:\(location)")
            
            self.categoryLabel.text = "Category: \(category)"
            self.negotiableLabel.text = "Negotiable"//: \(Common.boolToString(value: isNegotiable))"
            self.willShipLabel.text = "Shipping"//: \(Common.boolToString(value: willShip))"
            self.pickupLabel.text = "Pickup"//: \(Common.boolToString(value: willMeet))"
            self.conditionLabel.text = "Condition: \(condition)"
            
            if isNegotiable {
                self.negotiableIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            } else {
                self.negotiableIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            }
            if willShip {
                self.shipIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            } else {
                self.shipIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            }
            if willMeet {
                self.pickupIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            } else {
                self.pickupIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
            }
            
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(location) { (placemarks, error) in
                if let error = error {
                    debugPrint(error)
                }
                if let placemark = placemarks?.first {
                    var title = ""
                    var subtitle = ""
                    if let state = placemark.administrativeArea, let county = placemark.subAdministrativeArea {
                        title = "\(county), \(state)"
                    }
                    if let city = placemark.locality, let neighborhood = placemark.subLocality {
                        subtitle = "\(city), \(neighborhood)"
                    }
                    guard let lat = Double(location.split(separator: ",").first!) else {
                        return
                    }
                    guard let long = Double(location.split(separator: ",").last!) else {
                        return
                    }
                    let icon = UIImage(icon: .FACircle, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
                    
                    let annotation = UserMapAnnotation(title: title, name: subtitle, coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), image: icon.image(alpha: 0.5)!)
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.mapView.addAnnotation(annotation)
                    
                    let initialLocation = CLLocation(latitude: lat, longitude: long)
                    
                    let regionRadius: CLLocationDistance = 1000
                    let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,
                                                              latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
//                    self.mapView.setCenter(initialLocation.coordinate, animated: true)
                    let span = MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.05)
                    let region = MKCoordinateRegion(center: initialLocation.coordinate, span: span)
                    self.mapView.setRegion(region, animated: false)
                }
            }
        }
        
        firstly {
            PostServicesManager.shared.getPostLikes(postId: post.id)
            }.get { count in
                self.likeCountLabel.text = "\(count)"
            }.ensure {
                
            }.catch { error in
                
        }
        
        pageControlView.layer.cornerRadius = 10.0
        pageControlView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
        
        self.pageControl.numberOfPages = pageCount
        
        guard let postDetailPageViewController = self.parent as? PostDetailPageViewController else {
            return
        }
        if postDetailPageViewController.isSeller {
//            self.offerButton.isEnabled = false
//            self.messageButton.isEnabled = false
//            self.offerButton.alpha = 0
//            self.messageButton.alpha = 0
            self.likeButton.isHidden = true
            self.offerButton.isHidden = true
            self.messageButton.isHidden = true
            self.editButton.isHidden = false
        }
        if postDetailPageViewController.isFromUserProfile {
            self.sellerProfileButton.isEnabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pageControl.currentPage = pageNumber
        
        self.sellerProfileButton.setTitle(self.post?.user?.username, for: .normal)
        
        if let post = self.post, let isSold = post.isSold {
            if isSold {
                soldLabel.isHidden = false
            } else {
                soldLabel.isHidden = true
            }
            if let salePrice = post.salePrice, let price = post.price {
                salePriceLabel.isHidden = false
                let attributeString = NSMutableAttributedString(string: "$\(price)")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                attributeString.addAttribute(NSAttributedString.Key.strokeColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
                attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
                
                priceLabel.attributedText = attributeString
                salePriceLabel.text = "$\(salePrice) Sale"
            } else {
                salePriceLabel.isHidden = true
//                priceLabel.attributedText = nil
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension PostDetailViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let annotation = views.first?.annotation {
            mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        debugPrint("didSelect")
//        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
//        mapView.setRegion(region, animated: true)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        debugPrint("tapped")
    }
}
