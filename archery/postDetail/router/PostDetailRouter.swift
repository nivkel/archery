//
//  PostDetailRouter.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostDetailRouter: PresenterToRouterPostDetailProtocol {
    static func createModule() -> PostDetailPageViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "PostDetailPageViewController") as! PostDetailPageViewController
        
        let presenter: ViewToPresenterPostDetailProtocol & InteractorToPresenterPostDetailProtocol = PostDetailPresenter()
        let interactor: PresenterToInteractorPostDetailProtocol = PostDetailInteractor()
        let router:PresenterToRouterPostDetailProtocol = PostDetailRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushPostDetailController(navigationConroller: UINavigationController) {
        
    }
}
