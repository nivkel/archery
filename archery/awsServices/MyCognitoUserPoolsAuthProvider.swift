//
//  MyCognitoUserPoolsAuthProvider.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AWSAppSync
import AWSUserPoolsSignIn
import AWSMobileClient

class MyCognitoUserPoolsAuthProvider: AWSCognitoUserPoolsAuthProvider {
    func getLatestAuthToken() -> String {
        var token: String? = nil
        
//        token = AWSLoginProvider.shared.token().result as? String
//        debugPrint("AWSLoginProvider token:\(token)")
        
        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
//            debugPrint("getTokens:\(tokens)")
        }
        AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().currentUser()?.getSession().continueOnSuccessWith(block: { (task) -> Any? in
            token = task.result!.idToken!.tokenString
//            debugPrint("token:\(token)")
            return nil
        }).waitUntilFinished()
        
        if token != nil {
            return token!
        } else {
            return ""
        }
    }
}
