//
//  LoginProtocol.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

protocol ViewToPresenterLoginProtocol: class {
    var view: PresenterToViewLoginProtocol? {get set}
    var interactor: PresenterToInteractorLoginProtocol? {get set}
    var router: PresenterToRouterLoginProtocol? {get set}
    func startLogin(navigationController: UINavigationController)
    func showAnotherController(navigationController:UINavigationController)
}

protocol PresenterToViewLoginProtocol: class {
    func showLogin(response: LoginResponse)
    func showError()
}

protocol PresenterToRouterLoginProtocol: class {
    static func createModule()-> LoginViewController
    func pushToCoinbaseAccountScreen(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorLoginProtocol: class {
    var presenter:InteractorToPresenterLoginProtocol? {get set}
    func login(navigationController: UINavigationController)
}

protocol InteractorToPresenterLoginProtocol: class {
    func loginFetchedSuccess(response: LoginResponse)
    func loginFetchFailed()
}
