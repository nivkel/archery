//
//  LoginPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class LoginPresenter: ViewToPresenterLoginProtocol {
    var view: PresenterToViewLoginProtocol?
    var interactor: PresenterToInteractorLoginProtocol?
    var router: PresenterToRouterLoginProtocol?
    
    func startLogin(navigationController: UINavigationController) {
        interactor?.login(navigationController: navigationController)
    }
    
    func showAnotherController(navigationController: UINavigationController) {
        router?.pushToCoinbaseAccountScreen(navigationConroller: navigationController)
    }
}

extension LoginPresenter: InteractorToPresenterLoginProtocol {
    func loginFetchFailed() {
        view?.showError()
    }
    
    func loginFetchedSuccess(response: LoginResponse) {
        view?.showLogin(response: response)
    }
}
