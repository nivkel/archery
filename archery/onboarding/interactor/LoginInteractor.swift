//
//  LoginInteractor.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSFacebookSignIn

class LoginInteractor: PresenterToInteractorLoginProtocol {
    var presenter: InteractorToPresenterLoginProtocol?
    
    func login(navigationController: UINavigationController) {
        if !AWSSignInManager.sharedInstance().isLoggedIn {
            let config = AWSAuthUIConfiguration()
            config.enableUserPoolsUI = true
            config.addSignInButtonView(class: AWSFacebookSignInButton.self)
            config.backgroundColor = UIColor.black
            config.font = UIFont (name: "Helvetica Neue", size: 18)
            config.isBackgroundColorFullScreen = true
            config.canCancel = false
            
            AWSAuthUIViewController.presentViewController(with: navigationController, configuration: config, completionHandler: { (provider: AWSSignInProvider, error: Error?) in
                if error != nil {
                    print("Error occurred: \(String(describing: error))")
                    let response = LoginResponse()
                    self.presenter?.loginFetchedSuccess(response: response)
                } else {
                    // Sign in successful.
                    self.presenter?.loginFetchFailed()
                }
            })
        }
    }
}
