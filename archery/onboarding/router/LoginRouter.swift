//
//  LoginRouter.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class LoginRouter: PresenterToRouterLoginProtocol {
    static func createModule() -> LoginViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let presenter: ViewToPresenterLoginProtocol & InteractorToPresenterLoginProtocol = LoginPresenter()
        let interactor: PresenterToInteractorLoginProtocol = LoginInteractor()
        let router:PresenterToRouterLoginProtocol = LoginRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToCoinbaseAccountScreen(navigationConroller: UINavigationController) {
        
    }
}
