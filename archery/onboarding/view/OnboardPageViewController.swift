//
//  OnboardPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/28/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class OnboardPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let tutorialOneViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
        tutorialOneViewController.page = 0
        let tutorialTwoViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
        tutorialTwoViewController.page = 1
        tutorialTwoViewController.isLastPage = true
        let tutorialThreeViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
        tutorialThreeViewController.page = 2
        self.pages.append(tutorialOneViewController)
        self.pages.append(tutorialTwoViewController)
        self.pages.append(tutorialThreeViewController)
        
        self.setViewControllers([tutorialOneViewController], direction: .forward, animated: true, completion: nil)
        
        let appearance = UIPageControl.appearance(whenContainedInInstancesOf: [UIPageViewController.self])
        appearance.pageIndicatorTintColor = UIColor.black
        appearance.currentPageIndicatorTintColor = Common.primaryColor
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case 0:
            return nil
        default:
            return self.pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case (self.pages.count - 1):
            return nil
        default:
            return self.pages[currentIndex + 1]
        }
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let previous = previousViewControllers.map { $0 as! TutorialViewController }
        guard let parent = self.parent as? OnboardViewController else {
            return
        }
        guard let nextToLastPage = previous.last else {
            return
        }
        if nextToLastPage.isLastPage {
            DispatchQueue.main.async {
                parent.doneButton.isHidden = false
                parent.doneButton.alpha = 0.1
                UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                    parent.doneButton.alpha = 1.0
                }, completion: { (complete) in
                    
                })
            }
        }
        debugPrint("previousViewControllers:\(nextToLastPage.isLastPage) parent:\(parent)")
    }
}
