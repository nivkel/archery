//
//  OnboardViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/27/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class OnboardViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBAction func doneButtonAction(_ sender: UIButton) {
        showMain()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let height = self.view.frame.size.height - 110
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
        
    }
    
    func showMain() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        Common.showMain(mainNavigationController: rootViewController)
        Common.setTabBarIcons(navigationController: rootViewController)
        Common.setAppColors()
    }

}
