//
//  OnboardingPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/14/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var pages = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let tutorialOneViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
        self.pages.append(tutorialOneViewController)
        self.setViewControllers([tutorialOneViewController], direction: .forward, animated: true, completion: nil)
        
        let appearance = UIPageControl.appearance(whenContainedInInstancesOf: [UIPageViewController.self])
        appearance.pageIndicatorTintColor = UIColor.black
        appearance.currentPageIndicatorTintColor = Common.primaryColor
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case 0:
            return nil
        default:
            return self.pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case (self.pages.count - 1):
            return nil
        default:
            return self.pages[currentIndex + 1]
        }
    }

}
