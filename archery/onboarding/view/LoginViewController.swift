//
//  LoginViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSCore
import AWSMobileClient

class LoginViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    var presentor: ViewToPresenterLoginProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presentor?.startLogin(navigationController: self.navigationController!)
        debugPrint("LoggedIn: \(AWSSignInManager.sharedInstance().isLoggedIn)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension LoginViewController: PresenterToViewLoginProtocol {
    func showLogin(response: LoginResponse) {
        debugPrint("showLogin:\(response)")
        
         
    }
    
    func showError() {
        debugPrint("showLogin error")
    }
}
