//
//  TutorialViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/14/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var isLastPage: Bool = false
    
    var page: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        switch page {
        case 0:
            imageView.setFAIconWithName(icon: .FADollar, textColor: .white, orientation: .up, backgroundColor: .black, size: CGSize(width: 100.0, height: 100.0))
            break
        case 1:
            imageView.setFAIconWithName(icon: .FAShip, textColor: .white, orientation: .up, backgroundColor: .black, size: CGSize(width: 100.0, height: 100.0))
            break
        case 2:
            imageView.setFAIconWithName(icon: .FACar, textColor: .white, orientation: .up, backgroundColor: .black, size: CGSize(width: 100.0, height: 100.0))
            break
        default:
            break
        }
    }

}
