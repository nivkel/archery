//
//  OnboardingViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/14/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AWSMobileClient
import SwiftMessages
import NVActivityIndicatorView

class OnboardingViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let signInViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        let navigationController = UINavigationController(rootViewController: signInViewController)
//        self.present(navigationController, animated: true, completion: nil)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
        
//        AWSMobileClient.sharedInstance().showSignIn(navigationController: self.navigationController!) { (userState, error) in
//            debugPrint("showSignIn userState:\(userState), error:\(error)")
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
//            Common.showMain(mainNavigationController: rootViewController)
//        }
    }
    @IBOutlet weak var signUpButton: UIButton!
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        let signUpViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        
        let phoneViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "PhoneViewController") as! PhoneViewController
        let navigationController = UINavigationController(rootViewController: phoneViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
//        self.present(navigationController, animated: true, completion: nil)
    }
    @IBOutlet weak var facebookButton: UIButton!
    
    var fbLoginButton: FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fbLoginButton = FBSDKLoginButton(frame: self.facebookButton.frame)
        fbLoginButton.readPermissions = ["public_profile", "email"]
        fbLoginButton.delegate = self
        self.view.addSubview(fbLoginButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fbLoginButton.frame = self.facebookButton.frame
    }
    
    func showMain() {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
            Common.showMain(mainNavigationController: rootViewController)
        }
    }
}

extension OnboardingViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        debugPrint("login result:\(result.token) :\(result), error:\(error)")
        guard result.token != nil else {
            Common.showNoticeMessage(title: "Error", message: "Facebook Login error", theme: .error, style: .center)
            return
        }
        guard let fbToken = result.token.tokenString, let fbUserId = result.token.userID else {
            Common.showNoticeMessage(title: "Error", message: "Facebook Login error", theme: .error, style: .center)
            return
        }
        debugPrint("FB login didComplete token:\(fbToken), userId:\(fbUserId), error:\(error)")
        let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Logging in", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .gray, textColor: .white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
            debugPrint("FBSDKGraphRequest conn:\(connection), result:\(result), error:\(error)")
            guard let graph = result as? [String: Any], let id = graph["id"] as? String, let email = graph["email"] as? String else {
                return
            }
            let password = fbUserId
            //// signin or signup? check for facebook token on user table? check for user?
            AWSMobileClient.sharedInstance().signUp(username: email, password: password, userAttributes: ["email": email, "custom:from_facebook": "true", "custom:facebook_id": id], completionHandler: { (result, error) in
                print("User signUp: \(result?.signUpConfirmationState), error:\(error)")
                if let error = error as? AWSMobileClientError {
                    print("User exists, signin: \(error)")
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                        AWSMobileClient.sharedInstance().signIn(username: email, password: password, completionHandler: { (result, error) in
                            if let error = error as? AWSMobileClientError {
                                switch(error) {
                                case .notAuthorized(let message):
                                    print("invalidPassword:\(message)")
                                    DispatchQueue.main.async {
                                        Common.showNoticeMessage(title: "", message: "Cannot login with your facebook email", theme: .error, style: .center)
                                    }
                                    FBSDKLoginManager().logOut()
                                default:
                                    break
                                }
                            } else {
                                Common.showOnboard()
//                                self.showMain()
                            }
                            print("User signIn: \(result), error:\(error)")
                            NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                        })
                    default:
                        break
                    }
                } else {
                    print("User doesn't exist, signup")
                    AWSMobileClient.sharedInstance().signIn(username: email, password: password, completionHandler: { (result, error) in
                        print("User signIn after signup: \(result), error:\(error)")
                        Common.showOnboard()
//                        self.showMain()
                        
                        NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    })
                }
            })
            
//            ChatServicesManager.shared.getUser().done { user in
//                print("User exists, signin: \(user)")
//                AWSMobileClient.sharedInstance().signIn(username: email, password: password, completionHandler: { (result, error) in
//                    print("User signIn: \(result), error:\(error)")
//                    self.showMain()
//                })
//                }.ensure {
//
//                }.catch { error in
//                    print("User doesn't exist, signup: \(error.localizedDescription)")
//                    AWSMobileClient.sharedInstance().signUp(username: email, password: password, userAttributes: ["email": email, "custom:from_facebook": "true"], completionHandler: { (result, error) in
//                        print("User signUp: \(result), error:\(error)")
//                        self.showMain()
//                    })
//            }
        })
        
//        AWSMobileClient.sharedInstance().federatedSignIn(providerName: IdentityProvider.facebook.rawValue, token: fbToken) { (userState, error)  in
//            if let error = error {
//                print("Federated Sign In failed: \(error.localizedDescription)")
//            }
//            debugPrint("federatedSignIn:\(userState?.rawValue)")
//            guard let userState = userState else {
//                return
//            }
//            switch userState {
//            case .signedIn:
//                print("User is signed in.")
//                DispatchQueue.main.async {
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
//                    Common.showMain(mainNavigationController: rootViewController)
//                }
//                break
//            case .signedOut:
//                print("User is signed out.")
//                break
//            case .signedOutFederatedTokensInvalid:
//                print("User signedOutFederatedTokensInvalid.")
//                break
//            case .signedOutUserPoolsTokenInvalid:
//                print("User signedOutUserPoolsTokenInvalid.")
//                break
//            case .guest:
//                print("User guest.")
//                break
//            case .unknown:
//                print("User unknown.")
//                break
//            }
//        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        debugPrint("FB didLogOut")
    }
}
