//
//  LocationViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/13/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit

class LocationViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var zipCodeButton: UIButton!
    @IBAction func zipCodeButtonAction(_ sender: UIButton) {
        guard let zipCode = self.zipCodeTextField.text else {
            return
        }
        guard zipCode.count == 5 else {
            Common.showStatusMessage(title: "", message: "Enter a valid zip code", theme: .error, style: .center)
            return
        }
        let geocoder = CLGeocoder()
        let dic = [NSTextCheckingKey.zip: zipCode]
        geocoder.geocodeAddressDictionary(dic) { (placemark, error) in
            
            if((error) != nil){
                print(error!)
            }
            if let placemark = placemark?.first {
                let lat = placemark.location!.coordinate.latitude
                let long = placemark.location!.coordinate.longitude
                let icon = UIImage(icon: .FACircle, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
                let annotation = UserMapAnnotation(title: zipCode, name: "Location is approximate", coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), image: icon.image(alpha: 0.5)!)
                self.mapView.addAnnotation(annotation)
                
                let initialLocation = CLLocation(latitude: lat, longitude: long)
                
                self.centerMapOnLocation(location: initialLocation)
                
                if let coordinate = placemark.location?.coordinate {
                    let coordinateString = String(coordinate.latitude) + "," +  String(coordinate.longitude)
                    debugPrint("coordinateString:\(coordinateString)")
                    self.newLocation = coordinateString
                    
                    self.updateLocationButton.isEnabled = true
                    self.updateLocationButton.alpha = 1.0
                }
            }
            
        }
    }
    @IBAction func updateLocationButtonAction(_ sender: UIButton) {
        guard let newLocation = self.newLocation else {
            Common.showStatusMessage(title: "", message: "Hit locate", theme: .error, style: .center)
            return
        }
        UserServicesManager.shared.updateUserLocation(coordinate: newLocation).done {
            debugPrint("updateUserLocation done")
            }.ensure {
                self.dismiss(animated: true, completion: nil)
            }.catch { error in
                debugPrint("updateUserLocation error:\(error)")
        }
    }
    @IBOutlet weak var updateLocationButton: UIButton!
    
    var newLocation: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupKeyboardHandling(textField: self.zipCodeTextField, textView: nil, scrollView: self.scrollView)
        
        mapView.delegate = self
        mapView.register(UserMapView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 1.5)
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(LocationViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        
        zipCodeButton.layer.cornerRadius = 10.0
        updateLocationButton.layer.cornerRadius = 10.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.updateLocationButton.isEnabled = false
        self.updateLocationButton.alpha = 0.5
        self.zipCodeButton.isEnabled = false
        self.zipCodeButton.alpha = 0.5
    }

    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    let regionRadius: CLLocationDistance = 5000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: false)
    }
}

extension LocationViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
    }
}

extension LocationViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: updatedText)
        let validCharacters = allowedCharacters.isSuperset(of: characterSet)
        
        let maxLength = 5
        let newString = updatedText as NSString
        let validLength = newString.length <= maxLength
        
        if !validCharacters || newString.length < 5 {
            self.updateLocationButton.isEnabled = false
            self.updateLocationButton.alpha = 0.5
            self.zipCodeButton.isEnabled = false
            self.zipCodeButton.alpha = 0.5
        } else {
            self.zipCodeButton.isEnabled = true
            self.zipCodeButton.alpha = 1.0
        }
        
        return validCharacters && validLength
    }
}
