//
//  SearchViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import RxSwift
import RxCocoa
import PromiseKit

class SearchViewController: UIViewController {
    let disposeBag = DisposeBag()
    let searchController = UISearchController(searchResultsController: nil)
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()

    var data = [Post]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
        
        self.navigationItem.searchController = self.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(SearchViewController.close))
        closeButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = closeButton
        self.navigationItem.title = "Search"
        
        searchController.searchBar.tintColor = UIColor.darkGray
//        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search items"
//        searchController.searchBar.scopeButtonTitles = categories
        searchController.searchBar.delegate = self
        searchController.searchBar.rx.text.orEmpty
            .debounce(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter({ !$0.isEmpty })
            .subscribe(onNext: { [unowned self] query in
                debugPrint("searching query...:\(query)")
//                firstly {
//
//                }.map { posts in
//                    self.data = posts
//                }.ensure {
//                    self.adapter.performUpdates(animated: true, completion: { (complete) in
//
//                    })
//                }.catch { error in
//                    debugPrint("Search error:\(error)")
//                }
            }).disposed(by: self.disposeBag)
    }

    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        debugPrint("didScroll")
    }
}

extension SearchViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {
        
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension SearchViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.data
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        
        switch object {
        case is Any:
            return SearchSuggestSectionController()
        default:
            return PostFeedSectionController()
        }
        
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return UIView()
    }
}

extension SearchViewController: UISearchControllerDelegate {
    func didDismissSearchController(_ searchController: UISearchController) {
        print("DidDismissSearch")
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("selectedScopeButtonIndexDidChange")
    }
}
