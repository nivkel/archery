//
//  SearchSuggestSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 11/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class SearchSuggestSectionController: ListSectionController {

    var suggestions: ListDiffable!
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard var width = self.collectionContext?.containerSize.width else {
            return CGSize(width: 0, height: 100)
        }
        width = floor(width / 2)
        return CGSize(width: width, height: 200.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: SelectCategoryCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! SelectCategoryCell
        
        return cell
    }
    
    override func didUpdate(to object: Any) {
        
    }
    
    override func didSelectItem(at index: Int) {
        
    }
}
