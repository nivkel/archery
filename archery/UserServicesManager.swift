//
//  UserServicesManager.swift
//  archery
//
//  Created by Kelvin Lee on 1/14/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import PromiseKit

class UserServicesManager: NSObject {
    static let shared = UserServicesManager()
    
    func getUserForId(id: String) -> Promise<User> {
        return Promise { seal in
            let getUser = GetUserQuery(id: id)
            AWSDataService.shared.appSyncClient?.fetch(query: getUser, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                guard let userData = result?.data?.getUser else {
                    return
                }
                guard let verified = userData.verified else {
                    return
                }
                let user = User(id: userData.id, cognitoId: userData.cognitoId, username: userData.username, email: userData.email, location: userData.location, verified: verified, phoneVerified: userData.phoneVerified, score: userData.score, facebookId: userData.facebookId)
                seal.resolve(user, error)
            })
        }
    }
    
    func getUserScore(userId: String) -> Promise<String> {
        return Promise { seal in
            let getUserScore = GetUserScoreQuery(userId: userId)
            AWSDataService.shared.appSyncClient?.fetch(query: getUserScore, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("getUserScore:\(result)")
                guard let userData = result?.data?.getUserScore else {
                    return
                }
                guard let score = userData.score else {
                    return
                }
                seal.resolve(score, error)
            })
        }
    }
    
    func updateUserDeviceToken(deviceToken: String) -> Promise<()>  {
        return Promise { seal in
            firstly {
                ChatServicesManager.shared.getUserId()
                }.get { userId in
                    let updateUserInput = UpdateUserInput(id: userId, deviceToken: deviceToken)
                    let userMutation = UpdateUserMutation(input: updateUserInput)
                    AWSDataService.shared.appSyncClient?.perform(mutation: userMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                        debugPrint("updateUserDeviceToken update:\(update)")
                    }, conflictResolutionBlock: { (snapshot, mutation, result) in
                        debugPrint("updateUserDeviceToken snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
                    }, resultHandler: { (result, error) in
                        debugPrint("updateUserDeviceToken result:\(result?.data?.updateUser?.deviceToken), error:\(error)")
                        seal.resolve((), error)
                    })
                }.catch { error in
                    seal.reject(error)
            }
            
        }
    }
    
    func updateUserLocation(coordinate: String) -> Promise<()> {
        return Promise { seal in
            firstly {
                ChatServicesManager.shared.getUserId()
                }.get { userId in
                    let updateUserInput = UpdateUserInput(id: userId, location: coordinate)
                    let userMutation = UpdateUserMutation(input: updateUserInput)
                    AWSDataService.shared.appSyncClient?.perform(mutation: userMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                        debugPrint("updateUserDeviceToken update:\(update)")
                    }, conflictResolutionBlock: { (snapshot, mutation, result) in
                        debugPrint("updateUserDeviceToken snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
                    }, resultHandler: { (result, error) in
                        debugPrint("updateUserDeviceToken result:\(result?.data?.updateUser?.location), error:\(error)")
                        seal.resolve((), error)
                    })
                }.catch { error in
                    seal.reject(error)
            }
        }
    }
    
    func editUser(input: UpdateUserInput) -> Promise<()> {
        return Promise { seal in
            let userMutation = UpdateUserMutation(input: input)
            AWSDataService.shared.appSyncClient?.perform(mutation: userMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("editUser update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("editUser snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("editUser result:\(result?.data?.updateUser), error:\(error)")
                seal.resolve((), error)
            })
        }
    }
}
