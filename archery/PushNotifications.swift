//
//  Notifications.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import UserNotifications

struct PushNotifications {
    static func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            
            let viewAction = UNNotificationAction(identifier: "VIEW_ACTION_IDENTIFIER", title: "View", options: [.foreground])
            let messageCategory = UNNotificationCategory(identifier: "MESSAGE_CATEGORY_IDENTIFIER", actions: [viewAction], intentIdentifiers: [], options: [])
            let discoveryCategory = UNNotificationCategory(identifier: "DISCOVER_CATEGORY_IDENTIFIER", actions: [viewAction], intentIdentifiers: [], options: [])
            let marketingCategory = UNNotificationCategory(identifier: "MARKETING_CATEGORY_IDENTIFIER", actions: [viewAction], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([messageCategory, discoveryCategory, marketingCategory])
            
            self.getNotificationSettings()
        }
    }
    
    static func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async(execute: {
                UIApplication.shared.registerForRemoteNotifications()
                debugPrint("registerForRemoteNotifications")
            })
            
        }
    }
    
    static func notificationsAuthorized(completion: @escaping (_ result: Bool) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            
            guard settings.authorizationStatus == .authorized else {
                completion(false)
                return
            }
            completion(true)
        }
    }
}
