//
//  AppDelegate.swift
//  archery
//
//  Created by Kelvin Lee on 10/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSCore
import AWSPinpoint
import AWSMobileClient
import AWSAppSync
//import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var pinpoint: AWSPinpoint?
    var appSyncClient: AWSAppSyncClient?
    var dataService: AWSDataService?
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        debugPrint("didRegisterForRemoteNotificationsWithDeviceToken: \(token)")
        pinpoint?.notificationManager.interceptDidRegisterForRemoteNotifications(withDeviceToken: deviceToken)
        
        UserServicesManager.shared.updateUserDeviceToken(deviceToken: token).catch { error in
            debugPrint("updateUserDeviceToken:\(error)")
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("user clicked on the notification")
        let userInfo = response.notification.request.content.userInfo
        guard let aps = userInfo["aps"] as? [String: Any] else {
            completionHandler()
            return
        }
//        if response.actionIdentifier == "VIEW_ACTION_IDENTIFIER" {
//
//        }
        if let category = aps["category"] as? String {
            if category == "MESSAGE_CATEGORY_IDENTIFIER" {
                if let data = userInfo["data"] as? [String: Any], let jsonBody = data["jsonBody"] as? [String: Any], let conversationId = jsonBody["conversationId"] as? String, let subject = jsonBody["subject"] as? String {
                    Common.showChatViewControllerTab()
                    var type: Int = 0
                    if subject == "buy" {
                        type = 0
                    } else if subject == "sell" {
                        type = 1
                    }
                    let rootViewController = self.window?.rootViewController as! UINavigationController
                    Common.showChatroomView(navigationController: rootViewController, conversationId: conversationId, type: type)
                }
            } else if category == "DISCOVER_CATEGORY_IDENTIFIER" {
                Common.showTab(index: 1)
            } else if category == "MARKETING_CATEGORY_IDENTIFIER" {
                Common.showChatViewControllerTab()
            }
        }
        print("user clicked on the notification:\(userInfo)")
//        let alert = UIAlertController(title: "Notification Received userNotificationCenter", message: userInfo.description, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(
//            alert, animated: true, completion:nil)
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        pinpoint?.notificationManager.interceptDidReceiveRemoteNotification(userInfo, fetchCompletionHandler: completionHandler)
        
        guard let aps = userInfo["aps"] as? [String: Any] else {
            completionHandler(.failed)
            return
        }
        if let category = aps["category"] as? String {
            if category == "MESSAGE_CATEGORY_IDENTIFIER" {
                if let data = userInfo["data"] as? [String: Any], let jsonBody = data["jsonBody"] as? [String: Any], let conversationId = jsonBody["conversationId"] as? String, let subject = jsonBody["subject"] as? String {
                    if let badgeCount = aps["badge"] as? String {
                        let value = UserDefaults.standard.integer(forKey: "\(subject)Subject")
                        if let count = Int(badgeCount) {
                            let newValue = value + count
                            debugPrint("badgeCount value:\(value), newValue:\(newValue)")
                            UserDefaults.standard.set(newValue, forKey: "\(subject)Subject")
                            
                            Common.setTotalTabBarBadge(application: application)
                        }
                        
                    }
                }
            } else if category == "DISCOVER_CATEGORY_IDENTIFIER" {
                Common.setTabBarBadgeCount(tab: 1, value: "1")
            } else if category == "MARKETING_CATEGORY_IDENTIFIER" {
                
            }
        }
        
        switch application.applicationState {
        case .active:
            debugPrint("didReceiveRemoteNotification active: \(userInfo)")
            break
        case .background:
            debugPrint("didReceiveRemoteNotification background: \(userInfo)")
            break
        case .inactive:
            debugPrint("didReceiveRemoteNotification inactive: \(userInfo)")
            break
        }
//        let alert = UIAlertController(title: "Notification Received didReceiveRemoteNotification", message: userInfo.description, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion:nil)
        completionHandler(.newData)
    }
    
    // Add an AWSMobileClient call in application:open url
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return AWSMobileClient.sharedInstance().interceptApplication(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation] as Any)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
        
        let mainNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        mainNavigationController.navigationBar.isHidden = true
        self.window?.rootViewController = mainNavigationController
        self.window?.makeKeyAndVisible()
        
        //// Firebase
//        FirebaseApp.configure()
//
//        GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544~1458002511")
        ////
        
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                print("UserState: \(userState.rawValue)")
                switch userState {
                case .signedIn:
                    break
                case .signedOut:
                    break
                case .signedOutFederatedTokensInvalid:
                    break
                case .signedOutUserPoolsTokenInvalid:
                    break
                case .guest:
                    break
                case .unknown:
                    break
                }
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
        }

        dataService = AWSDataService()
        let configuration = AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: launchOptions)
//        configuration.debug = true
        pinpoint = AWSPinpoint(configuration:
            configuration)
        
//        let mainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
//        let postFeedViewController = PostFeedRouter.createModule()
//        let postFeedNavigationController = UINavigationController(rootViewController: postFeedViewController)
//        postFeedNavigationController.tabBarItem.title = "Feed"
//        let postPageViewController = PostRouter.createModule()
//
//        let discoverViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DiscoverViewController") as! DiscoverViewController
//
//        let conversationsPageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConversationsPageViewController") as! ConversationsPageViewController
//        let conversationsNavigationController = UINavigationController(rootViewController: conversationsPageViewController)
//        conversationsNavigationController.tabBarItem.title = "Messages"
//
//        let profileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//        let profileNavigationController = UINavigationController(rootViewController: profileViewController)
//        profileNavigationController.tabBarItem.title = "You"
//        mainTabBarController.viewControllers = [postFeedNavigationController, discoverViewController, postPageViewController, conversationsNavigationController, profileNavigationController]
//        mainNavigationController.setViewControllers([mainTabBarController], animated: true)
        
        if AWSMobileClient.sharedInstance().isSignedIn {
            Common.showMain(mainNavigationController: mainNavigationController)
//            // Check if launched from notification
//            let notificationOption = launchOptions?[.remoteNotification]
//
//            // 1
//            if let notification = notificationOption as? [String: AnyObject],
//                let aps = notification["aps"] as? [String: AnyObject] {
//                // 2
//
//            }
            ChatServicesManager.shared.getUserId().then { userId in
                UserServicesManager.shared.getUserScore(userId: userId)
            }.done { score in
                debugPrint("user score:\(score)")
            }.catch { error in
                debugPrint("user score error:\(error)")
            }
        } else {
            Common.showOnboarding(mainNavigationController: mainNavigationController)
        }
        Common.setTabBarIcons(navigationController: mainNavigationController)
        Common.setAppColors()
        Common.setTotalTabBarBadge(application: application)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if let targetingClient = pinpoint?.targetingClient {
            targetingClient.addAttribute(["science", "politics", "travel"], forKey: "interests")
            targetingClient.updateEndpointProfile()
            let endpointId = targetingClient.currentEndpointProfile().endpointId
            print("Updated custom attributes for endpoint: \(endpointId)")
        }

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

