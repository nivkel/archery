//
//  AWSDataService.swift
//  archery
//
//  Created by Kelvin Lee on 10/15/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AWSCore
import AWSAppSync
import AWSS3
//import ImagePicker
import AWSMobileClient
import JWTDecode

class AWSDataService {
    static let shared = AWSDataService()
    // AWS AppSync Client
    var appSyncClient: AWSAppSyncClient?
    let databaseURL = URL(fileURLWithPath:NSTemporaryDirectory()).appendingPathComponent("appsync.db")
    let url = URL(string: "https://3flgrreksrchpi2snphruggnqu.appsync-api.us-east-1.amazonaws.com/graphql")!
    let region = AWSRegionType.USEast1
    let credentialsProviderPoolId = "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6"
    let userPoolsAuthProvider: AWSCognitoUserPoolsAuthProvider!
    let s3Manager: AWSS3ObjectManager!
    
    static let bucket = "archerypostmedia"
    
    var discard: Cancellable?
    
    var posts = [Post]()
    
    init() {
        do {
            // Initialize the AWS AppSync configuration
            s3Manager = AWSS3Manager.shared
            userPoolsAuthProvider = MyCognitoUserPoolsAuthProvider()
            
//            let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: AWSLoginProvider.shared.identityPoolId, identityProviderManager: AWSLoginProvider.shared)
            
            let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: AWSLoginProvider.shared.identityPoolId)
            let config = try AWSAppSyncClientConfiguration(url: url, serviceRegion: region, credentialsProvider: AWSMobileClient.sharedInstance(), databaseURL: databaseURL, s3ObjectManager: s3Manager)
            
            let workingConfig = try AWSAppSyncClientConfiguration(url: url, serviceRegion: region, userPoolsAuthProvider: userPoolsAuthProvider, databaseURL: databaseURL, s3ObjectManager: s3Manager)
            
            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncClientInfo: AWSAppSyncClientInfo(),
                                                userPoolsAuthProvider: {
                                                                    class NewCognitoUserPoolsAuthProvider : AWSCognitoUserPoolsAuthProviderAsync {
                                                                        func getLatestAuthToken(_ callback: @escaping (String?, Error?) -> Void) {
                                                                            AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
                                                                                if error != nil {
                                                                                    callback(nil, error)
                                                                                } else {
                                                                                    callback(tokens?.idToken?.tokenString, nil)
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    return NewCognitoUserPoolsAuthProvider()}(),
                                                                  databaseURL:databaseURL)
            
            //// For S3 presigned url download client
//            let appSyncConfig = try AWSAppSyncClientConfiguration(url: url, serviceRegion: region, userPoolsAuthProvider: MyCognitoUserPoolsAuthProvider(), databaseURL: databaseURL, s3ObjectManager: awsS3Manager, presignedURLClient: awsS3Manager)
//            let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6")
//            let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
//            AWSServiceManager.default().defaultServiceConfiguration = configuration
            
//            let credentialsProvider = AWSCognitoCredentialsProvider(regionType: region, identityPoolId: credentialsProviderPoolId)
//            let appSyncConfig = try AWSAppSyncClientConfiguration(url: url, serviceRegion: region, credentialsProvider: credentialsProvider, databaseURL: databaseURL, s3ObjectManager: AWSS3Manager())
            
            // Doesn't work
//            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncClientInfo: AWSAppSyncClientInfo(), databaseURL: databaseURL)
            
            appSyncClient = try AWSAppSyncClient(appSyncConfig: workingConfig)
            appSyncClient?.apolloClient?.cacheKeyForObject = { $0["id"] }
            
        } catch {
            print("Error initializing appsync client. \(error)")
        }
    }
    
    func fetchPosts(presenter: InteractorToPresenterPostFeedProtocol, limit: Int?, nextToken: String?) {
        let query = ListPostsQuery(filter: nil, limit: limit, nextToken: nextToken)
        appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "")
                presenter.postFeedFetchFailed()
                return
            }
            guard let items = result?.data?.listPosts?.items else {
                presenter.postFeedFetchFailed()
                return
            }
            debugPrint("list posts items:\(items.count)")
            let nextToken = result?.data?.listPosts?.nextToken
            let posts = items.map({ item -> Post? in
                guard let item = item else {
                    return nil
                }
//                guard let location = item.location, let category = item.category, let tags = item.tags, let isSold = item.isSold, let isNegotiable = item.isNegotiable, let willShip = item.willShip, let willMeet = item.willMeet else {
//                    return nil
//                }
                let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: item.file, file2: item.file2, file3: item.file3, file4: item.file4, userId: item.userId, user: item.user, typename: item.typename)
                
//                if let file = item.file {
//                    let s3Object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region)
//                    let presignedUrl = self.appSyncClient?.presignedURLClient?.getPresignedURL(s3Object: s3Object)
//                    post.presignedUrl = presignedUrl
//                    debugPrint("list get presigned url:\(presignedUrl)")
//                }
                return post
            })
            debugPrint("fetchPosts items:\(result?.data?.listPosts?.items?.count)")
            presenter.postFeedFetchSuccess(response: posts, nextToken: nextToken)
        })
    }
    
    func fetchPost(id: String) {
        
    }
    
//    func fetchPosts(presenter: InteractorToPresenterPostFeedProtocol) {
//        appSyncClient?.fetch(query: ListPostsQuery(), cachePolicy: .returnCacheDataAndFetch, queue: DispatchQueue.main, resultHandler: { (result, error) in
//            if error != nil {
//                print(error?.localizedDescription ?? "")
//                presenter.postFeedFetchFailed()
//                return
//            }
//            guard let items = result?.data?.listPosts?.items else {
//                presenter.postFeedFetchFailed()
//                return
//            }
//            let posts = items.map({ item -> Post? in
//                guard let item = item else {
//                    return nil
//                }
//                let post = Post(id: item.id, title: item.title, price: item.price, condition: item.condition, content: item.content)
//                return post
//            })
//            debugPrint("fetchPosts items:\(posts.count)")
//            presenter.postFeedFetchSuccess(response: posts, nextToken: nil)
//        })
//    }
    
    func publishPostWithFiles(input: CreatePostWithFilesInput, images: [String]) {
        AWSS3Manager.shared.getIdentityIdFromCredentialProvider { (credential, error) in
            guard let credential = credential else {
                return
            }
            debugPrint("Credential :\(credential), error:\(error)")
            
            let fileInput1: S3ObjectInput? = nil
            let fileInput2: S3ObjectInput? = nil
            let fileInput3: S3ObjectInput? = nil
            let fileInput4: S3ObjectInput? = nil
            
            var fileInputs = [fileInput1, fileInput2, fileInput3, fileInput4]
            
            let bucket = "archerypostmedia"
            let region = "us-east-1"
//            let localUri1 = images.first!
//            let key1 = "private/\(credential)/testpost53a.jpg"
            let mimeType = "image/jpeg"
            
            for (idx, obj) in images.enumerated() {
                let filename = "\(UUID().uuidString)"
                let encoded = "\(Data(filename.utf8).base64EncodedString())-\(idx).jpg"
                let userId = input.userId!!
                let key = "private/\(credential)/\(userId)/\(encoded)"
                debugPrint("filename:\(key), obj:\(obj), idx:\(idx)")
                let input = S3ObjectInput(bucket: bucket, key: key, region: region, localUri: obj, mimeType: mimeType)
                fileInputs[idx] = input
//                debugPrint("fileinputs:\(fileInputs[idx])")
            }
//            let file = S3ObjectInput(bucket: bucket, key: key1, region: region, localUri: localUri1, mimeType: mimeType)
//
//            let localUri2 = images.last!
//            let key2 = "private/\(credential)/testpost53b.jpg"
//
//            let file2 = S3ObjectInput(bucket: bucket, key: key2, region: region, localUri: localUri2, mimeType: mimeType)
//
//            debugPrint("ffile inputs:\(fileInputs[0]), \(fileInputs[1]), \(fileInputs[2]), \(fileInputs[3])")
            
//            var addPostMutation: AddPostWithFilesMutation!
//            let count = fileInputs.filter { $0 != nil }.count
//            debugPrint("count:\(count)")
//            switch count {
//            case 1:
//                addPostMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0])
//                break
//            case 2:
//                addPostMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0], file2: fileInputs[1])
//                break
//            case 3:
//                addPostMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0], file2: fileInputs[1], file3: fileInputs[2])
//                break
//            case 4:
//                addPostMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0], file2: fileInputs[1], file3: fileInputs[2], file4: fileInputs[3])
//                break
//            default:
//                break
//            }
            let postMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0], file2: fileInputs[1], file3: fileInputs[2], file4: fileInputs[3])
//            debugPrint("postMutation file:\(postMutation.file), \(postMutation.file2), \(postMutation.file3), \(postMutation.file4)")
            
//            let postMutation = AddPostWithFilesMutation(input: input, file: file, file2: file2)

            self.appSyncClient?.perform(mutation: postMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("result:\(result), error:\(error)")
                let group = DispatchGroup()
                for file in fileInputs.dropFirst() {
                    if let file = file {
                        group.enter()
                        let object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region, localSourceFileUrl: file.localUri, mimeType: file.mimeType)
                        self.s3Manager.upload(s3Object: object, completion: { (complete, error) in
                            debugPrint("Secondary upload complete:\(complete) error:\(error)")
                            group.leave()
                        })
                    }
                }
//                let object = AWSS3Object(key: file2.key, bucket: file2.bucket, region: file2.region, localSourceFileUrl: file2.localUri, mimeType: file2.mimeType)
//                self.s3Manager.upload(s3Object: object, completion: { (complete, error) in
//                    debugPrint("Secondary upload complete:\(complete) error:\(error)")
//                })
            })
        }
    }
    
    func publishPost() {
        let title = "Title test77"
        let price = "670.00"
        let condition = "New 7"
        let content = "Description of item 7"
        let userId = "userId4342343243"
        
        let postInput = CreatePostInput(title: title, price: price, condition: condition, content: content, userId: userId)
        let postMutation = CreatePostMutation(input: postInput)
//        let pm = AddPostMutation(id: UUID().uuidString, title: title, price: price, condition: condition, content: content)
        appSyncClient?.perform(mutation: postMutation, queue: DispatchQueue.global(), optimisticUpdate: { (update) in
            debugPrint("update:\(update)")
        }, conflictResolutionBlock: { (snapshot, mutation, result) in
            debugPrint("snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
        }, resultHandler: { (result, error) in
            debugPrint("result:\(result), error:\(error)")
        })
    }
    
    func listConversations() {
        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
//            debugPrint("tokens:\(tokens?.idToken?.tokenString)")
            do {
                guard let token = tokens?.idToken?.tokenString else {
                    return
                }
                let jwt = try decode(jwt: token)
                guard let sub = jwt.body["sub"] as? String else {
                    return
                }
                debugPrint("jwt content sub:\(sub)")
                let getUserQuery = GetUserQuery(id: sub)
                self.appSyncClient?.fetch(query: getUserQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                    debugPrint("getUserQuery fetch:\(result?.data?.getUser?.conversations?.userConversations), cog:\(result?.data?.getUser?.id), error: \(error)")
                })
            } catch {
                print("Error tokens")
            }
            
        }
        
    }
    
    func loadAllMessagesFromCache(messages: [ListMessagesQuery.Data.ListMessage.Item]) {
        
        appSyncClient?.fetch(query: ListMessagesQuery(), cachePolicy: .returnCacheDataDontFetch)  { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
//            messages = result?.data?.listMessages?.items
        }
    }
    
    func startNewMessageSubscription(conversationId: GraphQLID) {
        let subscription = SubscribeToNewMessageSubscription(conversationId: conversationId)
        do {
            discard = try appSyncClient?.subscribe(subscription: subscription, resultHandler: { (result, transaction, error) in
                if let result = result {
                    debugPrint("SubscribeToNewMessageSubscription result:\(result.data?.subscribeToNewMessage)")
                    // Store a reference to the new object
                    let newMessage = result.data?.subscribeToNewMessage
                    // Create a new object for the desired query where the new object content should reside

//                let user = ListMessagesQuery.Data.ListMessage.Item.Author(cognitoId: <#T##GraphQLID#>, id: <#T##GraphQLID#>, username: <#T##String#>)
//                    let messageToAdd = ListMessagesQuery.Data.ListMessage.Item(author: user, content: newMessage.content, conversationId: newMessage.conversationId, createdAt: newMessage.createdAt, id: newMessage.id, isSent: newMessage.isSent, recipient: newMessage.recipient, sender: newMessage.sender)
//                    do {
//                        // Update the local store with the newly received data
//                        try transaction?.update(query: ListMessagesQuery()) { (data: inout ListMessagesQuery.Data) in
//                            data.listPosts?.items?.append(messageToAdd)
//                        }
//                        self.loadAllMessagesFromCache()
//                    } catch {
//                        print("Error updating store")
//                    }
                } else if let error = error {
                    print("SubscribeToNewMessageSubscription error:\(error.localizedDescription)")
                }
            })
        } catch {
            print("Error starting subscription.")
        }
    }
}
