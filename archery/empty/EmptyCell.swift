//
//  EmptyCell.swift
//  archery
//
//  Created by Kelvin Lee on 1/21/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class EmptyCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
