//
//  SkeletonView.swift
//  archery
//
//  Created by Kelvin Lee on 1/19/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import SkeletonView

class SkeletonView: UIView {

    var view: UIView!
    @IBOutlet weak var imageViewOne: UIImageView!
    @IBOutlet weak var imageViewTwo: UIImageView!
    @IBOutlet weak var imageViewThree: UIImageView!
    @IBOutlet weak var imageViewFour: UIImageView!
    @IBOutlet weak var imageViewFive: UIImageView!
    @IBOutlet weak var imageViewSix: UIImageView!
    @IBOutlet weak var textViewOne: UITextView!
    @IBOutlet weak var textViewTwo: UITextView!
    @IBOutlet weak var textViewThree: UITextView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
//        SkeletonAppearance.default.multilineHeight = 20
//        SkeletonAppearance.default.tintColor = .green
        
        let gradient = SkeletonGradient(baseColor: UIColor.clouds)
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        let bottomAnimation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .bottomTop)
        let topLeftAnimation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .topLeftBottomRight)
        
        imageViewOne.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        imageViewTwo.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        imageViewThree.showAnimatedGradientSkeleton(usingGradient: gradient, animation: bottomAnimation)
        imageViewFour.showAnimatedGradientSkeleton(usingGradient: gradient, animation: bottomAnimation)
        imageViewFive.showAnimatedGradientSkeleton(usingGradient: gradient, animation: topLeftAnimation)
        imageViewSix.showAnimatedGradientSkeleton(usingGradient: gradient, animation: topLeftAnimation)
        
        textViewOne.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        textViewTwo.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        textViewThree.showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }

}
