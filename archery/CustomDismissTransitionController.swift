//
//  CustomDismissTransitionController.swift
//  archery
//
//  Created by Kelvin Lee on 10/25/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class CustomDismissTransitionController: NSObject, UIViewControllerAnimatedTransitioning {
    
//    private let feedCell: PostFeedCell
    
    let interactionController: SwipeInteractionController?
    
    init(interactionController: SwipeInteractionController?) {
//        self.feedCell = cell
        self.interactionController = interactionController
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // 1
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to),
            let fromView = transitionContext.view(forKey: .from),
            let toView = transitionContext.view(forKey: .to),
            let snapshot = fromVC.view.snapshotView(afterScreenUpdates: false)
            else {
                return
        }
        
//        snapshot.layer.cornerRadius = 10.0
//        snapshot.layer.masksToBounds = true
        
        // 2
        let containerView = transitionContext.containerView
        containerView.insertSubview(toView, at: 0)
        containerView.addSubview(snapshot)
        fromView.isHidden = true
        
        // 3
//        AnimationHelper.perspectiveTransform(for: containerView)
        snapshot.layer.transform = CATransform3DIdentity
//        toView.layer.transform = AnimationHelper.yRotation(-.pi / 2)
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0,
            options: .calculationModeCubic,
            animations: {
                // 1
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1/3) {
//                    snapshot.frame = self.destinationFrame
                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.25, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(0.75, 0.75, 0.75)
                    snapshot.alpha = 1.0
                }
                
                UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3) {
//                    snapshot.layer.transform = AnimationHelper.yRotation(.pi / 2)
                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.5, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
                    snapshot.alpha = 0.5
                }
                
                UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3) {
                    snapshot.layer.transform = CATransform3DMakeTranslation(snapshot.frame.size.width * 0.7, 0, 0)
//                    snapshot.layer.transform = CATransform3DMakeScale(0.25, 0.25, 0.25)
//                    toView.layer.transform = AnimationHelper.yRotation(0.0)
                    snapshot.alpha = 0.0
                    
                }
                
        },
            // 2
            completion: { _ in
                fromView.isHidden = false
                snapshot.removeFromSuperview()
                if transitionContext.transitionWasCancelled {
                    toView.removeFromSuperview()
//                    toVC.view.removeFromSuperview()
                }
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
    

}
