//
//  PostRouter.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostRouter: PresenterToRouterPostProtocol {
    static func createModule() -> PostPageViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "PostPageViewController") as! PostPageViewController
        
        let presenter: ViewToPresenterPostProtocol & InteractorToPresenterPostProtocol = PostPresenter()
        let interactor: PresenterToInteractorPostProtocol = PostInteractor()
        let router:PresenterToRouterPostProtocol = PostRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushToCoinbaseAccountScreen(navigationConroller: UINavigationController) {
        
    }
}
