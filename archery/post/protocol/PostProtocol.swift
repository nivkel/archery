//
//  PostPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

protocol ViewToPresenterPostProtocol: class {
    var view: PresenterToViewPostProtocol? {get set}
    var interactor: PresenterToInteractorPostProtocol? {get set}
    var router: PresenterToRouterPostProtocol? {get set}
    func startPublishPost(input: CreatePostWithFilesInput, images: [String])
    func showPostNextStep(navigationController:UINavigationController)
}

protocol PresenterToViewPostProtocol: class {
    func postPublished(response: Post)
    func showError()
}

protocol PresenterToRouterPostProtocol: class {
    static func createModule()-> PostPageViewController
    func pushToCoinbaseAccountScreen(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorPostProtocol: class {
    var presenter:InteractorToPresenterPostProtocol? {get set}
    func publishPost(input: CreatePostWithFilesInput, images: [String])
}

protocol InteractorToPresenterPostProtocol: class {
    func publishPostSuccess(response: Post)
    func publishPostFailed()
}

protocol PageToViewPostProtocol: class {
    func textFieldFromPage(text: String)
}


