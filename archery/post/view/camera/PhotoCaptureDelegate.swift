//
//  PhotoCaptureDelegate.swift
//  archery
//
//  Created by Kelvin Lee on 10/22/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AVFoundation
import Photos

class PhotoCaptureProcessor: NSObject {
    private(set) var requestedPhotoSettings: AVCapturePhotoSettings
    
    private let willCapturePhotoAnimation: () -> Void
    
    private let livePhotoCaptureHandler: (Bool) -> Void
    
    private let completionHandler: (PhotoCaptureProcessor) -> Void
    
    private var photoData: Data?
    
    private var livePhotoCompanionMovieURL: URL?
    
    init(with requestedPhotoSettings: AVCapturePhotoSettings,
         willCapturePhotoAnimation: @escaping () -> Void,
         livePhotoCaptureHandler: @escaping (Bool) -> Void,
         completionHandler: @escaping (PhotoCaptureProcessor) -> Void) {
        self.requestedPhotoSettings = requestedPhotoSettings
        self.willCapturePhotoAnimation = willCapturePhotoAnimation
        self.livePhotoCaptureHandler = livePhotoCaptureHandler
        self.completionHandler = completionHandler
    }
    
    private func didFinish() {
        if let livePhotoCompanionMoviePath = livePhotoCompanionMovieURL?.path {
            if FileManager.default.fileExists(atPath: livePhotoCompanionMoviePath) {
                do {
                    try FileManager.default.removeItem(atPath: livePhotoCompanionMoviePath)
                } catch {
                    print("Could not remove file at url: \(livePhotoCompanionMoviePath)")
                }
            }
        }
        
        completionHandler(self)
    }
    
}

extension PhotoCaptureProcessor: AVCapturePhotoCaptureDelegate {
    /*
     This extension includes all the delegate callbacks for AVCapturePhotoCaptureDelegate protocol
     */
    
    func photoOutput(_ output: AVCapturePhotoOutput, willBeginCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        if resolvedSettings.livePhotoMovieDimensions.width > 0 && resolvedSettings.livePhotoMovieDimensions.height > 0 {
            livePhotoCaptureHandler(true)
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        willCapturePhotoAnimation()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            print("Error capturing photo: \(error)")
        } else {
            photoData = photo.fileDataRepresentation()
        }
    }
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishRecordingLivePhotoMovieForEventualFileAt outputFileURL: URL, resolvedSettings: AVCaptureResolvedPhotoSettings) {
        livePhotoCaptureHandler(false)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingLivePhotoToMovieFileAt outputFileURL: URL, duration: CMTime, photoDisplayTime: CMTime, resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        if error != nil {
            print("Error processing live photo companion movie: \(String(describing: error))")
            return
        }
        livePhotoCompanionMovieURL = outputFileURL
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        if let error = error {
            print("Error capturing photo: \(error)")
            didFinish()
            return
        }
        
        guard let photoData = photoData else {
            print("No photo data resource")
            didFinish()
            return
        }
        
        PHPhotoLibrary.requestAuthorization { status in
            if status == .authorized {
                debugPrint("Custom photo:\(CustomPhotoAlbum.sharedInstance.assetCollection)")
                
                let photoOptions = PHAssetResourceCreationOptions()
                photoOptions.uniformTypeIdentifier = self.requestedPhotoSettings.processedFileType.map { $0.rawValue }
                
                guard let livePhotoCompanionMovieURL = self.livePhotoCompanionMovieURL else {
                    debugPrint("No live photo url")
                    CustomPhotoAlbum.sharedInstance.save(photoData: photoData, saveToLibrary: true, photoOptions: photoOptions, completion: {
                        self.didFinish()
                    })
                    return
                }
                let livePhotoCompanionMovieFileResourceOptions = PHAssetResourceCreationOptions()
                livePhotoCompanionMovieFileResourceOptions.shouldMoveFile = true
                debugPrint("There's live photo url")
                CustomPhotoAlbum.sharedInstance.save(photoData: photoData, saveToLibrary: true, photoOptions: photoOptions, liveOptions: livePhotoCompanionMovieFileResourceOptions, liveURL: livePhotoCompanionMovieURL, completion: {
                    self.didFinish()
                })
                
                //                PHPhotoLibrary.shared().performChanges({
                //                    let options = PHAssetResourceCreationOptions()
                //                    let creationRequest = PHAssetCreationRequest.forAsset()
                //                    options.uniformTypeIdentifier = self.requestedPhotoSettings.processedFileType.map { $0.rawValue }
                //                    creationRequest.addResource(with: .photo, data: photoData, options: options)
                //
                //                    if let livePhotoCompanionMovieURL = self.livePhotoCompanionMovieURL {
                //                        let livePhotoCompanionMovieFileResourceOptions = PHAssetResourceCreationOptions()
                //                        livePhotoCompanionMovieFileResourceOptions.shouldMoveFile = true
                //                        creationRequest.addResource(with: .pairedVideo, fileURL: livePhotoCompanionMovieURL, options: livePhotoCompanionMovieFileResourceOptions)
                //                    }
                //
                //                }, completionHandler: { _, error in
                //                    if let error = error {
                //                        print("Error occurered while saving photo to photo library: \(error)")
                //                    }
                //
                //                    self.didFinish()
                //                })
            } else {
                self.didFinish()
            }
        }
    }
}
