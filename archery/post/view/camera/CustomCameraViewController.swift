//
//  CustomCameraViewController.swift
//  archery
//
//  Created by Kelvin Lee on 2/25/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftyCam
import Pickle

extension CustomCameraViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        debugPrint("didFinishPickingMediaWithInfo")
    }
}

extension CustomCameraViewController: UINavigationControllerDelegate {

}

extension CustomCameraViewController: CameraCompatible {
    var delegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)? {
        get {
            return self
        }
        set(newValue) {
            self.pickerDelegate = newValue
        }
    }
    
    var sourceType: UIImagePickerController.SourceType {
        get {
            return .camera
        }
        set(newValue) {
            self.type = newValue
        }
    }
    
    
}

extension CustomCameraViewController: SwiftyCamViewControllerDelegate {
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        // Called when takePhoto() is called or if a SwiftyCamButton initiates a tap gesture
        // Returns a UIImage captured from the current session
        debugPrint("photo didTake:\(photo)")
        
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        // Called when startVideoRecording() is called
        // Called if a SwiftyCamButton begins a long press gesture
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        // Called when stopVideoRecording() is called
        // Called if a SwiftyCamButton ends a long press gesture
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        // Called when stopVideoRecording() is called and the video is finished processing
        // Returns a URL in the temporary directory where video is stored
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        // Called when a user initiates a tap gesture on the preview layer
        // Will only be called if tapToFocus = true
        // Returns a CGPoint of the tap location on the preview layer
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        // Called when a user initiates a pinch gesture on the preview layer
        // Will only be called if pinchToZoomn = true
        // Returns a CGFloat of the current zoom level
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        // Called when user switches between cameras
        // Returns current camera selection
    }
}
class CustomCameraViewController: SwiftyCamViewController {
    
    var type: UIImagePickerController.SourceType?
    var pickerDelegate: (UIImagePickerControllerDelegate & UINavigationControllerDelegate)?

    override func viewDidLoad() {
        super.viewDidLoad()
        cameraDelegate = self
        audioEnabled = false
        
        let buttonFrame = CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100, height: 100)
        let captureButton = CameraButton(frame: buttonFrame)
//        captureButton.backgroundColor = UIColor.red
        self.view.addSubview(captureButton)
        captureButton.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
}
