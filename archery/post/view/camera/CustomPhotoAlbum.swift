//
//  CustomPhotoAlbum.swift
//  archery
//
//  Created by Kelvin Lee on 10/22/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Photos

class CustomPhotoAlbum: NSObject {
    static let albumName = "ArcheryPhotos"
    static let sharedInstance = CustomPhotoAlbum()
    
    var assetCollection: PHAssetCollection!
    
    override init() {
        super.init()
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            debugPrint("init asset exists:\(assetCollection)")
        } else {
            self.createAlbum()
            
        }
    }
    
    func requestAuthorizationHandler(status: PHAuthorizationStatus) {
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            // ideally this ensures the creation of the photo album even if authorization wasn't prompted till after init was done
            print("trying again to create the album")
            self.createAlbum()
        } else {
            print("should really prompt the user to let them know it's failed")
        }
    }
    
    func createAlbum() {
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)   // create an asset collection with the album name
        }) { success, error in
            if success {
                self.assetCollection = self.fetchAssetCollectionForAlbum()
                debugPrint("init asset create:\(self.assetCollection)")
            } else {
                print("error \(String(describing: error))")
            }
        }
    }
    
    func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }
    
    func saveVideo(outputFileURL: URL, completion:@escaping () -> Void) {
        if self.assetCollection == nil {
            completion()
            return
            // if there was an error upstream, skip the save
        }
        
        PHPhotoLibrary.shared().performChanges({
            let options = PHAssetResourceCreationOptions()
            options.shouldMoveFile = true
            guard let creationRequest = PHAssetCreationRequest.creationRequestForAssetFromVideo(atFileURL: outputFileURL) else {
                return
            }
            //            creationRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
            guard let assetPlaceHolder = creationRequest.placeholderForCreatedAsset,
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) else {
                    completion()
                    return
            }
            let enumeration: NSArray = [assetPlaceHolder]
            
            if self.assetCollection.estimatedAssetCount == 0
            {
                albumChangeRequest.addAssets(enumeration)
            }
            else {
                albumChangeRequest.insertAssets(enumeration, at: [0])
            }
            
        }, completionHandler: { success, error in
            if !success {
                print("Could not save movie to photo library: \(String(describing: error))")
                
            }
            completion()
        })
    }
    
    func save(photoData: Data, saveToLibrary: Bool = true, photoOptions: PHAssetResourceCreationOptions?, liveOptions: PHAssetResourceCreationOptions? = nil, liveURL: URL? = nil, completion : @escaping () -> ()) {
        if assetCollection == nil {
            completion()
            return
            // if there was an error upstream, skip the save
        }
        
        PHPhotoLibrary.shared().performChanges({
            guard let image = UIImage(data: photoData) else {
                completion()
                return
            }
            let creationRequest = PHAssetCreationRequest.creationRequestForAsset(from: image)
            guard let assetPlaceHolder = creationRequest.placeholderForCreatedAsset,
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection) else {
                    completion()
                    return
            }
            let enumeration: NSArray = [assetPlaceHolder]
            
            if self.assetCollection.estimatedAssetCount == 0
            {
                albumChangeRequest.addAssets(enumeration)
                //                if let liveURL = liveURL, let liveOptions = liveOptions {
                //                    debugPrint("Save live photo too")
                //                }
            }
            else {
                albumChangeRequest.insertAssets(enumeration, at: [0])
                //                if let liveURL = liveURL, let liveOptions = liveOptions {
                //                    debugPrint("Save live photo too")
                //                }
            }
            
            if saveToLibrary {
                debugPrint("Save to library")
                let creationRequest = PHAssetCreationRequest.forAsset()
                creationRequest.addResource(with: .photo, data: photoData, options: photoOptions)
                
                if let liveURL = liveURL, let liveOptions = liveOptions {
                    debugPrint("Save live to library")
                    creationRequest.addResource(with: .pairedVideo, fileURL: liveURL, options: liveOptions)
                }
            }
            
        }, completionHandler: { status , errror in
            completion( )
        })
    }
}
