//
//  MediaPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/22/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class MediaPageViewController: UIPageViewController {

    var pages = [UIViewController]()
    
    var presenter: ViewToPresenterPostProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let cameraViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
//        postPageOneViewController.pageToViewPostDelegate = self
        
//        var configuration = Configuration()
//        configuration.doneButtonTitle = "Finish"
//        configuration.noImagesTitle = "Sorry! There are no images here!"
//        configuration.recordLocation = false
//
//        let imagePicker = ImagePickerController(configuration: configuration)
//        let imagePickerController = imagePicker
//        imagePickerController.delegate = self
        
//        postPageTwoViewController.pageToViewPostDelegate = self
        
//        self.pages.append(cameraViewController)
//        self.pages.append(imagePickerController)
//        self.setViewControllers([imagePickerController], direction: .forward, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//extension MediaPageViewController: ImagePickerDelegate {
//    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
//        debugPrint("ImagePicker wrapperDidPress:\(images)")
//    }
//
//    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
//        debugPrint("ImagePicker done:\(images)")
//        self.dismiss(animated: true) {
//
//        }
//    }
//
//    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
//        debugPrint("ImagePicker cancel")
//        self.dismiss(animated: true) {
//
//        }
//    }
//}

extension MediaPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        let previousIndex = abs((currentIndex - 1) % self.pages.count)
        
        if currentIndex + 1 == self.pages.count - 1 {
            return nil
        }
        
        return self.pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        let nextIndex = abs((currentIndex + 1) % self.pages.count)
        
        if currentIndex + 1 == self.pages.count {
            return nil
        }
        return self.pages[nextIndex]
    }
}
