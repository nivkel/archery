//
//  PhotoCollectionCell.swift
//  archery
//
//  Created by Kelvin Lee on 10/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class PhotoCollectionCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        photo.layer.cornerRadius = 10.0
        photo.layer.masksToBounds = true
        removeButton.layer.cornerRadius = 8.0
    }
}
