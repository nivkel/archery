//
//  PostPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import PromiseKit
import SwiftMessages
import MapKit
import NVActivityIndicatorView

protocol PickerDelegate: class {
    func showPhotoOptions(sender: UIImage, indexPath: IndexPath)
    func updateAttachments(attachments: [UIImage])
    func removeAttachment(sender: UIButton)
}

protocol CollectionSelectionDelegate: class {
    func collectionSelection(items: [String])
}

protocol ItemSelectDelegate: class {
    func selectItem(item: String, type: BaseCollectionDelegate.ItemType)
    func deselectItem(item: String, type: BaseCollectionDelegate.ItemType)
}

protocol PostPageImagesDelegate: class {
    func getImages() -> [UIImage]
    func getImagesUri() -> [String]
    func getImagesToRemove() -> [Int]
}

protocol PostPageTitleDelegate: class {
    func setTitleText(value: String)
}

protocol PostPageTagsDescriptionDelegate: class {
    func setTags(value: String)
    func setDescriptionText(value: String)
}

protocol PostPageCategoryDelegate: class {
    func setCategory(value: String)
}

protocol PostPagePriceDelegate: class {
    func setPrice(value: String)
    func setNegotiable(value: Bool)
    func setSalePrice(value: Bool)
}

protocol PostPageShippingDelegate: class {
    func setMeetPickup(value: Bool)
    func setShipNationally(value: Bool)
    func setLocation(value: String)
}


enum PostPages: Int {
    case photos = 0
    case categories = 1
    case price = 2
    case shipping = 3
    case publish = 4
}

class PostPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController]()
    
    var presenter: ViewToPresenterPostProtocol?
    
    var existingPost: Post?
    
    var titleText: String?
    
    var selectedCondition: String?
    var selectedCategory: String? {
        didSet {
            let categoriesViewController = self.pages.compactMap { $0 as? CategoriesViewController }.first!
            if let selectedCategory = self.selectedCategory {
                categoriesViewController.categoryLabel.text = "Category: \(selectedCategory)"
            }
            
        }
    }
    var tags: String?
    var descriptionText: String?
    
    var price: String?
    var isNegotiable: Bool = true
    var hasSalePrice: Bool = false
    var isSold: Bool = false
    
    var willShipNationally: Bool = true
    var willMeetPickup: Bool = true
    var location: String?
    
    var userPayload: [String: String]!
    
    var postPageImagesDelegate: PostPageImagesDelegate!
    
    let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Working...", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let photosViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        photosViewController.pageToViewPostDelegate = self
        photosViewController.postPageTitleDelegate = self
        
        let categoriesViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
        categoriesViewController.pageToViewPostDelegate = self
        categoriesViewController.postPageTagsDescriptionDelegate = self
        
        let priceViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "PriceViewController") as! PriceViewController
        priceViewController.postPagePriceDelegate = self
        
        let shippingViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
        shippingViewController.postPageShippingDelegate = self
        
        let publishViewController = PostRouter.mainstoryboard.instantiateViewController(withIdentifier: "PublishViewController") as! PublishViewController
        
        self.pages.append(photosViewController)
        self.pages.append(categoriesViewController)
        self.pages.append(priceViewController)
        self.pages.append(shippingViewController)
        self.pages.append(publishViewController)
        self.setViewControllers([photosViewController], direction: .forward, animated: true, completion: nil)
        
        firstly {
            ChatServicesManager.shared.getUserPayload()
            }.get { userPayload in
                self.userPayload = userPayload
            }.catch { error in
                debugPrint("getUserPayload error:\(error)")
        }
        
        let height = self.view.frame.size.height - 110
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
        
        if let title = existingPost?.title {
            self.navigationItem.title = "Editing \(title)"
        }
        
        self.fillInPhotosFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.parent?.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func goToPage(page: PostPages, direction: UIPageViewController.NavigationDirection) {
        let nextPage = self.pages[page.rawValue]
        self.setViewControllers([nextPage], direction: direction, animated: true, completion: nil)
    }
    
    func resetFields() {
        
    }
    
    func handleFiles(existingPost: Post) {
        let photosViewController = self.pages[PostPages.photos.rawValue] as! PhotosViewController
        var images = [PhotosViewController.selectImage(), PhotosViewController.selectImage(), PhotosViewController.selectImage(), PhotosViewController.selectImage()]
        var localUris = [String]()
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
        DispatchQueue.global().async {
            let group = DispatchGroup()
            
            if let file = existingPost.file {
                debugPrint("existingPost file:\(file.key)")
                group.enter()
                DispatchQueue.main.async {
                    AWSS3Manager.shared.downloadImage(imageView: photosViewController.primaryImageView, key: file.key) { (image) in
                        if let image = image {
                            images[0] = image
//                            localUris.append(file.key)
                        }
                        debugPrint("file dl'ed")
                        group.leave()
                    }
                }
            }
            group.wait()
            
            if let file2 = existingPost.file2 {
                debugPrint("existingPost file:\(file2.key)")
                group.enter()
                DispatchQueue.main.async {
                    AWSS3Manager.shared.downloadImage(imageView: photosViewController.primaryImageView, key: file2.key) { (image) in
                        if let image = image {
                            images[1] = image
//                        localUris.append(file2.key)
                        }
                        debugPrint("file dl'ed 2")
                        group.leave()
                    }
                }
            }
            group.wait()
            
            if let file3 = existingPost.file3 {
                debugPrint("existingPost file:\(file3.key)")
                group.enter()
                DispatchQueue.main.async {
                    AWSS3Manager.shared.downloadImage(imageView: photosViewController.primaryImageView, key: file3.key) { (image) in
                        if let image = image {
                            images[2] = image
//                        localUris.append(file3.key)
                        }
                        debugPrint("file dl'ed 3")
                        group.leave()
                    }
                }
            }
            group.wait()
            
            if let file4 = existingPost.file4 {
                debugPrint("existingPost file:\(file4.key)")
                group.enter()
                DispatchQueue.main.async {
                    AWSS3Manager.shared.downloadImage(imageView: photosViewController.primaryImageView, key: file4.key) { (image) in
                        if let image = image {
                            images[3] = image
//                        localUris.append(file4.key)
                        }
                        debugPrint("file dl'ed 4")
                        group.leave()
                    }
                }
            }
            group.wait()
            group.notify(queue: .main) {
                debugPrint("file handling complete")
                photosViewController.images = images
                photosViewController.localUris = localUris
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
        }
    }
    
    func fillInPhotosFields() {
        guard let existingPost = self.existingPost else {
            return
        }
        debugPrint("existingPost:\(existingPost.title)")
        let photosViewController = self.pages[PostPages.photos.rawValue] as! PhotosViewController
        if let title = existingPost.title {
            photosViewController.titleTextField.text = title
            photosViewController.postPageTitleDelegate?.setTitleText(value: title)
        }
        
        if let isSold = existingPost.isSold {
            self.isSold = isSold
        }
        
        self.handleFiles(existingPost: existingPost)
    }
    
    func fillInCategoryFields() {
        guard let existingPost = self.existingPost else {
            return
        }
        let categoriesViewController = self.pages[PostPages.categories.rawValue] as! CategoriesViewController
        if let selectedCategory = existingPost.category {
            categoriesViewController.categoryLabel.text = selectedCategory
            self.setCategory(value: selectedCategory)
        }
        if let condition = existingPost.condition {
            categoriesViewController.conditionCollectionDelegate.selected = [condition]
            categoriesViewController.conditionCollectionDelegate.itemSelectDelegate?.selectItem(item: condition, type: .condition)
        }
        if let tags = existingPost.tags {
            categoriesViewController.postPageTagsDescriptionDelegate?.setTags(value: tags)
            categoriesViewController.tagTextField.text = tags
        }
        if let descriptionText = existingPost.content {
            categoriesViewController.postPageTagsDescriptionDelegate?.setDescriptionText(value: descriptionText)
            categoriesViewController.descriptionTextView.text = descriptionText
        }
    }
    
    func fillInPriceFields() {
        guard let existingPost = self.existingPost else {
            return
        }
        let priceViewController = self.pages[PostPages.price.rawValue] as! PriceViewController
        if let price = existingPost.price {
            priceViewController.priceTextView.text = price
            priceViewController.postPagePriceDelegate.setPrice(value: price)
        }
        if let isNegotiable = existingPost.isNegotiable {
            priceViewController.negotiableSwitch.setOn(isNegotiable, animated: true)
            priceViewController.postPagePriceDelegate.setNegotiable(value: isNegotiable)
        }
    }
    
    func fillInShippingFields() {
        guard let existingPost = self.existingPost else {
            return
        }
        let shippingViewController = self.pages[PostPages.shipping.rawValue] as! ShippingViewController
        if let meetPickup = existingPost.willMeet {
            shippingViewController.meetPickupSwitch.setOn(meetPickup, animated: true)
            shippingViewController.postPageShippingDelegate?.setMeetPickup(value: meetPickup)
        }
        if let willShip = existingPost.willShip {
            shippingViewController.shipNationallySwitch.setOn(willShip, animated: true)
            shippingViewController.postPageShippingDelegate?.setShipNationally(value: willShip)
        }
        if let location = existingPost.location {
            shippingViewController.postPageShippingDelegate?.setLocation(value: location)
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(location) { (placemarks, error) in
                if let error = error {
                    debugPrint(error)
                }
                if let placemark = placemarks?.first {
                    if let zipCode = placemark.postalCode {
                        shippingViewController.zipCodeTextField.text = zipCode
                        shippingViewController.handleLocation()
                    }
                }
            }
        }
    }
    
    func salePriceExists() -> String? {
        guard let existingPost = self.existingPost else {
            return nil
        }
        if let salePrice = existingPost.salePrice {
            return salePrice
        }
        return nil
    }
    
    func savePost() {
        guard let existingPost = self.existingPost else {
            return
        }
        guard let title = self.titleText, let price = self.price, let location = self.location, let category = self.selectedCategory, let condition = self.selectedCondition, let content = self.descriptionText, let tags = self.tags, let userId = self.userPayload["sub"], let username = self.userPayload["username"] else {
            debugPrint("Error: Parameters missing")
            Common.showStatusMessage(title: "Incomplete", message: "Fields missing", theme: .error, style: .bottom)
            return
        }
        var salePrice: String? = ""
        if !self.hasSalePrice {
            salePrice = nil
        }
        guard !self.postPageImagesDelegate.getImages().isEmpty || !self.postPageImagesDelegate.getImagesUri().isEmpty else {
            debugPrint("Error: Images empty")
            Common.showStatusMessage(title: "Incomplete", message: "Photos missing", theme: .error, style: .bottom)
            return
        }
        let localUris = self.postPageImagesDelegate.getImagesUri()
        let images = self.postPageImagesDelegate.getImages()
        let imagesToRemove = self.postPageImagesDelegate.getImagesToRemove()
        
        debugPrint("savePost localUris: \(localUris)")
        debugPrint("savePost images: \(images)")
        debugPrint("savePost imagesToRemove: \(imagesToRemove)")
        let updatePostInput = UpdateUserPostInput(id: existingPost.id, title: title, price: price, salePrice: salePrice, location: location, category: category, condition: condition, content: content, tags: tags, isSold: self.isSold, isNegotiable: self.isNegotiable, willShip: self.willShipNationally, willMeet: self.willMeetPickup)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        PostServicesManager.shared.editPostWithFiles(existingPost: existingPost, input: updatePostInput, images: localUris, imagesToRemove: imagesToRemove).done {
                debugPrint("savePost done")
            }.ensure {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                Common.showNoticeMessage(title: "", message: "Changes saved", theme: .success, style: .center)
            }.catch { error in
                debugPrint("savePost error: \(error)")
        }
    }
    
    func publishPost() {
        guard let title = self.titleText, let price = self.price, let location = self.location, let category = self.selectedCategory, let condition = self.selectedCondition, let content = self.descriptionText, let tags = self.tags, let userId = self.userPayload["sub"], let username = self.userPayload["username"] else {
            debugPrint("Error: Parameters missing")
            Common.showStatusMessage(title: "Incomplete", message: "Fields missing", theme: .error, style: .bottom)
            return
        }
        guard !self.postPageImagesDelegate.getImages().isEmpty && !self.postPageImagesDelegate.getImagesUri().isEmpty else {
            debugPrint("Error: Images empty")
            Common.showStatusMessage(title: "Incomplete", message: "Photos missing", theme: .error, style: .bottom)
            return
        }

        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
        let localUris = self.postPageImagesDelegate.getImagesUri()
        let postWithFilesInput = CreatePostWithFilesInput(title: title, price: price, location: location, category: category, condition: condition, content: content, tags: tags, isSold: false, isPublished: true, isNegotiable: self.isNegotiable, willShip: self.willShipNationally, willMeet: self.willMeetPickup, userId: userId, username: username)
        
        presenter?.startPublishPost(input: postWithFilesInput, images: localUris)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case 0:
            return nil
        default:
            return self.pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case (self.pages.count - 1):
            return nil
        default:
            return self.pages[currentIndex + 1]
        }
    }
    
//    func presentationCount(for pageViewController: UIPageViewController) -> Int {
//        return self.pages.count
//    }
    
//    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
//        return 0
//    }
}

extension PostPageViewController: PostPublishFieldsDelegate {
    func setPublishFields() -> [String: Any] {
        return [String: Any]()
    }
    
    
}

extension PostPageViewController: PageToViewPostProtocol {
    func textFieldFromPage(text: String) {
        debugPrint("textFieldFromPage text:\(text)")
    }
}

extension PostPageViewController: PresenterToViewPostProtocol {
    func postPublished(response: Post) {
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        debugPrint("postPublished:\(response)")
        let postSuccessViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "PostSuccessViewController") as! PostSuccessViewController
        let navigationController = UINavigationController(rootViewController: postSuccessViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
    }
    
    func showError() {
        NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        debugPrint("Post error")
        Common.showStatusMessage(title: "Error", message: "Post failed, try again.", theme: .error, style: .center)
    }
}

extension PostPageViewController: PostPageTagsDescriptionDelegate {
    func setDescriptionText(value: String) {
        self.descriptionText = value
        debugPrint("setDescription:\(value)")
    }
    
    func setTags(value: String) {
        debugPrint("setTags :\(value)")
        self.tags = value
    }
}

extension PostPageViewController: PostPageCategoryDelegate {
    func setCategory(value: String) {
        debugPrint("setCategory :\(value)")
        self.selectedCategory = value
    }
}

extension PostPageViewController: ItemSelectDelegate {
    func selectItem(item: String, type: BaseCollectionDelegate.ItemType) {
        debugPrint("ItemSelectDelegate selectItem:\(item)")
        switch type {
        case .condition:
            self.selectedCondition = item
            break
        default:
            break
        }
    }
    
    func deselectItem(item: String, type: BaseCollectionDelegate.ItemType) {
        debugPrint("ItemSelectDelegate deselectItem:\(item)")
    }
}

extension PostPageViewController: PostPagePriceDelegate {
    func setPrice(value: String) {
        debugPrint("setPrice value:\(value)")
        self.price = value
    }
    
    func setNegotiable(value: Bool) {
        debugPrint("setNegotiable value:\(value)")
        self.isNegotiable = value
    }
    
    func setSalePrice(value: Bool) {
        debugPrint("setSalePrice value:\(value)")
        self.hasSalePrice = value
    }
}

extension PostPageViewController: PostPageTitleDelegate {
    func setTitleText(value: String) {
        debugPrint("setTitleText value \(value)")
        self.titleText = value
    }
}

extension PostPageViewController: PostPageShippingDelegate {
    func setMeetPickup(value: Bool) {
        debugPrint("setMeetPickup value \(value)")
        self.willMeetPickup = value
    }
    
    func setShipNationally(value: Bool) {
        debugPrint("setShipNationally value \(value)")
        self.willShipNationally = value
    }
    
    func setLocation(value: String) {
        debugPrint("setLocation value \(value)")
        self.location = value
    }
}


