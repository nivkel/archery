//
//  SelectCollectionCell.swift
//  archery
//
//  Created by Kelvin Lee on 10/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class SelectCollectionCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override var isSelected: Bool {
        didSet{
            if self.isSelected {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                }, completion: nil)
                self.containerView.backgroundColor = Common.primaryColor
                self.nameLabel.textColor = UIColor.white
            } else {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                    self.transform = CGAffineTransform.identity
                }, completion: nil)
                
                self.containerView.backgroundColor = Common.backgroundColor
                self.nameLabel.textColor = Common.primaryColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.layer.borderColor = Common.primaryColor.cgColor
        self.containerView.layer.borderWidth = 1.0
        self.containerView.layer.cornerRadius = 10.0
        self.containerView.backgroundColor = Common.backgroundColor
        self.nameLabel.textColor = Common.primaryColor
    }
}
