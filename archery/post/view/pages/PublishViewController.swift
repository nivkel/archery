//
//  PublishViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit

protocol PostPublishFieldsDelegate: class {
    func setPublishFields() -> [String: Any]
}

class PublishViewController: UIViewController {
    
    var postPublishFieldsDelegate: PostPublishFieldsDelegate!
    
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var negotiableLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var meetingLabel: UILabel!
    @IBOutlet weak var negotiableIcon: UIImageView!
    @IBOutlet weak var shipIcon: UIImageView!
    @IBOutlet weak var meetIcon: UIImageView!
    
    @IBOutlet weak var editImageButton: UIButton!
    @IBAction func editImageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .photos, direction: .reverse)
    }
    @IBAction func editTitleButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .photos, direction: .reverse)
    }
    @IBOutlet weak var editTitleButton: UIButton!
    @IBAction func editPriceButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .price, direction: .reverse)
    }
    @IBOutlet weak var editPriceButton: UIButton!
    @IBAction func editDetailsButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .categories, direction: .reverse)
    }
    @IBOutlet weak var editDetailsButton: UIButton!
    @IBAction func editLocationButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .shipping, direction: .reverse)
    }
    @IBOutlet weak var editLocationButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .shipping, direction: .reverse)
    }
    
    @IBOutlet weak var postButton: UIButton!
    @IBAction func postButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        if let _ = postPageViewController.existingPost {
            debugPrint("edit Post save button pushed")
            postPageViewController.savePost()
            return
        }
        debugPrint("Post button pushed")
        postPageViewController.publishPost()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backButton.layer.cornerRadius = 10.0
        postButton.layer.cornerRadius = 10.0
        
        editImageButton.layer.cornerRadius = 10.0
        editTitleButton.layer.cornerRadius = 10.0
        editPriceButton.layer.cornerRadius = 10.0
        editDetailsButton.layer.cornerRadius = 10.0
        editLocationButton.layer.cornerRadius = 10.0
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        if let _ = postPageViewController.existingPost {
            self.postButton.setTitle("Save", for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let fields = postPublishFieldsDelegate.setPublishFields()
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        guard let image = postPageViewController.postPageImagesDelegate.getImages().first else {
            self.postImageView.setFAIconWithName(icon: .FAImage, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 100.0, height: 100.0))
            return
        }
        self.postImageView.image = image
        
        guard let titleText = postPageViewController.titleText, let price = postPageViewController.price, let category = postPageViewController.selectedCategory, let condition = postPageViewController.selectedCondition, let location = postPageViewController.location, let descriptionText = postPageViewController.descriptionText else {
            return
        }
        if let tags = postPageViewController.tags {
            self.tagsLabel.text = "Tags: \(tags)"
        }
        let negotiable = postPageViewController.isNegotiable
        let shipping = postPageViewController.willShipNationally
        let meeting = postPageViewController.willMeetPickup
        
        titleLabel.text = "Title: \(titleText)"
        priceLabel.text = "Price: \(price)"
        categoryLabel.text = "Category: \(category)"
        conditionLabel.text = "Condition: \(condition)"
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { (placemarks, error) in
            if let error = error {
                debugPrint(error)
            }
            if let placemark = placemarks?.first {
                if let zipCode = placemark.postalCode {
                    self.locationLabel.text = "Zip Code: \(zipCode)"
                }
            }
        }
        descriptionLabel.text = "Description: \(descriptionText)"
        negotiableLabel.text = "Negotiable"//: \(negotiable)"
        shippingLabel.text = "Shipping"//: \(shipping)"
        meetingLabel.text = "Meeting"//: \(meeting)"
        
        if negotiable {
            self.negotiableIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        } else {
            self.negotiableIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        }
        if shipping {
            self.shipIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        } else {
            self.shipIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        }
        if meeting {
            self.meetIcon.setFAIconWithName(icon: .FACheckCircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        } else {
            self.meetIcon.setFAIconWithName(icon: .FACircle, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50, height: 50))
        }
    }
}
