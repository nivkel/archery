//
//  BaseViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/25/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    private var baseScrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupKeyboardHandling(textField: UITextField?, textView: UITextView?, scrollView: UIScrollView) {
        self.baseScrollView = scrollView
        
        if let textField = textField {
            textField.delegate = self
        }
        if let textView = textView {
            textView.delegate = self
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        let toolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 30.0))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 30.0, height: 30.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self.view, action: #selector(self.view.endEditing(_:)))
        closeButton.tintColor = UIColor.black
        toolbar.setItems([flexSpace, closeButton], animated: false)
        toolbar.sizeToFit()
        
        if let textField = textField {
            textField.inputAccessoryView = toolbar
        }
        if let textView = textView {
            textView.inputAccessoryView = toolbar
        }
    }
    
    @objc private func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let padding: CGFloat = 40.0
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            baseScrollView.contentInset = UIEdgeInsets.zero
        } else {
            baseScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - padding, right: 0)
        }
        baseScrollView.scrollIndicatorInsets = baseScrollView.contentInset
    }

}

extension BaseViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("title text:\(String(describing: textField.text))")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}

extension BaseViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("CharChange:\(textView.text.count)")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("didEnd")
    }
}
