//
//  PostSuccessViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/17/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class PostSuccessViewController: UIViewController {

    @IBOutlet weak var postSuccessLabel: UILabel!
    @IBAction func postAnotherButtonAction(_ sender: UIButton) {
        
        self.close()
    }
    @IBOutlet weak var postAnotherButton: UIButton!
    @IBOutlet weak var postSuccessImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(PostSuccessViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        
        let height = self.view.frame.size.height - 110
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
    }

    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
}
