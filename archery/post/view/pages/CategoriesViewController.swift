//
//  CategoriesViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftMessages

class CategoriesViewController: BaseViewController {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var conditionCollectionView: UICollectionView!
    @IBOutlet weak var tagTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionCountLabel: UILabel!
    @IBOutlet weak var descriptionPlaceholder: UILabel!
    @IBOutlet weak var selectCategoryButton: UIButton!
    @IBAction func selectCategoryButtonAction(_ sender: UIButton) {
        
        let selectCategoryViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectCategoryViewController") as! SelectCategoryViewController
        selectCategoryViewController.isPostPage = true
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        selectCategoryViewController.postPageCategoryDelegate = postPageViewController
        let navigationController = UINavigationController(rootViewController: selectCategoryViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .centered)
        segue.perform()
//        self.present(navigationController, animated: true, completion: nil)
    }
    
    var pageToViewPostDelegate: PageToViewPostProtocol?
    
    var postPageTagsDescriptionDelegate: PostPageTagsDescriptionDelegate?
    
    var conditionCollectionDelegate: BaseCollectionDelegate!
    
    var itemConditions = ["New", "Like new", "Good", "Fair", "Poor"]
    
    var selectedItems: [String]?
    
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func nextPageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        guard let _ = postPageViewController.selectedCategory else {
            Common.showStatusMessage(title: "Incomplete", message: "Select a category", theme: .error, style: .bottom)
            return
        }
        guard let _ = postPageViewController.selectedCondition else {
            Common.showStatusMessage(title: "Incomplete", message: "Select a condition", theme: .error, style: .bottom)
            return
        }
        guard let descriptionText = postPageViewController.descriptionText else {
            Common.showStatusMessage(title: "Incomplete", message: "Enter a description", theme: .error, style: .bottom)
            return
        }
        postPageViewController.goToPage(page: .price, direction: .forward)
    }
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backPageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .photos, direction: .reverse)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupConditionCollectionView()
        
        setupKeyboardHandling(textField: self.tagTextField, textView: self.descriptionTextView, scrollView: self.scrollView)
        
        selectCategoryButton.layer.cornerRadius = 10.0
        backButton.layer.cornerRadius = 10.0
        nextButton.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.fillInCategoryFields()
        
        if self.descriptionTextView.text.isEmpty {
            self.descriptionPlaceholder.isHidden = false
        } else {
            self.descriptionPlaceholder.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageToViewPostDelegate?.textFieldFromPage(text: "Test page 2")
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        debugPrint("postPageViewController:\(postPageViewController.pages.count)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setupConditionCollectionView() {
        conditionCollectionView.register(SelectCollectionCell.nib, forCellWithReuseIdentifier: SelectCollectionCell.reuseIdentifier)
        if let layout = conditionCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        conditionCollectionView.allowsMultipleSelection = false
        conditionCollectionDelegate = ConditionCollectionDelegate(collectionView: conditionCollectionView, items: self.itemConditions, type: .condition)
        if let selectedItems = self.selectedItems {
            conditionCollectionDelegate?.selected = selectedItems
        }
        
        conditionCollectionView.delegate = conditionCollectionDelegate
        conditionCollectionView.dataSource = conditionCollectionDelegate
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        conditionCollectionDelegate?.itemSelectDelegate = postPageViewController
    }
}

extension CategoriesViewController {
    override func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: (textView.superview?.frame.origin.y)!), animated: true)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        let maxLength = 200
        let newString = updatedText as NSString
        let validLength = newString.length <= maxLength
        
        if updatedText.count <= maxLength {
            if !updatedText.isEmpty {
                self.postPageTagsDescriptionDelegate?.setDescriptionText(value: updatedText)
                self.descriptionCountLabel.text = "\(updatedText.count)/200"
                self.descriptionPlaceholder.isHidden = true
            } else {
                self.postPageTagsDescriptionDelegate?.setDescriptionText(value: updatedText)
                self.descriptionCountLabel.text = "\(0)/200"
                self.descriptionPlaceholder.isHidden = false
            }
        }
        return validLength
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        print("CharChange:\(textView.text.count)")
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        print("didEnd")
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText.count <= 40 {
            self.postPageTagsDescriptionDelegate?.setTags(value: updatedText)
            return true
        } else {
            self.postPageTagsDescriptionDelegate?.setTags(value: updatedText)
            return false
        }
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("title text:\(String(describing: textField.text))")
        guard let updatedText = textField.text else {
            return
        }
        self.postPageTagsDescriptionDelegate?.setTags(value: updatedText)
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
