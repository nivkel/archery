//
//  PhotosViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Photos
import SwiftMessages
import Pickle
import NVActivityIndicatorView

class PhotosViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleCountLabel: UILabel!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var primaryImageView: UIImageView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func nextPageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        debugPrint("getImages count:\(postPageViewController.postPageImagesDelegate.getImages().count)")
        guard postPageViewController.postPageImagesDelegate.getImages().count > 1 else {
            Common.showStatusMessage(title: "Incomplete", message: "Add at least one photo", theme: .error, style: .bottom)
            return
        }
        guard let title = postPageViewController.titleText else {
            Common.showStatusMessage(title: "Incomplete", message: "Enter a valid title", theme: .error, style: .bottom)
            return
        }
        guard !title.isEmpty || title.count > 2 else {
            Common.showStatusMessage(title: "Incomplete", message: "Enter a valid title", theme: .error, style: .bottom)
            return
        }
        let nextPage = postPageViewController.pages[PostPages.categories.rawValue]
        postPageViewController.setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
    }
    
    var pageToViewPostDelegate: PageToViewPostProtocol?
    
    var postPageTitleDelegate: PostPageTitleDelegate?
    
    var photoCollectionDelegate: PhotoCollectionDelegate!
    
    typealias Placeholder = UIImage
    static func selectImage() -> Placeholder {
        return UIImage(named: "photo")!
//        return UIImage(icon: .FACamera, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
    }
    
    var images = [selectImage(), selectImage(), selectImage(), selectImage()] {
        didSet {
//            self.images.append(PhotosViewController.selectImage())
            for (idx, obj) in self.images.enumerated() {
                self.photoCollectionDelegate.attachments[idx] = obj
            }
//            self.photoCollectionDelegate.attachments = self.images
            debugPrint("self.images:\(self.photoCollectionDelegate.attachments.count)")
            DispatchQueue.main.async {
                let layout = UICollectionViewFlowLayout()
                self.photoCollectionView.collectionViewLayout = layout
                self.photoCollectionView.reloadData()
                self.primaryImageView.image = self.images.first!
            }
        }
    }
    var localUris = [String]()
    var imagesToRemove = [Int]()
    var selectedIndexPath: IndexPath!
    var selectedAssets = [PHAsset]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.primaryImageView.layer.cornerRadius = 10.0
        self.primaryImageView.clipsToBounds = true
        
        setupPhotoCollectionView()
        
        setupKeyboardHandling(textField: self.titleTextField, textView: nil, scrollView: self.scrollView)
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.postPageImagesDelegate = self
        
        nextButton.layer.cornerRadius = 10.0
        primaryImageView.setFAIconWithName(icon: .FAImage, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 100.0, height: 100.0))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageToViewPostDelegate?.textFieldFromPage(text: "Test page 1")
    }
    
    func showCamera() {
//        let configuration = Configuration()
//        configuration.doneButtonTitle = "Finish"
//        configuration.noImagesTitle = "Sorry! There are no images here!"
//        configuration.recordLocation = false
//
//        let imagePicker = ImagePickerController(configuration: configuration)
//        imagePicker.imageLimit = 4
//        imagePicker.delegate = self
//
//        self.present(imagePicker, animated: true, completion: nil)
        
//        let mediaViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
//        self.present(mediaViewController, animated: true) {
//            
//        }
        var parameters = Pickle.Parameters()
        parameters.allowedSelections = .limit(to: 1)
//        let initializer = {
//            return CustomCameraViewController.self
//        }
        let picker = ImagePickerController(selectedAssets: self.selectedAssets, configuration: parameters, cameraType: UIImagePickerController.self)
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    private func setupPhotoCollectionView() {
        self.photoCollectionDelegate = PhotoCollectionDelegate(collectionView: photoCollectionView, attachments: images)
                photoCollectionView.register(PhotoCollectionCell.nib, forCellWithReuseIdentifier: PhotoCollectionCell.reuseIdentifier)
//                photoCollectionDelegate?.pickerDelegate = self
                self.photoCollectionView.delegate = photoCollectionDelegate
                self.photoCollectionView.dataSource = photoCollectionDelegate
        self.photoCollectionDelegate.pickerDelegate = self
    }
}

extension PhotosViewController: PickerDelegate {
    func showPhotoOptions(sender: UIImage, indexPath: IndexPath) {
        debugPrint("showPhotoOptions:\(sender), index:\(indexPath)")
//        if self.images.count == indexPath.row + 1 {
//            showCamera()
//        } else {
//            self.primaryImageView.image = sender
//        }
        self.primaryImageView.image = sender
        showCamera()
        self.selectedIndexPath = indexPath
    }
    
    func updateAttachments(attachments: [UIImage]) {
        
    }
    
    func removeAttachment(sender: UIButton) {
        if let postPageViewController = self.parent as? PostPageViewController, let existingPost = postPageViewController.existingPost {
            Common.showConfirmationMessage(title: "", message: "Remove photo?", theme: .warning, style: .center) {
                let activity = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.midX - 50, y: self.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballPulse, color: Common.primaryColor, padding: 1.0)
                DispatchQueue.main.async {
                    self.view.addSubview(activity)
                    activity.startAnimating()
                }
                let updatePostInput = UpdateUserPostInput(id: existingPost.id)
                PostServicesManager.shared.editPostRemoveFile(existingPost: existingPost, input: updatePostInput, image: sender.tag).done {
                    debugPrint("editPostRemoveFile done")
                    self.images[sender.tag] = PhotosViewController.selectImage()
                }.ensure {
                    DispatchQueue.main.async {
                        activity.removeFromSuperview()
                        activity.stopAnimating()
                        Common.showNoticeMessage(title: "", message: "Photo removed", theme: .success, style: .center)
                    }
                }.catch { error in
                    debugPrint("editPostRemoveFile error: \(error)")
                }
            }
        } else {
            self.localUris.removeAll()
            let image = self.images[sender.tag]
//        let localUri = self.localUris[tag]
            debugPrint("removeAttachment image:\(image)")
            self.images[sender.tag] = PhotosViewController.selectImage()
        }
//        if !self.imagesToRemove.contains(sender.tag) {
//            self.imagesToRemove.append(sender.tag)
//            debugPrint("ImagesToRemove mark delete:\(imagesToRemove.count)")
//            sender.backgroundColor = .red
//        } else {
//            self.imagesToRemove.removeAll { (imageInArray) -> Bool in
//                return imageInArray == sender.tag
//            }
//            debugPrint("ImagesToRemove mark not delete:\(imagesToRemove.count)")
//            sender.backgroundColor = .lightGray
//        }
    }
}

extension PhotosViewController: PostPageImagesDelegate {
    func getImagesToRemove() -> [Int] {
        return self.imagesToRemove
    }
    
    func getImages() -> [UIImage] {
        return self.images
    }
    
    func getImagesUri() -> [String] {
        return self.localUris
    }
}

extension PhotosViewController: ImagePickerControllerDelegate {
    func imagePickerController(_ picker: ImagePickerController, shouldLaunchCameraWithAuthorization status: AVAuthorizationStatus) -> Bool {
        return true
    }
    
    func imagePickerController(_ picker: ImagePickerController, didFinishPickingImageAssets assets: [PHAsset]) {
        self.selectedAssets = assets
//        var images = [UIImage]()
        DispatchQueue.global().async {
            let group = DispatchGroup()
            let activity = NVActivityIndicatorView(frame: CGRect(x: picker.view.frame.midX - 50, y: picker.view.frame.midY, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballPulse, color: Common.primaryColor, padding: 1.0)
            DispatchQueue.main.async {
                picker.view.addSubview(activity)
                activity.startAnimating()
            }
            let option = PHImageRequestOptions()
//            self.localUris.removeAll()
            for asset in assets {
                group.enter()
                PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option) { (image, info) in
                    if let image = image {
                        debugPrint("requestImage image:\(image)")
                        self.images[self.selectedIndexPath.row] = image
//                            images.append(image)
                        let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
                        options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                            return true
                        }
                        asset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
                            if let imageUrl = contentEditingInput?.fullSizeImageURL?.absoluteString {
                                debugPrint("asset url:\(imageUrl)")
                                self.localUris = [imageUrl]
                            }
//                                self.localUris.append(contentEditingInput!.fullSizeImageURL!.absoluteString)
                            group.leave()
                        })
                    }
                }
            }
            group.notify(queue: .main) {
                debugPrint("requestImage complete:\(self.images.count)")
//                    self.images = images
                guard let postPageViewController = self.parent as? PostPageViewController, let existingPost = postPageViewController.existingPost else {
                    DispatchQueue.main.async {
                        activity.removeFromSuperview()
                        activity.stopAnimating()
                        self.dismiss(animated: true) { }
                    }
                    return
                }
                let updatePostInput = UpdateUserPostInput(id: existingPost.id)
                PostServicesManager.shared.editPostAddFile(existingPost: existingPost, input: updatePostInput, image: self.selectedIndexPath.row, imageUri: self.localUris.first!).done { file in
                    debugPrint("editPostAddFile done file:\(file.key)")
                    
//                        let object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region, localSourceFileUrl: file.localUri, mimeType: file.mimeType)
//                        AWSDataService.shared.s3Manager.upload(s3Object: object, completion: { (complete, error) in
//                            debugPrint("Secondary upload complete:\(complete) error:\(error)")
//                        })
                    
                    }.ensure {
                        DispatchQueue.main.async {
                            activity.removeFromSuperview()
                            activity.stopAnimating()
                            Common.showNoticeMessage(title: "", message: "Photo added", theme: .success, style: .center)
                            self.dismiss(animated: true) { }
                        }
                    }.catch { error in
                        debugPrint("editPostAddFile error: \(error)")
                }
            }
        }
        
        
//        self.localUris.removeAll()
//
//        for asset in assets {
//            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
//            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
//                return true
//            }
//            asset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
//                debugPrint("asset url:\(contentEditingInput!.fullSizeImageURL!.absoluteString)")
//                self.localUris.append(contentEditingInput!.fullSizeImageURL!.absoluteString)
//            })
//        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: ImagePickerController) {
        self.dismiss(animated: true) {

        }
    }
    
    
}

//extension PhotosViewController: ImagePickerDelegate {
//    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
////        debugPrint("ImagePicker wrapperDidPress:\(images)")
////        debugPrint("ImagePicker assets:\(imagePicker.stack.assets.first?.localIdentifier)")
//
//        self.images = images
////        self.photoCollectionDelegate.attachments = images
////        self.photoCollectionView.reloadData()
//    }
//
//    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
////        debugPrint("ImagePicker done:\(images)")
////        debugPrint("ImagePicker assets:\(imagePicker.stack.assets.first)")
//        self.images = images
////        self.photoCollectionDelegate.attachments = images
////        self.photoCollectionView.reloadData()
//        self.localUris.removeAll()
//
//        for asset in imagePicker.stack.assets {
//            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
//            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
//                return true
//            }
//            asset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
//                debugPrint("asset url:\(contentEditingInput!.fullSizeImageURL!.absoluteString)")
//                self.localUris.append(contentEditingInput!.fullSizeImageURL!.absoluteString)
//            })
//        }
//
//
//        self.dismiss(animated: true) {
//
//        }
//    }
//
//    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
//        debugPrint("ImagePicker cancel")
//        self.dismiss(animated: true) {
//
//        }
//    }
//}

extension PhotosViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("ShouldCharChange:\(textField.tag), superView:\(String(describing: textField.superview?.superview))")
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        if updatedText.count <= 40 {
            if !updatedText.isEmpty {
                self.titleCountLabel.text = "\(updatedText.count)/40"
            } else {
                self.titleCountLabel.text = "\(0)/40"
            }
            self.pageToViewPostDelegate?.textFieldFromPage(text: updatedText)
            self.postPageTitleDelegate?.setTitleText(value: updatedText)
            return true
        } else {
            return false
        }
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: (textField.superview?.frame.origin.y)!), animated: true)
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("title text:\(String(describing: textField.text))")
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
