//
//  PriceViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PriceViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var priceTextView: UITextView!
    @IBOutlet weak var negotiableSwitch: UISwitch!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var salePriceSwitch: UISwitch!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .categories, direction: .reverse)
    }
    
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func nextPageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        guard let _ = postPageViewController.price else {
            Common.showStatusMessage(title: "Incomplete", message: "Enter a price", theme: .error, style: .bottom)
            return
        }
        let nextPage = postPageViewController.pages[PostPages.shipping.rawValue]
        postPageViewController.setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
    }
    
    var postPagePriceDelegate: PostPagePriceDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupKeyboardHandling(textField: nil, textView: self.priceTextView, scrollView: self.scrollView)
        
        negotiableSwitch.addTarget(self, action: #selector(PriceViewController.handleNegotiableSwitch(sender:)), for: .valueChanged)
        
        salePriceSwitch.addTarget(self, action: #selector(PriceViewController.handleSalePriceSwitch(sender:)), for: .valueChanged)
        
        backButton.layer.cornerRadius = 10.0
        nextButton.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.postPagePriceDelegate.setPrice(value: self.priceTextView.text)
        self.postPagePriceDelegate.setNegotiable(value: self.negotiableSwitch.isOn)
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.fillInPriceFields()
        if let salePrice = postPageViewController.salePriceExists() {
            salePriceLabel.isHidden = false
            salePriceSwitch.isHidden = false
            salePriceLabel.text = "Enable Sale Price $\(salePrice)"
            self.postPagePriceDelegate.setSalePrice(value: self.salePriceSwitch.isOn)
        } else {
            salePriceLabel.isHidden = true
            salePriceSwitch.isHidden = true
        }
        
        if let text = self.priceTextView.text {
            if text.isEmpty {
                self.placeholderLabel.isHidden = false
            } else {
                self.placeholderLabel.isHidden = true
            }
        } else {
            self.placeholderLabel.isHidden = false
        }
    }

    @objc private func handleNegotiableSwitch(sender: UISwitch) {
        self.postPagePriceDelegate.setNegotiable(value: sender.isOn)
    }
    
    @objc private func handleSalePriceSwitch(sender: UISwitch) {
        self.postPagePriceDelegate.setSalePrice(value: sender.isOn)
    }
}

extension PriceViewController {
    override func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: (textView.superview?.frame.origin.y)!), animated: true)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        let allowedCharacters = CharacterSet.decimalDigits.union (CharacterSet (charactersIn: "."))
        let characterSet = CharacterSet(charactersIn: updatedText)
        let validCharacters = allowedCharacters.isSuperset(of: characterSet)
        
        let maxLength = 10
        let newString = updatedText as NSString
        let validLength = newString.length <= maxLength
        let charge = String(newString)
        debugPrint("price string :\(charge)")
        
        if validCharacters && validLength {
            self.postPagePriceDelegate.setPrice(value: charge)
        }
        
        if updatedText.isEmpty {
            self.placeholderLabel.isHidden = false
        } else {
            self.placeholderLabel.isHidden = true
        }
        
        return validCharacters && validLength
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        print("CharChange:\(textView.text.count)")
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        print("didEnd")
    }
}
