//
//  SelectCategoryHeader.swift
//  archery
//
//  Created by Kelvin Lee on 1/10/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class SelectCategoryHeader: UICollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
