//
//  SelectCategorySectionController.swift
//  archery
//
//  Created by Kelvin Lee on 11/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class SelectCategorySectionController: ListSectionController {
    
    var category: Category!
    
    override init() {
        super.init()
        supplementaryViewSource = self
        
        self.inset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 10)
        self.minimumLineSpacing = 8.0 // Between rows
        self.minimumInteritemSpacing = 10.0 // Between items
    }
    
    override func numberOfItems() -> Int {
        return category.subcategories.count
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = self.collectionContext!.containerSize.width
        return CGSize(width: width / 2 - 16, height: 180)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: SelectCategoryCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! SelectCategoryCell
        cell.titleLabel.text = self.category.subcategories[index]
        cell.iconImageView.setFAIconWithName(icon: .FAAsterisk, textColor: Common.secondaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 20.0, height: 20.0))
        
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.category = object as? Category
    }
    
    override func didSelectItem(at index: Int) {
        guard let selectCategoryViewController = self.viewController as? SelectCategoryViewController else {
            return
        }
        let selected = self.category.subcategories[index]
        selectCategoryViewController.selectCategory(category: selected)
    }
}

extension SelectCategorySectionController: ListSupplementaryViewSource {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader/*, UICollectionElementKindSectionFooter*/]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
//        case UICollectionElementKindSectionFooter:
//            return userFooterView(atIndex: index)
        default:
            fatalError()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 50)
    }
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
//        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, for: self, nibName: "SelectCategoryCell", bundle: nil, at: index) as? SelectCategoryCell else {
//            fatalError()
//        }
        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, nibName: "SelectCategoryHeader", bundle: nil, at: index) as? SelectCategoryHeader else {
            fatalError()
        }
        view.titleLabel.text = category.name
        view.iconImageView.setFAIconWithName(icon: .FATags, textColor: Common.secondaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 20.0, height: 20.0))
        
        return view
    }
    
//    private func userFooterView(atIndex index: Int) -> UICollectionReusableView {
//        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, for: self, nibName: "SelectCategoryCell", bundle: nil, at: index) as? SelectCategoryCell else {
//            fatalError()
//        }
//        view.titleLabel.text = "Footer"
//        return view
//    }
}
