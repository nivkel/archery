//
//  SelectCategoryViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class SelectCategoryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = [Category(name: "Filter", subcategories: ["Clear"]), Category(name: "Bows", subcategories: ["Recurve Bow", "Long Bow", "Compound Bow"]), Category(name: "Arrows", subcategories: ["Feather Arrows", "Vanes Arrows"]), Category(name: "Accessories", subcategories: ["Sight", "Rest"]), Category(name: "Gear", subcategories: ["Quiver", "Arm Guard", "Gloves", "Bag"])]
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()
    
    var postPageCategoryDelegate: PostPageCategoryDelegate!
    
    var isPostPage: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isPostPage {
            self.categories.remove(at: 0)
        }
        
        let layout = ListCollectionViewLayout(stickyHeaders: false, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
//        layout.minimumLineSpacing = 2.0
//        layout.minimumInteritemSpacing = 2.0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.contentInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 0.0)
        
        self.collectionView.register(SelectCategoryCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SelectCategoryCell.reuseIdentifier)
        self.collectionView.register(SelectCategoryCell.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: SelectCategoryCell.reuseIdentifier)
        
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(SelectCategoryViewController.close))
        closeButton.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = closeButton
        
        self.navigationItem.title = "Categories"
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func selectCategory(category: String) {
        debugPrint("selectCategory:\(category)")
        self.postPageCategoryDelegate.setCategory(value: category)
        self.close()
    }
}

extension SelectCategoryViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {

    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension SelectCategoryViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.categories as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        default:
            return SelectCategorySectionController()
        }
        
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        
        
        return UIView()
    }
}
