//
//  ShippingViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/24/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

class ShippingViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var meetPickupSwitch: UISwitch!
    @IBOutlet weak var shipNationallySwitch: UISwitch!
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    @IBOutlet weak var getLocationButton: UIButton!
    @IBAction func getLocationButtonAction(_ sender: UIButton) {
        handleLocation()
    }
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func backButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.goToPage(page: .price, direction: .reverse)
    }
    
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func nextPageButtonAction(_ sender: UIButton) {
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        guard let _ = postPageViewController.price else {
            Common.showStatusMessage(title: "Incomplete", message: "Enter a zip code", theme: .error, style: .bottom)
            return
        }
        let nextPage = postPageViewController.pages[PostPages.publish.rawValue]
        postPageViewController.setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
    }
    
    var postPageShippingDelegate: PostPageShippingDelegate?
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupKeyboardHandling(textField: self.zipCodeTextField, textView: nil, scrollView: self.scrollView)
        
        mapView.delegate = self
        
//        mapView.register(UserMapMarkerView.self,
//                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.register(UserMapView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
//        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
//        centerMapOnLocation(location: initialLocation)
        
        meetPickupSwitch.rx.isOn.changed.debounce(0.1, scheduler: MainScheduler.instance)
            .distinctUntilChanged().asObservable()
            .subscribe(onNext:{[weak self] value in
                debugPrint("meetPickupSwitch vlue:\(value)")
                self?.postPageShippingDelegate?.setMeetPickup(value: value)
            }).disposed(by: disposeBag)
        
        shipNationallySwitch.rx.isOn.changed.debounce(0.1, scheduler: MainScheduler.instance)
            .distinctUntilChanged().asObservable()
            .subscribe(onNext:{[weak self] value in
                debugPrint("shipNationallySwitch vlue:\(value)")
                self?.postPageShippingDelegate?.setShipNationally(value: value)
            }).disposed(by: disposeBag)
        
        backButton.layer.cornerRadius = 10.0
        nextButton.layer.cornerRadius = 10.0
        getLocationButton.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.postPageShippingDelegate?.setMeetPickup(value: self.meetPickupSwitch.isOn)
        self.postPageShippingDelegate?.setShipNationally(value: self.shipNationallySwitch.isOn)
        
        guard let postPageViewController = self.parent as? PostPageViewController else {
            return
        }
        postPageViewController.fillInShippingFields()
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
//        self.mapView.setCenter(location.coordinate, animated: true)
        let span = MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: false)
    }
    
    func handleLocation() {
        guard let zipCode = self.zipCodeTextField.text else {
            return
        }
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(zipCode) { (placemarks, error) in
            debugPrint("placemarks:\(placemarks)")
            if let error = error {
                debugPrint(error)
            }
            if let placemark = placemarks?.first {
                let lat = placemark.location!.coordinate.latitude
                let long = placemark.location!.coordinate.longitude
                let icon = UIImage(icon: .FACircle, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor).image(alpha: 0.5)!
                //                if let country = placemark.country {
                //
                //                }
                var title = ""
                var subtitle = ""
                if let state = placemark.administrativeArea, let county = placemark.subAdministrativeArea {
                    title = "\(county), \(state)"
                }
                
                if let city = placemark.locality, let neighborhood = placemark.subLocality {
                    subtitle = "\(city), \(neighborhood)"
                }
                
                let annotation = UserMapAnnotation(title: title, name: subtitle, coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), image: icon)
                self.mapView.removeAnnotations(self.mapView.annotations)
                self.mapView.addAnnotation(annotation)
                
                let initialLocation = CLLocation(latitude: lat, longitude: long)
                
                self.centerMapOnLocation(location: initialLocation)
                
                if let coordinate = placemark.location?.coordinate {
                    let coordinateString = String(coordinate.latitude) + "," +  String(coordinate.longitude)
                    self.postPageShippingDelegate?.setLocation(value: coordinateString)
                }
            }
        }
    }

}

extension ShippingViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: updatedText)
        let validCharacters = allowedCharacters.isSuperset(of: characterSet)
        
        let maxLength = 5
        let newString = updatedText as NSString
        let validLength = newString.length <= maxLength
        
        return validCharacters && validLength
    }
}

extension ShippingViewController: MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        guard let annotation = annotation as? UserMapAnnotation else { return nil }
//        // 3
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        // 4
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            // 5
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = true
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let annotation = views.first?.annotation {
            mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: mapView.region.span)
//        mapView.setRegion(region, animated: true)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: view.annotation!.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}
