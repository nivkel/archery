//
//  BaseCollectionDelegate.swift
//  archery
//
//  Created by Kelvin Lee on 10/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class BaseCollectionDelegate: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    
    private var items: [String]!
    private var type: ItemType!
    
    var selectedItems: [String]?
    
    var selected = [String]()
    
    weak var itemSelectDelegate: ItemSelectDelegate?
    
    weak var collectionSelectionDelegate: CollectionSelectionDelegate?
    
    enum ItemType {
        case condition
        case shipping
    }

    init(collectionView: UICollectionView) {
        super.init()
        self.collectionView = collectionView
        
    }
    
    init(collectionView: UICollectionView, items: [String], type: ItemType) {
        super.init()
        self.collectionView = collectionView
        self.items = items
        self.type = type
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if items == nil {
            return
        }
        let item = self.items[indexPath.row]
        debugPrint("selected will display:\(self.selected)")
        if self.selected.contains(item) {
            DispatchQueue.main.async {
                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
            }
        } else {
            DispatchQueue.main.async {
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectCollectionCell.reuseIdentifier, for: indexPath) as! SelectCollectionCell
        let item = self.items[indexPath.row]
        cell.nameLabel.text = item
        debugPrint("selected cell for item:\(self.selected)")
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        self.itemSelectDelegate?.selectItem(item: item, type: self.type)
        if self.type == .shipping {
            self.selected.append(item)
        } else {
            self.selected = [item]
        }
        self.collectionSelectionDelegate?.collectionSelection(items: self.selected)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        if self.type == .shipping {
            self.itemSelectDelegate?.deselectItem(item: item, type: self.type)
        }
        if let index = self.selected.index(of: item) {
            self.selected.remove(at: index)
            self.collectionSelectionDelegate?.collectionSelection(items: self.selected)
        }
        debugPrint("did deselect:\(item)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 100.0, height: 60.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 0.0, right: 10.0)
    }
}
