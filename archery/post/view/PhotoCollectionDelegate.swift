//
//  PhotoCollectionDelegate.swift
//  archery
//
//  Created by Kelvin Lee on 10/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PhotoCollectionDelegate: BaseCollectionDelegate {
    var attachments: [UIImage]!
    weak var pickerDelegate: PickerDelegate?
    
    init(collectionView: UICollectionView, attachments: [UIImage]?) {
        super.init(collectionView: collectionView)
        
        self.attachments = attachments
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(PhotoCollectionDelegate.handleLongGesture(gesture:)))
        collectionView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleRemoveAttachment(sender: UIButton) {
        print("HandleRemove:\(sender.tag)")
        self.pickerDelegate?.removeAttachment(sender: sender)
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        var animator: UIViewPropertyAnimator!
        switch(gesture.state) {
        case UIGestureRecognizer.State.began:
            guard let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) else {
                break
            }
//            if selectedIndexPath.row == (self.attachments.count - 1) {
//                return
//            }
            let cell = self.collectionView.cellForItem(at: selectedIndexPath) as! PhotoCollectionCell
            animator = UIViewPropertyAnimator(duration: 0.5, curve: .easeIn) {
                cell.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }
            animator.startAnimation()
            collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case UIGestureRecognizer.State.changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case UIGestureRecognizer.State.ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let attachment = self.attachments.remove(at: sourceIndexPath.row)
        self.attachments.insert(attachment, at: destinationIndexPath.row)
        
        self.pickerDelegate?.updateAttachments(attachments: self.attachments as! [UIImage])
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        debugPrint("self.images numberOfRowss:\(self.attachments.count)")
        return self.attachments.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionCell.reuseIdentifier, for: indexPath) as! PhotoCollectionCell
        cell.removeButton.tag = indexPath.row
        let attachment = self.attachments[indexPath.row]
        cell.removeButton.backgroundColor = .red
        cell.removeButton.addTarget(self, action: #selector(PhotoCollectionDelegate.handleRemoveAttachment), for: .touchUpInside)
        if attachment.size == PhotosViewController.selectImage().size {
            cell.removeButton.isHidden = true
        } else {
            cell.removeButton.isHidden = false
        }
//        if indexPath.row == (self.attachments.count - 1) {
//            cell.removeButton.isHidden = true
//        } else {
//            cell.removeButton.isHidden = false
//        }
        DispatchQueue.main.async {
            cell.photo.image = attachment
        }
        debugPrint("self.images cellForRow:\(attachment), count:\(self.attachments.count) indexPath:\(indexPath.row)")
        
        return cell
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let attachment = self.attachments[indexPath.row] as? UIImage {
            self.pickerDelegate?.showPhotoOptions(sender: attachment, indexPath: indexPath)
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60.0, height: 80.0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
