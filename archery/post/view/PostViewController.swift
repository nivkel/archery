//
//  PostViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/16/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {
    
    var presenter: ViewToPresenterPostProtocol?

    @IBOutlet weak var pageContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        debugPrint("child vc:\(self.children.first?.children)")
        
        self.title = "Post"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PostViewController: PresenterToViewPostProtocol {
    func postPublished(response: Post) {
        
    }
    
    func showError() {
        
    }
}
