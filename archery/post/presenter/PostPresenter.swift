//
//  PostPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostPresenter: ViewToPresenterPostProtocol {
    var view: PresenterToViewPostProtocol?
    
    var interactor: PresenterToInteractorPostProtocol?
    
    var router: PresenterToRouterPostProtocol?
    
    func startPublishPost(input: CreatePostWithFilesInput, images: [String]) {
        interactor?.publishPost(input: input, images: images)
    }
    
    func showPostNextStep(navigationController: UINavigationController) {
        
    }
}

extension PostPresenter: InteractorToPresenterPostProtocol {
    func publishPostSuccess(response: Post) {
        view?.postPublished(response: response)
    }
    
    func publishPostFailed() {
        view?.showError()
    }
    
    
}
