//
//  Mapping.swift
//  archery
//
//  Created by Kelvin Lee on 12/31/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import IGListKit
import AWSAppSync

class Mapping: NSObject {
    var id: GraphQLID
    var userId: GraphQLID
    var type: String
    var postId: GraphQLID
    var post: Post
    
    init(id: GraphQLID, userId: GraphQLID, type: String, postId: GraphQLID, post: Post) {
        self.id = id
        self.userId = userId
        self.type = type
        self.postId = postId
        self.post = post
    }
}
