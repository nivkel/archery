//
//  Category.swift
//  archery
//
//  Created by Kelvin Lee on 11/20/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class Category: ListDiffable {
    let name: String
    let subcategories: [String]
    
    init(name: String, subcategories: [String]) {
        self.name = name
        self.subcategories = subcategories
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return name as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? Category {
            return name == object.name
        }
        return false
    }
    

}
