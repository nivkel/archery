//
//  PostInteractor.swift
//  archery
//
//  Created by Kelvin Lee on 10/17/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class PostInteractor: PresenterToInteractorPostProtocol {
    var presenter: InteractorToPresenterPostProtocol?
    
    func publishPost(input: CreatePostWithFilesInput, images: [String]) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let dataService = appDelegate.dataService
//        dataService?.publishPostWithFiles(input: input, images: images)
        PostServicesManager.shared.publishPostWithFiles(input: input, images: images).get { post in
            self.presenter?.publishPostSuccess(response: post)
        }.catch { error in
            self.presenter?.publishPostFailed()
        }
    }
    
    func saveEditedPost(input: UpdatePostInput, images: [String]) {
        
    }
}
