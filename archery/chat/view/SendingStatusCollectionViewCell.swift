//
//  SendingStatusCollectionViewCell.swift
//  archery
//
//  Created by Kelvin Lee on 10/19/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class SendingStatusCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var label: UILabel!
    
    var text: NSAttributedString? {
        didSet {
            self.label.attributedText = self.text
        }
    }
}
