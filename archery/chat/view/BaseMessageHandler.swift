//
//  BaseMessageHandler.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import Chatto
import ChattoAdditions

public protocol ChatMessageViewModelProtocol {
    var messageModel: ChatMessageModelProtocol { get }
}

class BaseMessageHandler {
    
    private let messageSender: ChatMessageSender
    private let messagesSelector: MessagesSelectorProtocol
    private var conversationId: String!
    
    init(messageSender: ChatMessageSender, messagesSelector: MessagesSelectorProtocol, conversationId: String) {
        self.messageSender = messageSender
        self.messagesSelector = messagesSelector
        self.conversationId = conversationId
    }
    func userDidTapOnFailIcon(viewModel: ChatMessageViewModelProtocol) {
        print("userDidTapOnFailIcon")
        self.messageSender.sendMessage(viewModel.messageModel as! ChatTextMessageModel, conversationId: self.conversationId)
    }
    
    func userDidTapOnAvatar(viewModel: MessageViewModelProtocol) {
        print("userDidTapOnAvatar")
    }
    
    func userDidTapOnBubble(viewModel: ChatMessageViewModelProtocol) {
        print("userDidTapOnBubble")
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatMessageViewModelProtocol) {
        print("userDidBeginLongPressOnBubble")
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatMessageViewModelProtocol) {
        print("userDidEndLongPressOnBubble")
    }
    
    func userDidSelectMessage(viewModel: ChatMessageViewModelProtocol) {
        print("userDidSelectMessage")
        self.messagesSelector.selectMessage(viewModel.messageModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatMessageViewModelProtocol) {
        print("userDidDeselectMessage")
        self.messagesSelector.deselectMessage(viewModel.messageModel)
    }
}
