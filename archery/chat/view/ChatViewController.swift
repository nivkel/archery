//
//  ChatViewController.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions
import PromiseKit
import NVActivityIndicatorView

class ChatViewController: BaseChatViewController {
    
    var chatInputPresenter: BasicChatInputBarPresenter!
    
    var messageSender: ChatMessageSender!
    let messagesSelector = BaseMessagesSelector()
    
    var conversationId: String!
    
    var titleValue: String!
    
    var dataSource: ChatDataSource! {
        didSet {
            self.chatDataSource = self.dataSource
            self.messageSender = self.dataSource.messageSender
        }
    }
    
    lazy private var baseMessageHandler: BaseMessageHandler = {
        return BaseMessageHandler(messageSender: self.messageSender, messagesSelector: self.messagesSelector, conversationId: self.conversationId)
    }()
    
    override func createPresenterBuilders() -> [ChatItemType : [ChatItemPresenterBuilderProtocol]] {
        let textMessagePresenter = TextMessagePresenterBuilder(
            viewModelBuilder: ChatTextMessageViewModelBuilder(),
            interactionHandler: ChatTextMessageHandler(baseHandler: self.baseMessageHandler)
        )
        //// Chat bubble colors
        let chatColor = BaseMessageCollectionViewCellDefaultStyle.Colors(
            incoming: UIColor.lightGray, // white background for incoming
            outgoing: Common.primaryColor // black background for outgoing
        )
        // used for base message background + text background
        let baseMessageStyle = BaseMessageCollectionViewCellAvatarStyle(colors: chatColor)
        
        let textStyle = TextMessageCollectionViewCellDefaultStyle.TextStyle(
            font: UIFont.systemFont(ofSize: 13),
            incomingColor: UIColor.white, // black text for incoming
            outgoingColor: UIColor.white, // white text for outgoing
            incomingInsets: UIEdgeInsets(top: 10, left: 19, bottom: 10, right: 15),
            outgoingInsets: UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 19)
        )
        let textCellStyle: TextMessageCollectionViewCellDefaultStyle = TextMessageCollectionViewCellDefaultStyle(
            textStyle: textStyle,
            baseStyle: baseMessageStyle) // without baseStyle, you won't have the right background
        textMessagePresenter.baseMessageStyle = baseMessageStyle//BaseMessageCollectionViewCellAvatarStyle()
        textMessagePresenter.textCellStyle = textCellStyle
        ////
        
        let photoMessagePresenter = PhotoMessagePresenterBuilder(
            viewModelBuilder: ChatPhotoMessageViewModelBuilder(),
            interactionHandler: ChatPhotoMessageHandler(baseHandler: self.baseMessageHandler)
        )
        photoMessagePresenter.baseCellStyle = BaseMessageCollectionViewCellAvatarStyle()
        
        return [
            ChatTextMessageModel.chatItemType: [textMessagePresenter],
            ChatPhotoMessageModel.chatItemType: [photoMessagePresenter],
            SendingStatusModel.chatItemType: [SendingStatusPresenterBuilder()],
            TimeSeparatorModel.chatItemType: [TimeSeparatorPresenterBuilder()]
        ]
    }
    
    override func createChatInputView() -> UIView {
        let chatInputView = ChatInputBar.loadNib()
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = NSLocalizedString("Send", comment: "")
        appearance.textInputAppearance.placeholderText = NSLocalizedString("Type a message", comment: "")
        self.chatInputPresenter = BasicChatInputBarPresenter(chatInputBar: chatInputView, chatInputItems: self.createChatInputItems(), chatInputBarAppearance: appearance)
        chatInputView.maxCharactersCount = 1000
        return chatInputView
    }
    
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
//        items.append(self.createPhotoInputItem())
        return items
    }
    
    private func createTextInputItem() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { [weak self] text in
            // Your handling logic
            self?.dataSource.addTextMessage(text)
        }
        return item
    }
    
    private func createPhotoInputItem() -> PhotosChatInputItem {
        let item = PhotosChatInputItem(presentingController: self)
        item.photoInputHandler = { [weak self] image in
            // Your handling logic
            self?.dataSource.addPhotoMessage(image)
        }
        return item
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = self.titleValue
        self.messagesSelector.delegate = self
        self.chatItemsDecorator = ChatItemsDecorator(messagesSelector: self.messagesSelector)
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            topItem.backBarButtonItem?.tintColor = Common.primaryColor
        }
        
        self.loadMessages()
        
        self.subscribeToMessages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func subscribeToMessages() {
        firstly {
            ChatServicesManager.shared.getUserId()
            }.then { userId in
                self.dataSource.subscribeToMessages(conversationId: self.conversationId, senderId: userId)
            }.ensure {
                
            }.catch { error in
                debugPrint("subscribeToMessages error:\(error)")
        }
    }
    
    func loadMessages() {
        let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Loading", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        firstly {
            ChatServicesManager.shared.getUserId()
            }.then { userId in
                ChatServicesManager.shared.messagesForConversation(id: self.conversationId, senderId: userId)
            }.get { objects in
                self.dataSource.loadMessages(messages: objects.0, senderId: objects.1)
            }.ensure {
                NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }.catch { error in
                debugPrint("messagesForConversation error:\(error)")
        }
    }
}

extension ChatViewController: MessagesSelectorDelegate {
    func messagesSelector(_ messagesSelector: MessagesSelectorProtocol, didSelectMessage: MessageModelProtocol) {
        self.enqueueModelUpdate(updateType: .normal)
    }
    
    func messagesSelector(_ messagesSelector: MessagesSelectorProtocol, didDeselectMessage: MessageModelProtocol) {
        self.enqueueModelUpdate(updateType: .normal)
    }
}
