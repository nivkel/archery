//
//  ChatMessageSender.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import Chatto
import ChattoAdditions
import PromiseKit

public protocol ChatMessageModelProtocol: MessageModelProtocol {
    var status: MessageStatus { get set }
}

public class ChatMessageSender {
    
    public var onMessageChanged: ((_ message: ChatMessageModelProtocol) -> Void)?
    
    public func sendMessages(_ messages: [ChatMessageModelProtocol]) {
        for message in messages {
            self.fakeMessageStatus(message)
        }
    }
    
    public func sendMessage(_ message: ChatTextMessageModel, conversationId: String) {
        debugPrint("Sending message..... :\(message.text), conversationId:\(conversationId), date:\(Common.createdAtDate())")
        self.updateMessage(message, status: .sending)
        firstly {
            ChatServicesManager.shared.addMessage(message: message, conversationId: conversationId, text: message.text)
            }.get { id in
                debugPrint("sendMessage success:\(id)")
            }.ensure {
                self.updateMessage(message, status: .success)
            }.catch { error in
                debugPrint("sendMessage error:\(error)")
                self.updateMessage(message, status: .failed)
        }
//        self.fakeMessageStatus(message)
    }
    
    public func sendPhoto(_ message: ChatPhotoMessageModel, conversationId: String) {
        
        self.fakeMessageStatus(message)
    }
    
    private func fakeMessageStatus(_ message: ChatMessageModelProtocol) {
        switch message.status {
        case .success:
            break
        case .failed:
            self.updateMessage(message, status: .sending)
            self.fakeMessageStatus(message)
        case .sending:
            switch arc4random_uniform(100) % 5 {
            case 0:
                if arc4random_uniform(100) % 2 == 0 {
                    self.updateMessage(message, status: .failed)
                } else {
                    self.updateMessage(message, status: .success)
                }
            default:
                let delaySeconds: Double = Double(arc4random_uniform(1200)) / 1000.0
                let delayTime = DispatchTime.now() + Double(Int64(delaySeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.fakeMessageStatus(message)
                }
            }
        }
    }
    
    private func updateMessage(_ message: ChatMessageModelProtocol, status: MessageStatus) {
        if message.status != status {
            message.status = status
            self.notifyMessageChanged(message)
        }
    }
    
    private func notifyMessageChanged(_ message: ChatMessageModelProtocol) {
        self.onMessageChanged?(message)
    }
}
