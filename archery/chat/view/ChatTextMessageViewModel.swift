//
//  ChatTextMessageViewModel.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import ChattoAdditions

public class ChatTextMessageViewModel: TextMessageViewModel<ChatTextMessageModel>, ChatMessageViewModelProtocol {
    
    public override init(textMessage: ChatTextMessageModel, messageViewModel: MessageViewModelProtocol) {
        super.init(textMessage: textMessage, messageViewModel: messageViewModel)
    }
    
    public var messageModel: ChatMessageModelProtocol {
        return self.textMessage
    }
}

public class ChatTextMessageViewModelBuilder: ViewModelBuilderProtocol {
    public init() {}
    
    let messageViewModelBuilder = MessageViewModelDefaultBuilder()
    
    public func createViewModel(_ textMessage: ChatTextMessageModel) -> ChatTextMessageViewModel {
        let messageViewModel = self.messageViewModelBuilder.createMessageViewModel(textMessage)
        let textMessageViewModel = ChatTextMessageViewModel(textMessage: textMessage, messageViewModel: messageViewModel)
        
        if textMessage.isIncoming {
            textMessageViewModel.avatarImage.value = UIImage(icon: .FAUserCircle, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: .gray, backgroundColor: .clear)
        } else {
            textMessageViewModel.avatarImage.value = UIImage(icon: .FAUserCircle, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: .clear)
        }
        return textMessageViewModel
    }
    
    public func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatTextMessageModel
    }
}
