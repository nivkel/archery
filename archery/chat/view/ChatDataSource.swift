//
//  ChatDataSource.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions
import PromiseKit

class ChatDataSource: ChatDataSourceProtocol {
    var slidingWindow: SlidingDataSource<ChatItemProtocol>!
    
    var nextMessageId: Int = 0
    let preferredMaxWindowSize = 500
    
    private var conversationId: String!
    
    init(count: Int, pageSize: Int, conversationId: String) {
        self.conversationId = conversationId
//        self.slidingWindow = SlidingDataSource(count: count, pageSize: pageSize) { [weak self] () -> ChatItemProtocol in
//            guard let sSelf = self else { return (self?.makeTextMessage("1", text: "test 1", isIncoming: true))! }
//            defer { sSelf.nextMessageId += 1 }
//            return (self?.makeTextMessage("2", text: "test 2", isIncoming: false))!
//        }
        self.slidingWindow = SlidingDataSource(items: [Message](), pageSize: 20)
    }
    
    lazy var messageSender: ChatMessageSender = {
        let sender = ChatMessageSender()
        sender.onMessageChanged = { [weak self] (message) in
            guard let sSelf = self else { return }
            sSelf.delegate?.chatDataSourceDidUpdate(sSelf)
        }
        return sender
    }()
    
    var hasMoreNext: Bool {
        return self.slidingWindow.hasMore()
    }
    
    var hasMorePrevious: Bool {
        return self.slidingWindow.hasPrevious()
    }
    
    var chatItems: [ChatItemProtocol] {
        return self.slidingWindow.itemsInWindow
    }
    
    weak var delegate: ChatDataSourceDelegateProtocol?
    
    func loadNext() {
        self.slidingWindow.loadNext()
        self.slidingWindow.adjustWindow(focusPosition: 1, maxWindowSize: self.preferredMaxWindowSize)
        self.delegate?.chatDataSourceDidUpdate(self, updateType: .pagination)
    }
    
    func loadPrevious() {
        self.slidingWindow.loadPrevious()
        self.slidingWindow.adjustWindow(focusPosition: 0, maxWindowSize: self.preferredMaxWindowSize)
        self.delegate?.chatDataSourceDidUpdate(self, updateType: .pagination)
    }
    
    func subscribeToMessages(conversationId: String, senderId: String) -> Promise<()> {
        return Promise { seal in
            do {
                let subscription = SubscribeToNewMessageSubscription(conversationId: conversationId)
                AWSDataService.shared.discard = try AWSDataService.shared.appSyncClient?.subscribe(subscription: subscription, resultHandler: { (result, transaction, error) in
                    if let result = result {
//                        debugPrint("SubscribeToNewMessageSubscription result:\(result.data?.subscribeToNewMessage)")
                        guard let newMessage = result.data?.subscribeToNewMessage else {
                            return
                        }
                        let message = Message(id: newMessage.id, content: newMessage.content, conversationId: newMessage.conversationId, createdAt: newMessage.createdAt!, sender: newMessage.sender!, senderName: newMessage.senderName!, isSent: newMessage.isSent!)
                        debugPrint("subscribeToMessages msg:\(message)")
                        if message.sender != senderId {
                            let messageModel = self.makeTextMessage(message.id, text: message.content, senderId: message.sender, isIncoming: true, status: .success, date: Common.createdAtDate(value: message.createdAt))
                            self.slidingWindow.insertItem(messageModel, position: .bottom)
                            self.delegate?.chatDataSourceDidUpdate(self)
                        }
                        seal.fulfill(())
                    } else if let error = error {
                        print("SubscribeToNewMessageSubscription error:\(error.localizedDescription)")
                        seal.reject(error)
                    }
                })
            } catch {
                print("Error starting subscription.")
            }
        }
    }
    
    func loadMessages(messages: [Message], senderId: String) {
        for message in messages {
            var isIncoming: Bool = false
            if message.sender != senderId {
                isIncoming = true
            }
            let textMessageModel = self.makeTextMessage(message.id, text: message.content, senderId: message.sender, isIncoming: isIncoming, status: .success, date: Common.createdAtDate(value: message.createdAt))
            self.slidingWindow.insertItem(textMessageModel, position: .top)
        }
        self.delegate?.chatDataSourceDidUpdate(self)
    }
    
    func addTextMessage(_ text: String) {
        let uid = UUID().uuidString
        debugPrint("msg uid:\(uid)")
        firstly {
            ChatServicesManager.shared.getUserId()
            }.get { userId in
                debugPrint("addTextMessage userId:\(userId)")
                let message = self.makeTextMessage(uid, text: text, senderId: userId, isIncoming: false, status: .sending, date: Date())
                self.messageSender.sendMessage(message, conversationId: self.conversationId)
                self.slidingWindow.insertItem(message, position: .bottom)
            }.ensure {
                self.delegate?.chatDataSourceDidUpdate(self)
            }.catch { error in
        }
    }
    
    func addPhotoMessage(_ image: UIImage) {
        let uid = UUID().uuidString
        debugPrint("msg uid:\(uid)")
        firstly {
            ChatServicesManager.shared.getUserId()
            }.get { userId in
                let message = self.makePhotoMessage(uid, image: image, size: image.size, senderId: userId, isIncoming: false, status: .sending, date: Date())
                self.messageSender.sendPhoto(message, conversationId: self.conversationId)
                self.slidingWindow.insertItem(message, position: .bottom)
            }.ensure {
                self.delegate?.chatDataSourceDidUpdate(self)
            }.catch { error in
        }
    }
    
    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
        let didAdjust = self.slidingWindow.adjustWindow(focusPosition: focusPosition, maxWindowSize: preferredMaxCount ?? self.preferredMaxWindowSize)
        completion(didAdjust)
    }
    
    func makeTextMessage(_ uid: String, text: String, senderId: String, isIncoming: Bool, status: MessageStatus, date: Date) -> ChatTextMessageModel {
        let messageModel = self.makeMessageModel(uid, senderId: senderId, isIncoming: isIncoming, type: TextMessageModel<MessageModel>.chatItemType, status: status, date: date)
        let textMessageModel = ChatTextMessageModel(messageModel: messageModel, text: text)
        return textMessageModel
    }
    
    func makePhotoMessage(_ uid: String, image: UIImage, size: CGSize, senderId: String, isIncoming: Bool, status: MessageStatus, date: Date) -> ChatPhotoMessageModel {
        let messageModel = self.makeMessageModel(uid, senderId: senderId, isIncoming: isIncoming, type: PhotoMessageModel<MessageModel>.chatItemType, status: status, date: date)
        let photoMessageModel = ChatPhotoMessageModel(messageModel: messageModel, imageSize: size, image: image)
        return photoMessageModel
    }
    
    private func makeMessageModel(_ uid: String, senderId: String, isIncoming: Bool, type: String, status: MessageStatus, date: Date) -> MessageModel {
        return MessageModel(uid: uid, senderId: senderId, type: type, isIncoming: isIncoming, date: date, status: status)
    }
}

extension TextMessageModel {
    static var chatItemType: ChatItemType {
        return "text"
    }
}

extension PhotoMessageModel {
    static var chatItemType: ChatItemType {
        return "photo"
    }
}
