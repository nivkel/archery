//
//  ChatTextMessageModel.swift
//  archery
//
//  Created by Kelvin Lee on 10/18/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import ChattoAdditions

public class ChatTextMessageModel: TextMessageModel<MessageModel>, ChatMessageModelProtocol {
    public override init(messageModel: MessageModel, text: String) {
        super.init(messageModel: messageModel, text: text)
    }
    
    public var status: MessageStatus {
        get {
            return self._messageModel.status
        }
        set {
            self._messageModel.status = newValue
        }
    }
}
