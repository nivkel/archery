//
//  AWSPinpointManager.swift
//  archery
//
//  Created by Kelvin Lee on 10/10/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSCore
import AWSPinpoint
import AWSMobileClient

enum AnalyticsEvents: String {
    case postOpen = "postOpen"
}

enum AnalyticsAttributeKeys: String {
    case postId = "postId"
    case postTitle = "postTitle"
    case postPrice = "postPrice"
}

class AWSPinpointManager: NSObject {
    
    static let shared = AWSPinpointManager()
    // open post
    //
    func logEvent(eventType: AnalyticsEvents, attributeValue: String, attributeKey: AnalyticsAttributeKeys, metric: NSNumber) {
        let pinpointAnalyticsClient =
            AWSPinpoint(configuration:
                AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: nil)).analyticsClient
        
        let event = pinpointAnalyticsClient.createEvent(withEventType: eventType.rawValue)
        event.addAttribute(attributeValue, forKey: attributeKey.rawValue)
        event.addMetric(metric, forKey: eventType.rawValue)
        pinpointAnalyticsClient.record(event)
        pinpointAnalyticsClient.submitEvents()
    }
    
    func sendMonetizationEvent(productId: String, price: Double, quantity: Int, currency: String)
    {
        let pinpointClient = AWSPinpoint(configuration:
            AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: nil))
        
        let pinpointAnalyticsClient = pinpointClient.analyticsClient
        
        let event =
            pinpointAnalyticsClient.createVirtualMonetizationEvent(withProductId:
                productId, withItemPrice: price, withQuantity: quantity, withCurrency: currency)
        pinpointAnalyticsClient.record(event)
        pinpointAnalyticsClient.submitEvents()
    }
}
