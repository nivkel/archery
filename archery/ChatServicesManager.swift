//
//  ChatServicesManager.swift
//  archery
//
//  Created by Kelvin Lee on 11/30/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AWSMobileClient
import PromiseKit
import JWTDecode

class ChatServicesManager {
    static let shared = ChatServicesManager()
    
    func getUserPayload() -> Promise<[String: String]> {
        return Promise { seal in
            AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
                do {
                    guard let token = tokens?.idToken?.tokenString else {
                        return
                    }
                    let jwt = try decode(jwt: token)
                    guard let sub = jwt.body["sub"] as? String, let username = jwt.body["cognito:username"] as? String, let email = jwt.body["email"] as? String else {
                        
                        return
                    }
                    print("getUserId sub:\(sub) username:\(username) email:\(email)")
                    let values = ["sub": sub, "username": username, "email": email]
                    seal.fulfill(values)
                } catch {
                    print("Error tokens")
                    seal.reject(error)
                }
            }
        }
    }
    
    func getUserId() -> Promise<String> {
        return Promise { seal in
            AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
                do {
                    guard let token = tokens?.idToken?.tokenString else {
                        return
                    }
                    let jwt = try decode(jwt: token)
                    guard let sub = jwt.body["sub"] as? String else {

                        return
                    }
                    if let username = jwt.body["cognito:username"] as? String {
                        print("getUserId username:\(username)")
                    }
                    if let email = jwt.body["email"] as? String {
                        print("getUserId email:\(email)")
                    }
                    seal.fulfill(sub)
                } catch {
                    print("Error tokens")
                    seal.reject(error)
                }
            }
        }
    }
    
    func getUser() -> Promise<User> {
        return Promise { seal in
            let meQuery = MeQuery()
            AWSDataService.shared.appSyncClient?.fetch(query: meQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                let airyaError = NSError(domain: "com.airya", code: 0, userInfo: nil)
                guard let userData = result?.data?.me else {
                    seal.reject(airyaError)
                    return
                }
                guard let verified = userData.verified else {
                    seal.reject(airyaError)
                    return
                }
                let user = User(id: userData.id, cognitoId: userData.cognitoId, username: userData.username, email: userData.email, location: userData.location, verified: verified, phoneVerified: userData.phoneVerified, score: userData.score, facebookId: userData.facebookId)
                seal.resolve(user, error)
            })
        }
    }
    
    func getUserConversations() -> Promise<[UserConversation]> {
        return Promise { seal in
            let meQuery = MeQuery()
            AWSDataService.shared.appSyncClient?.fetch(query: meQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                //// Make sure there are no null fields or response will be nil!!!!!
                debugPrint("GetUserConversationForQuery result:\(result?.data?.me?.conversations?.userConversations)")
                let nextToken = result?.data?.me?.conversations?.nextToken
                guard let items = result?.data?.me?.conversations?.userConversations else {
                    return
                }
                let mappedItems = items.compactMap { $0 }
                let userConversations = mappedItems.map { item -> UserConversation in
                    let userConversation = UserConversation(userId: item.userId, postId: item.postId, conversationId: item.conversationId, subject: item.subject, userProfile: item.userProfile, username: item.username, postTitle: item.postTitle, postPrice: item.postPrice, postAuthor: item.postAuthor, senderName: item.senderName, postFileKey: item.postFileKey, createdAt: item.createdAt)
                    debugPrint("userConversation post title:\(item.postPrice)")
                    return userConversation
                }
                seal.resolve(error, userConversations)
            })
        }
    }
    
    func listConversations(userId: String, postId: String) -> Promise<[UserConversation?]> {
        return Promise { seal in
            let getUserConversationsQuery = GetUserConversationQuery(userId: userId, postId: postId)
            AWSDataService.shared.appSyncClient?.fetch(query: getUserConversationsQuery, cachePolicy: .returnCacheDataAndFetch, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("GetUserConversationForQuery result:\(result?.data?.getUserConversation?.userConversations)")
                
                let nextToken = result?.data?.getUserConversation?.nextToken
                guard let items = result?.data?.getUserConversation?.userConversations else {
                    return
                }
                let userConversations = items.map { item -> UserConversation? in
                    guard let item = item else {
                        return nil
                    }
                    let userConversation = UserConversation(userId: item.userId, postId: item.postId, conversationId: item.conversationId, subject: item.subject, userProfile: item.userProfile, username: item.username, postTitle: item.postTitle, postPrice: item.postPrice, postAuthor: item.postAuthor, senderName: item.senderName, postFileKey: item.postFileKey, createdAt: item.createdAt)
                    return userConversation
                }
                seal.resolve(error, userConversations)
            })
                
        }
    }
    
    
    func handleConversationCreation(post: Post, subject: String, senderName: String?) -> Promise<String> {
        let recipientUserId = "8358de8d-d996-4080-8448-aabfbe631c55"
        return Promise { seal in
            firstly {
                getUserId()
                }.then { userId in
                    self.getUserConversation(userId: userId, postId: post.id)
                }.get { results in
                    if results.0 {
                        debugPrint("getUserConversation exists:\(results.0), id:\(results.1)")
                        seal.fulfill(results.1)
                    } else {
                        debugPrint("getUserConversation doesn't exist, creating..:\(results.0)")
                        firstly {
                            self.createConversation(subject: subject, postId: post.id)
                        }.then { conversationId in
                            self.createUserConversation(post: post, conversationId: conversationId, userId: results.1, subject: subject, senderName: "")
                        }.then { createUserConversationResults in
                            self.createUserConversation(post: post, conversationId: createUserConversationResults.0, userId: createUserConversationResults.1.userId, subject: "sell", senderName: senderName)// Seller user
                        }.done { createUserConversationResults in
                            debugPrint("userConversations created id:\(createUserConversationResults.0)")
                            seal.fulfill(createUserConversationResults.0)
                        }.catch { error in
                            debugPrint("createUserConversation error:\(error)")
                            seal.reject(error)
                        }
                    }
                }.catch { error in
                    debugPrint("getUserConversation error:\(error)")
                    seal.reject(error)
            }
        }
    }
    
    func createConversation(subject: String, postId: String) -> Promise<String> {
        let dateFormatter = DateFormatter()
        let createdAt = dateFormatter.string(from: Date())
        debugPrint("convo created at:\(createdAt)")
        
        return Promise { seal in
            let addConversationMutation = AddConversationMutation(id: postId)
            AWSDataService.shared.appSyncClient?.perform(mutation: addConversationMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("newConversation update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("newConversation snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("newConversation result:\(result?.data?.addConversation?.id), error:\(error)")
                guard let conversationId = result?.data?.addConversation?.id else {
                    let error = NSError(domain: "addConversation", code: 0, userInfo: nil) as Error
                    seal.reject(error)
                    return
                }
                seal.resolve(error, conversationId)
            })
        }
    }
    
    func createUserConversation(post: Post, conversationId: String, userId: String, subject: String, senderName: String?) -> Promise<(String, Post)> {
        
        return Promise { seal in
            let addUserConversationMutation = AddUserConversationMutation(userId: userId, conversationId: conversationId, postId: post.id, subject: subject, senderName: senderName, postFileKey: post.file?.key)
            AWSDataService.shared.appSyncClient?.perform(mutation: addUserConversationMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("newUserConversation update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("newUserConversation snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("newUserConversation result:\(result?.data?.addUserConversation), error:\(error)")
                guard let conversationId = result?.data?.addUserConversation?.conversationId else {
                    return
                }
                seal.resolve((conversationId, post), error)
            })
        }
    }
    
    func getUserConversation(userId: String, postId: String) -> Promise<(Bool, String)> {
        return Promise { seal in
            let getUserConversationForQuery = GetUserConversationQuery(userId: userId, postId: postId)
            AWSDataService.shared.appSyncClient?.fetch(query: getUserConversationForQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("getUserConversationForQuery result:\(result?.data?.getUserConversation?.userConversations)")
                guard let userConversations = result?.data?.getUserConversation?.userConversations else {
                    return
                }
                if userConversations.isEmpty {
                    debugPrint("UserConversation doesn't exist. Creating one.. pass the userId")
                    seal.fulfill((false, userId))
                } else {
                    debugPrint("UserConversation exist.. pass the conversationId")
                    guard let userConversation = userConversations.first, let conversationId = userConversation?.conversationId else {
                        return
                    }
                    seal.fulfill((true, conversationId))
                }
            })
        }
    }
    
    func addMessage(message: ChatMessageModelProtocol, conversationId: String, text: String) -> Promise<String> {
        return Promise { seal in
            let addMessage = AddMessageMutation(id: message.uid, conversationId: conversationId, content: text, createdAt: Common.createdAtDate())
            AWSDataService.shared.appSyncClient?.perform(mutation: addMessage, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("addMessage update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("addMessage snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("addMessage result:\(result?.data?.addMessage?.id), error:\(error)")
                guard let id = result?.data?.addMessage?.id else {
                    return
                }
                seal.resolve(id, error)
            })
        }
    }
    
    func messagesForConversation(id: String, senderId: String) -> Promise<([Message], String)> {
        return Promise { seal in
            let getMessagesForConversationQuery = GetMessagesForConversationQuery(after: nil, conversationId: id, first: nil)
            AWSDataService.shared.appSyncClient?.fetch(query: getMessagesForConversationQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("getMessagesForConversationQuery result:\(result?.data?.getMessagesForConversation?.messages)")
                let nextToken = result?.data?.getMessagesForConversation?.nextToken
                guard let items = result?.data?.getMessagesForConversation?.messages else {
                    return
                }
                let mappedItems = items.compactMap { $0 }
                let messages = mappedItems.map { item -> Message in
                    let message = Message(id: item.id, content: item.content, conversationId: item.conversationId, createdAt: item.createdAt!, sender: item.sender!, senderName: item.senderName!, isSent: item.isSent!)
                    return message
                }
                seal.resolve((messages, senderId), error)
            })
        }
    }
}
