//
//  DiscoverViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/22/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import SwiftMessages

class DiscoverViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var posts = [Post]()
    var nextToken: String?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = Common.backgroundColor
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(DiscoverViewController.handleRefresh), for: .valueChanged)
        return refreshControl
    }()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Airya"
        
        let layout = ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
        self.collectionView.collectionViewLayout = layout
        self.collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let post1 = Post(emptyTitle: "Welcome to Market place")
//        post1.id = UUID().uuidString
//        let post2 = Post(emptyTitle: "New item is available!")
//        post2.id = UUID().uuidString
//        let post3 = Post(emptyTitle: "Influencer talks about item")
//        post3.id = UUID().uuidString
//        posts = [post1, post2, post3]
//
//        self.adapter.performUpdates(animated: true, completion: { (finished) in
//
//        })
//        self.refreshControl.endRefreshing()
        
        fetchDiscoverPosts()
    }
    
    func fetchDiscoverPosts() {
        PostServicesManager.shared.fetchDiscoveryPosts().done { posts in
            self.posts = posts
            }.ensure {
                self.adapter.performUpdates(animated: true, completion: { (finished) in
                    
                })
                self.refreshControl.endRefreshing()
            }.catch { error in
                
        }
    }
    
    func showDiscoverDetail(index: Int) {
        let discoverDetailViewController = DiscoverDetailViewController()
//        let navigationController = UINavigationController(rootViewController: discoverDetailViewController)
        let post = self.posts[index]
        discoverDetailViewController.post = post
        self.navigationController?.pushViewController(discoverDetailViewController, animated: true)
//        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
//        segue.configure(layout: .topCard)
//        segue.perform()
    }
    
    @objc func handleRefresh() {
        debugPrint("Refresh")
        
        fetchDiscoverPosts()
    }

}

extension DiscoverViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {
        debugPrint("willDisplay index:\(index)")
        
        if self.posts.count - 1 == index {
            if let nextToken = self.nextToken {
                debugPrint("willDisplay index:\(index) pagination token:")
            }
        }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension DiscoverViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.posts as! [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        default:
            return DiscoverSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let customView = UIView(frame: self.view.frame)
//        customView.loadingIndicator.startAnimating()
        
        return customView
    }
}
