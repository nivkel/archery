//
//  DiscoverSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 1/13/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class DiscoverSectionController: ListSectionController {

    var post: Post!
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let width = self.collectionContext?.containerSize.width else {
            return CGSize(width: 0, height: 100)
        }
        return CGSize(width: width, height: 200.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: DiscoverCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! DiscoverCell
        cell.titleLabel.text = post.title
        cell.imageView.setFAIconWithName(icon: .FAFileImageO, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 100, height: 100))
        
        return cell
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        self.post = object as? Post
    }
    
    override func didSelectItem(at index: Int) {
        guard let discoverViewController = self.viewController as? DiscoverViewController else {
            return
        }
        discoverViewController.showDiscoverDetail(index: index)
    }

}
