//
//  DiscoverCell.swift
//  archery
//
//  Created by Kelvin Lee on 1/23/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable

class DiscoverCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
