//
//  DiscoverDetailViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/9/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class DiscoverDetailViewController: UIViewController {
    
    var webView: WKWebView!
    
    var post: Post!
    
    let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Loading", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(DiscoverDetailViewController.close))
//        closeButton.tintColor = Common.primaryColor
//        self.navigationItem.rightBarButtonItem = closeButton
//
//        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 100)
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: self.view.frame, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        loadPage()
    }
    
    func loadPage() {
        guard let content = post.content else {
            return
        }
        if let url = URL(string: content) {
            webView.load(URLRequest(url: url))
        }
        webView.allowsBackForwardNavigationGestures = true

    }

    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
}

extension DiscoverDetailViewController: WKUIDelegate, WKNavigationDelegate {
    func webViewDidClose(_ webView: WKWebView) {
        debugPrint("WebView DidClose")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("WebView DidFail:\(error)")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        debugPrint("WebView DidStart")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("WebView DidFinish")
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            return
        }
        print("url:\(url.absoluteString) scheme:\(String(describing: url.scheme)), host:\(String(describing: url.host))")
        guard url.scheme != nil else {
            return
        }
        // check redirect url matches
        let redirect = ""
        if url.host == redirect {
            print("Url opened:\(url)")
            
        }
        decisionHandler(.allow)
    }
}
