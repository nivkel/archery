//
//  PostServicesManager.swift
//  archery
//
//  Created by Kelvin Lee on 12/27/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AWSMobileClient
import PromiseKit

class PostServicesManager {
    enum MappingTypes: String {
        case like = "like"
        case view = "view"
    }
    
    static let shared = PostServicesManager()
    
    func publishPostWithFiles(input: CreatePostWithFilesInput, images: [String]) -> Promise<Post> {
        return Promise { seal in
            AWSS3Manager.shared.getIdentityIdFromCredentialProvider { (credential, error) in
                guard let credential = credential else {
                    return
                }
                debugPrint("Credential :\(credential), error:\(error)")
                
                let fileInput1: S3ObjectInput? = nil
                let fileInput2: S3ObjectInput? = nil
                let fileInput3: S3ObjectInput? = nil
                let fileInput4: S3ObjectInput? = nil
                
                var fileInputs = [fileInput1, fileInput2, fileInput3, fileInput4]
                
                let bucket = "archerypostmedia"
                let region = "us-east-1"
                let mimeType = "image/jpeg"
                
                for (idx, obj) in images.enumerated() {
                    let filename = "\(UUID().uuidString)"
                    let encoded = "\(Data(filename.utf8).base64EncodedString())-00\(idx).jpg"
                    let userId = input.userId!!
                    let key = "private/\(credential)/\(userId)/\(encoded)"
                    debugPrint("filename:\(key), obj:\(obj), idx:\(idx)")
                    let input = S3ObjectInput(bucket: bucket, key: key, region: region, localUri: obj, mimeType: mimeType)
                    fileInputs[idx] = input
                }
                let postMutation = AddPostWithFilesMutation(input: input, file: fileInputs[0], file2: fileInputs[1], file3: fileInputs[2], file4: fileInputs[3])
                
                AWSDataService.shared.appSyncClient?.perform(mutation: postMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                    debugPrint("update:\(update)")
                }, conflictResolutionBlock: { (snapshot, mutation, result) in
                    debugPrint("snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
                }, resultHandler: { (result, error) in
                    debugPrint("result:\(result), error:\(error)")
                    
                    guard let item = result?.data?.addPostWithFiles else {
                        return
                    }
                    let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: nil, file2: nil, file3: nil, file4: nil, userId: item.userId, user: nil, typename: item.typename)
                    let group = DispatchGroup()
                    for file in fileInputs.dropFirst() {
                        if let file = file {
                            group.enter()
                            let object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region, localSourceFileUrl: file.localUri, mimeType: file.mimeType)
                            AWSDataService.shared.s3Manager.upload(s3Object: object, completion: { (complete, error) in
                                debugPrint("Secondary upload complete:\(complete) error:\(error)")
                                group.leave()
                            })
                        }
                    }
                    group.notify(queue: .main, execute: {
                        debugPrint("All uploads completed")
                        seal.resolve(post, error)
                    })
                })
            }
        }
        
    }
    
    func editPostAddFile(existingPost: Post, input: UpdateUserPostInput, image: Int, imageUri: String) -> Promise<S3ObjectInput> {
        return Promise { seal in
            AWSS3Manager.shared.getIdentityIdFromCredentialProvider { (credential, error) in
                guard let credential = credential else {
                    return
                }
                debugPrint("Credential :\(credential), error:\(error)")
                
                var fileInput1: S3ObjectInput? = nil
                var fileInput2: S3ObjectInput? = nil
                var fileInput3: S3ObjectInput? = nil
                var fileInput4: S3ObjectInput? = nil
                
                let bucket = "archerypostmedia"
                let region = "us-east-1"
                let mimeType = "image/jpeg"
                
                let filename = "\(UUID().uuidString.lowercased())"
                let encoded = "\(Data(filename.utf8).base64EncodedString())-00\(image).jpg"
                let userId = existingPost.userId
                let key = "private/\(credential)/\(userId)/\(encoded)"
                debugPrint("filename:\(key), obj:\(imageUri), idx:\(image)")
                
                let fileInput = S3ObjectInput(bucket: bucket, key: key, region: region, localUri: imageUri, mimeType: mimeType)
                
                switch image {
                case 0:
                    debugPrint("editPostAddFile fileInput1")
                    fileInput1 = fileInput
                    break
                case 1:
                    debugPrint("editPostAddFile fileInput2")
                    fileInput2 = fileInput
                    break
                case 2:
                    debugPrint("editPostAddFile fileInput3")
                    fileInput3 = fileInput
                    break
                case 3:
                    debugPrint("editPostAddFile fileInput4")
                    fileInput4 = fileInput
                    break
                default:
                    break
                }
                self.editPost(input: input, file: fileInput1, file2: fileInput2, file3: fileInput3, file4: fileInput4).done { updatedPost in
                    debugPrint("editPost add done")
                    seal.fulfill(fileInput)
                    }.catch { error in
                        debugPrint("editPost add error:\(error)")
                        seal.reject(error)
                }
            }
            
        }
    }
    
    func editPostRemoveFile(existingPost: Post, input: UpdateUserPostInput, image: Int) -> Promise<()> {
        return Promise { seal in
            var fileInput1: S3ObjectInput? = nil
            var fileInput2: S3ObjectInput? = nil
            var fileInput3: S3ObjectInput? = nil
            var fileInput4: S3ObjectInput? = nil
            
            let fileInput = S3ObjectInput(bucket: "_", key: "_", region: "_", localUri: "_", mimeType: "_")
            
            switch image {
            case 0:
                debugPrint("editPostRemoveFile fileInput1")
                fileInput1 = fileInput
                break
            case 1:
                debugPrint("editPostRemoveFile fileInput2")
                fileInput2 = fileInput
                break
            case 2:
                debugPrint("editPostRemoveFile fileInput3")
                fileInput3 = fileInput
                break
            case 3:
                debugPrint("editPostRemoveFile fileInput4")
                fileInput4 = fileInput
                break
            default:
                break
            }
            self.editPost(input: input, file: fileInput1, file2: fileInput2, file3: fileInput3, file4: fileInput4).done { updatedPost in
                debugPrint("editPost remove done")
                seal.fulfill(())
            }.catch { error in
                debugPrint("editPost remove error:\(error)")
                seal.reject(error)
            }
        }
    }
    
    func editPostWithFiles(existingPost: Post, input: UpdateUserPostInput, images: [String], imagesToRemove: [Int]) -> Promise<()> {
        return Promise { seal in
            let fileInput1: S3ObjectInput? = nil
            let fileInput2: S3ObjectInput? = nil
            let fileInput3: S3ObjectInput? = nil
            let fileInput4: S3ObjectInput? = nil
            
            var fileInputs = [fileInput1, fileInput2, fileInput3, fileInput4]
            
            let bucket = "archerypostmedia"
            let region = "us-east-1"
            let mimeType = "image/jpeg"
            
            AWSS3Manager.shared.getIdentityIdFromCredentialProvider { (credential, error) in
                guard let credential = credential else {
                    return
                }
                debugPrint("Credential :\(credential), error:\(error)")
                for (idx, obj) in images.enumerated() {
                    let filename = "\(UUID().uuidString.lowercased())"
                    let encoded = "\(Data(filename.utf8).base64EncodedString())-00\(idx).jpg"
                    let userId = existingPost.userId
                    let key = "private/\(credential)/\(userId)/\(encoded)"
                    debugPrint("filename:\(key), obj:\(obj), idx:\(idx)")
                    
                    var input: S3ObjectInput!
                    if imagesToRemove.contains(idx) {
                        input = S3ObjectInput(bucket: bucket, key: "", region: region, localUri: obj, mimeType: mimeType)
                    } else {
                        input = S3ObjectInput(bucket: bucket, key: key, region: region, localUri: obj, mimeType: mimeType)
                    }
                    fileInputs[idx] = input
                }
                for input in fileInputs {
                    debugPrint("edit post test inputs:\(input?.key)")
                }
                
                self.editPost(input: input, file: fileInputs[0], file2: fileInputs[1], file3: fileInputs[2], file4: fileInputs[3]).done { updatedPost in 
                    debugPrint("editPost done")
                    let group = DispatchGroup()
                    for file in fileInputs {
                        if let file = file {
                            group.enter()
                            let object = AWSS3Object(key: file.key, bucket: file.bucket, region: file.region, localSourceFileUrl: file.localUri, mimeType: file.mimeType)
                            AWSDataService.shared.s3Manager.upload(s3Object: object, completion: { (complete, error) in
                                debugPrint("Secondary upload complete:\(complete) error:\(error)")
                                group.leave()
                            })
                        }
                    }
                    group.notify(queue: .main, execute: {
                        debugPrint("All uploads completed")
                        seal.fulfill(())
                    })
                }.catch { error in
                    debugPrint("editPost error:\(error)")
                    seal.reject(error)
                }
            }
        }
    }
    
    func fetchPostsFilter(filter: ModelPostFilterInput, presenter: InteractorToPresenterPostFeedProtocol, limit: Int?, nextToken: String?) {
        let query = ListPostsQuery(filter: filter, limit: limit, nextToken: nextToken)
        AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
            guard let items = result?.data?.listPosts?.items else {
                presenter.postFeedFetchFailed()
                return
            }
            let mappedItems = items.compactMap { $0 }
            let posts = mappedItems.map({ item -> Post in
                let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: item.file, file2: item.file2, file3: item.file3, file4: item.file4, userId: item.userId, user: item.user, typename: item.typename)
                return post
            })
            debugPrint("fetchPosts items:\(result?.data?.listPosts?.items?.count), nextToken isEmpty:\(result?.data?.listPosts?.nextToken)")
            presenter.postFeedSearchSuccess(response: posts, nextToken: nextToken)
        })
    }
    
    func fetchPostsFilterSearchQuery(filters: [ModelPostFilterInput], presenter: InteractorToPresenterPostFeedProtocol, limit: Int?, nextToken: String?) {
        let orFilterInput = ModelPostFilterInput(or: filters)
        let query = ListPostsQuery(filter: orFilterInput, limit: limit, nextToken: nextToken)
        AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
            guard let items = result?.data?.listPosts?.items else {
                presenter.postFeedFetchFailed()
                return
            }
            let mappedItems = items.compactMap { $0 }
            let posts = mappedItems.map({ item -> Post in
                let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: item.file, file2: item.file2, file3: item.file3, file4: item.file4, userId: item.userId, user: item.user, typename: item.typename)
                return post
            })
            debugPrint("fetchPosts items:\(result?.data?.listPosts?.items?.count), nextToken isEmpty:\(result?.data?.listPosts?.nextToken)")
            presenter.postFeedSearchSuccess(response: posts, nextToken: nextToken)
        })
    }
    
    func fetchPostsFilterUserId(presenter: InteractorToPresenterPostFeedProtocol, userId: String, limit: Int?, nextToken: String?) {
        let filterUserId = ModelIDFilterInput(eq: userId)
        let postFilterInput = ModelPostFilterInput(userId: filterUserId)
        let query = ListPostsQuery(filter: postFilterInput, limit: limit, nextToken: nextToken)
        AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
            guard let items = result?.data?.listPosts?.items else {
                presenter.postFeedFetchFailed()
                return
            }
            let mappedItems = items.compactMap { $0 }
            let posts = mappedItems.map({ item -> Post in
                let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: item.file, file2: item.file2, file3: item.file3, file4: item.file4, userId: item.userId, user: item.user, typename: item.typename)
                return post
            })
            debugPrint("fetchPosts items:\(result?.data?.listPosts?.items?.count)")
            presenter.postFeedFetchSuccess(response: posts, nextToken: nextToken)
        })
    }
    
    func fetchPostsFilterIsSold(user: User, isSold: Bool, limit: Int?, nextToken: String?) -> Promise<[Post]> {
        return Promise { seal in
            let userIdFilter = ModelIDFilterInput(eq: user.id)
            let isSoldFilter = ModelBooleanFilterInput(eq: isSold)
            let postFilterInput = ModelPostFilterInput(isSold: isSoldFilter, userId: userIdFilter)
            let query = ListPostsQuery(filter: postFilterInput, limit: limit, nextToken: nextToken)
            AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                guard let items = result?.data?.listPosts?.items else {
                    return
                }
                let mappedItems = items.compactMap { $0 }
                let posts = mappedItems.map({ item -> Post in
                    let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: item.file, file2: item.file2, file3: item.file3, file4: item.file4, userId: item.userId, user: item.user, typename: item.typename)
                    return post
                })
                debugPrint("fetchPostsIsSold:\(posts)")
                seal.resolve(posts, error)
            })
        }
    }
    
    func editPost(input: UpdateUserPostInput, file: S3ObjectInput?, file2: S3ObjectInput?, file3: S3ObjectInput?, file4: S3ObjectInput?) -> Promise<Post?> {
        return Promise { seal in
            let updateUserPostMutation = UpdateUserPostMutation(input: input, file: file, file2: file2, file3: file3, file4: file4)
            AWSDataService.shared.appSyncClient?.perform(mutation: updateUserPostMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("editPost update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("addLikeForPost snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("editPost result:\(result?.data?.updateUserPost), error:\(error)")
                guard let item = result?.data?.updateUserPost else {
                    seal.resolve(nil, error)
                    return
                }
                let updatedPost = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: nil, file2: nil, file3: nil, file4: nil, userId: item.userId, user: nil, typename: item.typename)
                seal.resolve(updatedPost, error)
            })
        }
    }
    
    func getLikeForUser(userId: String, postId: String) -> Promise<(Bool, String, String?)> {
        return Promise { seal in
            let getLikeTypeQuery = GetLikeTypeQuery(userId: userId, postId: postId, type: MappingTypes.like.rawValue)
            AWSDataService.shared.appSyncClient?.fetch(query: getLikeTypeQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("likeTypeData:\(result?.data)")
                guard let likeTypeData = result?.data?.getLikeType else {
                    seal.fulfill((false, userId, nil))
                    return
                }
                guard let mappings = likeTypeData.mappings else {
                    seal.fulfill((false, userId, nil))
                    return
                }
                guard !mappings.isEmpty else {
                    seal.fulfill((false, userId, nil))
                    return
                }
                guard let likeObject = mappings.first, let likeId = likeObject?.id else {
                    return
                }
                seal.fulfill((true, userId, likeId))
            })
        }
    }
    
    func addUserLikeForPost(userId: String, postId: String) -> Promise<()> {
        return Promise { seal in
            let addLikeTypeMutation = AddLikeTypeMutation(userId: userId, postId: postId, type: MappingTypes.like.rawValue)
            AWSDataService.shared.appSyncClient?.perform(mutation: addLikeTypeMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("addLikeForPost update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("addLikeForPost snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("addLikeForPost result:\(result), error:\(error)")
                seal.resolve((), error)
            })
        }
    }
    
    func removeUserLikeForPost(id: String) -> Promise<()> {
        return Promise { seal in
            let removeLikeTypeMutation = RemoveLikeTypeMutation(id: id)
            AWSDataService.shared.appSyncClient?.perform(mutation: removeLikeTypeMutation, queue: DispatchQueue.main, optimisticUpdate: { (update) in
                debugPrint("removeLikeTypeMutation update:\(update)")
            }, conflictResolutionBlock: { (snapshot, mutation, result) in
                debugPrint("removeLikeTypeMutation snapshot:\(snapshot), mutation:\(mutation), result:\(result)")
            }, resultHandler: { (result, error) in
                debugPrint("removeLikeTypeMutation result:\(result), error:\(error)")
                seal.resolve((), error)
            })
        }
    }
    
    func getUserPostLikes(userId: String) -> Promise<[PostLike]> {
        return Promise { seal in
            let getLikesForUserQuery = GetLikesForUserQuery(userId: userId, type: MappingTypes.like.rawValue)
            AWSDataService.shared.appSyncClient?.fetch(query: getLikesForUserQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                guard let userLikesData = result?.data?.getLikesForUser?.mappings else {
                    return
                }
                let mappedItems = userLikesData.compactMap { $0 }
                let postLikes = mappedItems.map({ item -> PostLike in
                    let postLike = PostLike(id: item.id, userId: item.userId, postId: item.postId!, type: item.type, post: item.post!)
                    
                    return postLike
                })
                seal.fulfill(postLikes)
            })
        }
    }
    
    func getPostLikes(postId: String) -> Promise<Int> {
        return Promise { seal in
            let getLikesForPostQuery = GetLikesForPostQuery(postId: postId, type: MappingTypes.like.rawValue)
            AWSDataService.shared.appSyncClient?.fetch(query: getLikesForPostQuery, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                debugPrint("postLikesData:\(result?.data)")
                guard let postLikesData = result?.data?.getLikesForPost else {
                    seal.fulfill(0)
                    return
                }
                debugPrint("postLikesData:\(postLikesData)")
                guard let mappings = postLikesData.mappings else {
                    seal.fulfill(0)
                    return
                }
                seal.fulfill(mappings.count)
            })
        }
    }
    
    func fetchDiscoveryPosts() -> Promise<[Post]> {
        return Promise { seal in
            let stringFilterInput = ModelStringFilterInput(eq: "Discover")
            let postFilterInput = ModelPostFilterInput(typename: stringFilterInput)
            let query = ListPostsQuery(filter: postFilterInput, limit: 20, nextToken: nil)
            AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                guard let items = result?.data?.listPosts?.items else {
                    return
                }
                let mappedItems = items.compactMap { $0 }
                let posts = mappedItems.map({ item -> Post in
                    let post = Post(id: item.id, title: item.title!, content: item.content!, typename: item.typename!)
                    return post
                })
                debugPrint("fetchDiscoveryPosts:\(posts)")
                seal.resolve(posts, error)
            })
        }
    }
    
    func getPost(id: String) -> Promise<Post> {
        return Promise { seal in
            let query = GetPostQuery(id: id)
            AWSDataService.shared.appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData, queue: DispatchQueue.main, resultHandler: { (result, error) in
                guard let item = result?.data?.getPost else {
                    return
                }
                let post = Post(id: item.id, title: item.title, price: item.price, salePrice: item.salePrice, location: item.location, category: item.category, condition: item.condition, content: item.content, tags: item.tags, isSold: item.isSold, isPublished: item.isPublished, createdAt: item.createdAt, updatedAt: item.updatedAt, promotedAt: item.promotedAt, promotedCount: item.promotedCount, dateSold: item.dateSold, isNegotiable: item.isNegotiable, willShip: item.willShip, willMeet: item.willMeet, file: nil, file2: nil, file3: nil, file4: nil, userId: item.userId, user: nil, typename: item.typename)
                debugPrint("fetchPost post:\(post)")
                seal.resolve(post, error)
            })
        }
    }
}
