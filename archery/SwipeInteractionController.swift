//
//  SwipeInteractionController.swift
//  archery
//
//  Created by Kelvin Lee on 10/25/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

protocol PanelAnimationControllerDelegate {
    func shouldHandlePanelInteractionGesture() -> Bool
}

class SwipeInteractionController: UIPercentDrivenInteractiveTransition {
    var interactionInProgress = false
    
    private var shouldCompleteTransition = false
    private weak var viewController: UIViewController!// (UIViewController & PanelAnimationControllerDelegate)?
    
    init(viewController: UIViewController) {
        super.init()
        self.viewController = viewController// as? (UIViewController & PanelAnimationControllerDelegate)
        prepareGestureRecognizer(in: viewController.view)
    }
    
    private func prepareGestureRecognizer(in view: UIView) {
        let gesture = UIScreenEdgePanGestureRecognizer(target: self,
                                                       action: #selector(handleGesture(_:)))
        gesture.edges = .left
        view.addGestureRecognizer(gesture)
    }
    
    @objc func handleGesture(_ gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        // 1
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view)
        var progress = (translation.x / 200)
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        let percentThreshold:CGFloat = 0.3
//        let verticalMovement = translation.y / gestureRecognizer.view!.bounds.height
//        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
//        let downwardMovementPercent = fminf(downwardMovement, 1.0)
//        let progress = CGFloat(downwardMovementPercent)
        
        switch gestureRecognizer.state {
            
        // 2
        case .began:
            interactionInProgress = true
            viewController?.dismiss(animated: true, completion: nil)
            
        // 3
        case .changed:
//            debugPrint("changed progress: \(progress), gesture:")
//            if !(viewController?.shouldHandlePanelInteractionGesture())! && progress == 0 {
//                return;
//            }
            shouldCompleteTransition = progress > percentThreshold
            update(progress)
            
        // 4
        case .cancelled:
            interactionInProgress = false
            cancel()
                
        // 5
        case .ended:
            interactionInProgress = false
            if shouldCompleteTransition {
                finish()
            } else {
                cancel()
            }
        default:
            break
        }
    }
}
