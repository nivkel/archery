//
//  AWSS3Manager.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import AWSS3
import AWSAppSync
import Kingfisher

class AWSS3Object: AWSS3ObjectProtocol, AWSS3InputObjectProtocol {
    var key: String!
    var bucket: String!
    var region: String!
    var localSourceFileUrl: String?
    var mimeType: String?
    
    static let imageHandlerUrl = "https://d15q3r0eoql0jw.cloudfront.net/fit-in/"
    
    init(key: String, bucket: String, region: String, localSourceFileUrl: String? = nil, mimeType: String? = nil) {
        self.key = key
        self.bucket = bucket
        self.region = region
        self.localSourceFileUrl = localSourceFileUrl
        self.mimeType = mimeType
    }
    
    func getBucketName() -> String {
        return self.bucket
    }
    
    func getKeyName() -> String {
        return self.key
    }
    
    func getRegion() -> String {
        return self.region
    }
    
    func getLocalSourceFileURL() -> URL? {
        guard let localSourceFileUrl = self.localSourceFileUrl else {
            return nil
        }
        return URL(string: localSourceFileUrl)
    }
    
    func getMimeType() -> String {
        guard let mimeType = self.mimeType else {
            return ""
        }
        return mimeType
    }
}

class AWSS3Manager: AWSS3ObjectManager, AWSS3ObjectPresignedURLGenerator {
    
    static let shared = AWSS3Manager()
    
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    
    var downloadCompletionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    
    func upload(s3Object: AWSS3InputObjectProtocol & AWSS3ObjectProtocol, completion: @escaping ((Bool, Error?) -> Void)) {
        debugPrint("upload obj:\(s3Object.getLocalSourceFileURL())")
        self.uploadData(s3Object: s3Object, completion: completion)
        
    }
    
    func download(s3Object: AWSS3ObjectProtocol, toURL: URL, completion: @escaping ((Bool, Error?) -> Void)) {
        debugPrint("download")
        self.downloadData(s3Object: s3Object, toURL: toURL, completion: completion)
    }
    
    func uploadData(completion: @escaping ((Bool, Error?) -> Void)) {
        
        do {
            let imageData = "Test data 1111".data(using: .utf32)!
            
            debugPrint("Image data:\(imageData)")
            //Create an expression object for progress tracking, to pass in request headers etc.
            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = {(task, progress) in
                // Do something e.g. Update a progress bar.
            }
            
            //Create a completion handler to be called when the transfer completes
            var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
            completionHandler = { (task, error) -> Void in
                // Do something e.g. Alert a user that the transfer has completed.
                // On failed uploads, `error` contains the error object.
                debugPrint("Upload transfer complete:\(task.progress), error:\(error)")
                guard let error = error else {
                    completion(true, nil)
                    return
                }
                completion(true, error)
            }
            //Instantiate the transferUtility object. This will pick up the bucketName, region,
            //and auth configuration from the awsconfiguration.json file
            
            
//            let transferUtility = AWSS3TransferUtility.default()
//            debugPrint("transfer utility confi:\(transferUtility.configuration.credentialsProvider)")
            
            let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6")
            let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
            //Setup the transfer utility configuration
            let tuConf = AWSS3TransferUtilityConfiguration()
            
            //Register a transfer utility object
            AWSS3TransferUtility.register(
                with: configuration!,
                transferUtilityConfiguration: tuConf,
                forKey: "transfer-utility-with-advanced-options"
            )
            
            //Look up the transfer utility object from the registry to use for your transfers.
            let transferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: "transfer-utility-with-advanced-options")
            
            //Upload the data. Pass in the expression to get progress updates and completion handler to get notified
            //when the transfer is completed.
            let bucket = "archerypostmedia"
            let key = "uploads/testpost15.jpg"
            let mimeType = "image/jpeg"
            
            transferUtility.uploadData(imageData, bucket: bucket, key: key, contentType: mimeType, expression: expression, completionHandler: completionHandler)
        } catch let error {
            debugPrint("Image data upload error:\(error)")
            completion(true, error)
        }
    }
    
    private func uploadData(s3Object: AWSS3InputObjectProtocol & AWSS3ObjectProtocol, completion: @escaping ((Bool, Error?) -> Void)) {
        guard let fileUrl = s3Object.getLocalSourceFileURL() else {
            completion(false, nil)
            return
        }
        do {
            let imageData = try Data(contentsOf: fileUrl)
            debugPrint("Image data:\(imageData)")
            //Create an expression object for progress tracking, to pass in request headers etc.
            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = {(task, progress) in
                // Do something e.g. Update a progress bar.
                debugPrint("upload progress:\(progress)")
            }
            
            //Create a completion handler to be called when the transfer completes
//            var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
            uploadCompletionHandler = { (task, error) -> Void in
                // Do something e.g. Alert a user that the transfer has completed.
                // On failed uploads, `error` contains the error object.
                debugPrint("Upload transfer complete:\(task.progress), error:\(error)")
                guard let error = error else {
                    completion(true, nil)
                    return
                }
                completion(true, error)
            }
            //Instantiate the transferUtility object. This will pick up the bucketName, region,
            //and auth configuration from the awsconfiguration.json file
//            let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6")
//            let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
//            AWSServiceManager.default().defaultServiceConfiguration = configuration
            let transferUtility = AWSS3TransferUtility.default()
            
            // AWSS3TransferUtility
//            let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6")
//            let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
//            //Setup the transfer utility configuration
//            let transferUtilityConfiguration = AWSS3TransferUtilityConfiguration()
//            transferUtilityConfiguration.timeoutIntervalForResource = 600
//            transferUtilityConfiguration.retryLimit = 3
//            //Register a transfer utility object
//            AWSS3TransferUtility.register(
//                with: configuration!,
//                transferUtilityConfiguration: transferUtilityConfiguration,
//                forKey: "transfer-utility-with-advanced-options"
//            )
//            //Look up the transfer utility object from the registry to use for your transfers.
//            let transferUtility = AWSS3TransferUtility.s3TransferUtility(forKey: "transfer-utility-with-advanced-options")
            
            //Upload the data. Pass in the expression to get progress updates and completion handler to get notified
            //when the transfer is completed.
            transferUtility.uploadData(imageData, bucket: s3Object.getBucketName(), key: s3Object.getKeyName(), contentType: s3Object.getMimeType(), expression: expression, completionHandler: uploadCompletionHandler)
        } catch let error {
            debugPrint("Image data upload error:\(error)")
            completion(true, error)
        }
    }
    
    private func downloadData(s3Object: AWSS3ObjectProtocol, toURL: URL, completion: @escaping ((Bool, Error?) -> Void)) {
        //Create an expression object for progress tracking, to pass in request headers etc.
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = {(task, progress) in
            // Do something e.g. Update a progress bar.
            debugPrint("download progress:\(progress)")
        }
        
        //Create a completion handler to be called when the transfer completes
//        var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
//        downloadCompletionHandler = { (task, URL, data, error) -> Void in
//            // Do something e.g. Alert a user for transfer completion.
//            // On failed downloads, `error` contains the error object.
//            debugPrint("Download transfer complete:\(task.progress), error:\(error)")
//            completion(true, nil)
//        }
        //Instantiate the transferUtility object. This will pickup the bucketName, region, and auth configuration
        //from the awsconfiguration.json file
        let transferUtility = AWSS3TransferUtility.default()
        //Download the data. Pass in the expression to get progress updates and completion handler to get notified
        //when the transfer is completed.
        let task = transferUtility.downloadData(
            forKey: s3Object.getKeyName(),
            expression: expression,
            completionHandler: downloadCompletionHandler
        )
    }
    
    func getFileUrl(credential: String, s3Object: AWSS3ObjectProtocol) {
        // https://s3.amazonaws.com/archerypostmedia/private/us-east-1%3A4f2fd870-1801-4603-927d-2cfd2450304b/testpost35.jpg
        
        
    }
    
    func getPresignedURL(s3Object: AWSS3ObjectProtocol) -> URL? {
        let getPreSignedURLRequest = AWSS3GetPreSignedURLRequest()
        getPreSignedURLRequest.bucket = s3Object.getBucketName()
        getPreSignedURLRequest.key = s3Object.getKeyName()
        getPreSignedURLRequest.httpMethod = .GET
        getPreSignedURLRequest.expires = Date(timeIntervalSinceNow: 3600)
        
        let task = AWSS3PreSignedURLBuilder.default().getPreSignedURL(getPreSignedURLRequest).continueWith { (task) -> Any? in
            if let error = task.error {
                print("Error: \(error)")
                return nil
            }
            let presignedURL = task.result
            print("Download presignedURL is: \(presignedURL)")
//            let request = URLRequest(url: presignedURL as! URL)
//            let downloadTask: URLSessionDownloadTask = URLSession.shared.downloadTask(with: request)
//            downloadTask.resume()
            return nil
        }
        guard let url = task.result as? URL else {
            return nil
        }
        debugPrint("presigned download url:\(url)")
        return url
    }
    
    func getIdentityIdFromCredentialProvider(completion: @escaping ((String?, Error?) -> Void)) {
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "us-east-1:b3b51a8f-3ff7-4361-ad1e-70bec7aad7d6")
        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
         (AWSServiceManager.default().defaultServiceConfiguration.credentialsProvider as! AWSCognitoCredentialsProvider).getIdentityId().continueWith { (task) -> Any? in
            
            guard let credentials = task.result as String? else {
                completion(nil, nil)
                return nil
            }
            debugPrint("AWSServiceManager creds: \(credentials)")
            completion(credentials, nil)
            return nil
        }
    }
    
    func downloadImage(imageView: UIImageView, key: String, completion: @escaping (UIImage?) -> Void) {
        let width = Int(imageView.frame.size.width) * 2
        let height = Int(imageView.frame.size.height) * 2
        debugPrint("imageView size height:\(height), width:\(width)")
        let fitInResolution = "\(width)x\(height)/"
        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
        debugPrint("s3 urlString:\(urlString)")
        guard let url = URL(string: urlString) else {
            return
        }
        debugPrint("s3 url:\(url)")
        let imageResource = ImageResource(downloadURL: url)
        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 30.0, height: 30.0), orientation: .up, textColor: UIColor.black, backgroundColor: UIColor.white)
//        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
            debugPrint("Progress received:\(received), total:\(total)")
        }) { (result) in
            switch result {
            case .success(let value):
                // The image was set to image view:
                print(value.image)
                imageView.image = value.image
                // From where the image was retrieved:
                // - .none - Just downloaded.
                // - .memory - Got from memory cache.
                // - .disk - Got from disk cache.
                print(value.cacheType)
                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                // The source object which contains information like `url`.
                print("download image source:\(value.source.cacheKey)")
                completion(value.image)
            case .failure(let error):
                print("error \(error)") // The error happens
                completion(nil)
            }
        }
    }
}
