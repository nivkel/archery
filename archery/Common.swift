//
//  Common.swift
//  archery
//
//  Created by Kelvin Lee on 10/11/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSFacebookSignIn
import SwiftMessages
import SwiftDate

extension UIColor {
    public static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIView {
    func takeScreenshot() -> UIImage {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}

extension UIImage {
    func image(alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

struct Common {
    static let primaryColor = UIColor.hexStringToUIColor(hex: "#6666CC")
    static let secondaryColor = UIColor.black
    static let tertiaryColor = UIColor.white
    static let backgroundColor = UIColor.clear
    
    static func boolToString(value: Bool) -> String {
        if value == true {
            return "Yes"
        } else {
            return "No"
        }
    }
    
    static func handlePriceFormat(price: Float) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
        formatter.maximumFractionDigits = 2
        let number = NSNumber(value: price)
        guard let rounded = formatter.string(from: number) else {
            return nil
        }
        return rounded
    }
    
    static func usernameInitials(value: String) -> String {
        var username = value
        username.insert(" ", at: .init(encodedOffset: 1))
        
        return username
    }
    
    static func createdAtDate() -> String {
        let dateFormatter = ISO8601DateFormatter()
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        debugPrint("createdAtDate:\(dateString)")
        return dateString
    }
    
    static func createdAtDate(value: String) -> Date {
        let dateFormatter = ISO8601DateFormatter()
        let date = dateFormatter.date(from: value)
        guard let dateValue = date else {
            debugPrint("Error with date format")
            return Date()
        }
        return dateValue
    }
    
    static func createdAtRelativeDate(value: String) -> String {
        let date = value.toDate()
        guard let relativeDate = date?.toRelative(since: DateInRegion(Date(), region: .current), style: RelativeFormatter.twitterStyle(), locale: Locales.english) else {
            return ""
        }
        debugPrint("createdAtRelativeDate:\(relativeDate)")
        return relativeDate
    }
    
    static func setTotalTabBarBadge(application: UIApplication) {
        let buyValue = UserDefaults.standard.integer(forKey: "buySubject")
        let sellValue = UserDefaults.standard.integer(forKey: "sellSubject")
        let totalValue = buyValue + sellValue
        if totalValue == 0 {
            Common.setTabBarBadgeCount(tab: 3, value: nil)
        } else {
            Common.setTabBarBadgeCount(tab: 3, value: "\(totalValue)")
        }
        application.applicationIconBadgeNumber = totalValue
    }
    
    static func setTabBarBadgeCount(tab: Int, value: String?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        if let mainTabBarController = rootViewController.viewControllers.first as? UITabBarController {
            if let tabItems = mainTabBarController.tabBar.items {
                let tabItem = tabItems[tab]
                tabItem.badgeValue = value
            }
        }
    }
    
    static func showTab(index: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        let mainTabBarController = rootViewController.viewControllers.first as! UITabBarController
        mainTabBarController.selectedIndex = index
    }
    
    static func showChatViewControllerTab() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        let mainTabBarController = rootViewController.viewControllers.first as! UITabBarController
        mainTabBarController.selectedIndex = 3
    }
    
    static func showChatroomView(navigationController: UINavigationController, conversationId: String, type: Int) {
        let chatViewController = ChatRouter.createModule()
        let dataSource = ChatDataSource(count: 0, pageSize: 50, conversationId: conversationId)
        chatViewController.dataSource = dataSource
        chatViewController.conversationId = conversationId
        
        if let tabBarController = navigationController.viewControllers.first as? UITabBarController {
            if let conversationNavigationController = tabBarController.viewControllers?[3] as? UINavigationController {
                debugPrint("showChatroomView nav root:\(conversationNavigationController.viewControllers)")
                if let conversationPageViewController = conversationNavigationController.viewControllers.first as? ConversationsPageViewController {
                    debugPrint("conversationPageViewController pages:\(conversationPageViewController.pages)")
                    if !conversationPageViewController.pages.isEmpty {
                        conversationPageViewController.setViewControllers([conversationPageViewController.pages[type]], direction: .forward, animated: true) { (complete) in
                            conversationNavigationController.pushViewController(chatViewController, animated: true)
                        }
                    } else {
                        conversationNavigationController.pushViewController(chatViewController, animated: true)
                    }
                    
                }
                
            }
        }
    }
    
    static func showSignInViewController() -> SignInViewController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        let signInViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        rootViewController.setViewControllers([signInViewController], animated: true)
        return signInViewController
    }
    
    static func showMain(mainNavigationController: UINavigationController) {
        let mainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
        let postFeedViewController = PostFeedRouter.createModule()
        let postFeedNavigationController = UINavigationController(rootViewController: postFeedViewController)
        postFeedNavigationController.tabBarItem.title = "Feed"
        let postPageViewController = PostRouter.createModule()
        
        let discoverViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DiscoverViewController") as! DiscoverViewController
        let discoverNavigationController = UINavigationController(rootViewController: discoverViewController)
        discoverNavigationController.tabBarItem.title = "Discover"
        
        let conversationsPageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConversationsPageViewController") as! ConversationsPageViewController
        let conversationsNavigationController = UINavigationController(rootViewController: conversationsPageViewController)
        conversationsNavigationController.tabBarItem.title = "Messages"
        
        let profileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        let profileNavigationController = UINavigationController(rootViewController: profileViewController)
        profileNavigationController.tabBarItem.title = "You"
        mainTabBarController.viewControllers = [postFeedNavigationController, discoverNavigationController, postPageViewController, conversationsNavigationController, profileNavigationController]
        mainNavigationController.setViewControllers([mainTabBarController], animated: true)
    }
    
    static func showOnboarding(mainNavigationController: UINavigationController?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        
        let onboardingViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
        
        rootViewController.setViewControllers([onboardingViewController], animated: true)
    }
    
    static func showOnboard() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
        
        let onboardViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnboardViewController") as! OnboardViewController
        
        rootViewController.setViewControllers([onboardViewController], animated: true)
    }
    
//    static func showMainViewControllers() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
//        let mainTabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
//        let postFeedViewController = PostFeedRouter.createModule()
//        let postPageViewController = PostRouter.createModule()
//        let profileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
//        mainTabBarController.viewControllers = [postFeedViewController, postPageViewController, profileViewController]
//        rootViewController.setViewControllers([mainTabBarController], animated: true)
//    }
    
    static func showLogin(navigationController: UINavigationController) {
        debugPrint("LoggedIn: \(AWSSignInManager.sharedInstance().isLoggedIn)")
        let config = AWSAuthUIConfiguration()
        config.enableUserPoolsUI = true
        config.addSignInButtonView(class: AWSFacebookSignInButton.self)
        config.backgroundColor = UIColor.black
        config.font = UIFont (name: "Helvetica Neue", size: 18)
        config.isBackgroundColorFullScreen = true
        config.canCancel = true
        
        AWSAuthUIViewController.presentViewController(with: navigationController, configuration: config) { (provider, error) in
            debugPrint("Provider :\(provider.token().result)")
            if error != nil {
                print("Error occurred: \(String(describing: error))")
                
            } else {
                // Sign in successful.
                debugPrint("Sign in success")
            }
        }
    }
    
    func showMenu(_ sender: Any, delegate: UIViewController, type: MenuType, layout: SwiftMessagesSegue.Layout, post: Post? = nil, image: UIImage? = nil) {
        let menuViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
            withIdentifier: "MenuViewController") as! MenuViewController
        let navigationController = UINavigationController(rootViewController: menuViewController)
        menuViewController.type = type
        if let post = post {
            menuViewController.post = post
        }
        menuViewController.image = image
        let segue = SwiftMessagesSegue(identifier: nil, source: delegate, destination: navigationController)
        segue.configure(layout: layout)
        segue.perform()
    }
    
    func showMenu(_ sender: Any, delegate: UIViewController, type: MenuType) {
        let menuViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
            withIdentifier: "MenuViewController") as! MenuViewController
        menuViewController.type = type
        // Use the popover presentation style for your view controller.
        menuViewController.modalPresentationStyle = .popover
        menuViewController.preferredContentSize = CGSize(width: 200.0, height: 220.0)
        // Specify the anchor point for the popover.
        menuViewController.popoverPresentationController?.barButtonItem = sender as? UIBarButtonItem
        if let presentationController = menuViewController.presentationController {
            presentationController.delegate = delegate as? UIAdaptivePresentationControllerDelegate
        }
//        if let popover = menuViewController.popoverPresentationController {
//            popover.sourceView = (sender as! UIView)
//            popover.sourceRect = (sender as! UIView).bounds
//        }
        // Present the view controller (in a popover).
        delegate.present(menuViewController, animated: true) {
            // The popover is visible.
        }
    }
    
    static func showStatusMessage(title: String, message: String, theme: Theme, style: SwiftMessages.PresentationStyle) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.error)
        view.configureDropShadow()
        view.configureContent(title: title, body: message)
        view.button?.setTitle("OK", for: .normal)
        view.buttonTapHandler = { _ in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = .seconds(seconds: 6.0)
        config.presentationStyle = .bottom
        config.dimMode = .gray(interactive: true)
        config.eventListeners.append() { event in
            if case .didHide = event { print("didHide") }
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    static func showNoticeMessage(title: String, message: String, theme: Theme, style: SwiftMessages.PresentationStyle) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(theme)
        view.configureDropShadow()
        view.configureContent(title: title, body: message)
        view.button?.setTitle("OK", for: .normal)
        view.buttonTapHandler = { _ in
            SwiftMessages.hide()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = .forever
        config.presentationStyle = style
        config.dimMode = .gray(interactive: true)
        config.eventListeners.append() { event in
            if case .didHide = event { print("didHide") }
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    static func showConfirmationMessage(title: String, message: String, theme: Theme, style: SwiftMessages.PresentationStyle, completion:@escaping (() -> Void)) {
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(theme)
        view.configureDropShadow()
        view.configureContent(title: title, body: message)
        view.button?.setTitle("Yes", for: .normal)
        view.buttonTapHandler = { _ in
            SwiftMessages.hide()
            completion()
        }
        var config = SwiftMessages.defaultConfig
        config.duration = .forever
        config.presentationStyle = style
        config.dimMode = .gray(interactive: true)
        config.eventListeners.append() { event in
            if case .didHide = event { print("didHide") }
        }
        SwiftMessages.show(config: config, view: view)
    }
    
    static func setTabBarIcons(navigationController: UINavigationController) {
        if let tabBarController = navigationController.viewControllers.first as? UITabBarController {
            if let tabBarItems = tabBarController.tabBar.items {
                for (index, item) in tabBarItems.enumerated() {
                    switch index {
                    case 0:
                        let image = UIImage(named: "feed")
                        item.image = image
                        item.selectedImage = image
//                        item.setFAIcon(icon: .FAColumns, size: CGSize(width: 30.0, height: 30.0), orientation: UIImage.Orientation.down, textColor: Common.tertiaryColor, backgroundColor: Common.backgroundColor, selectedTextColor: Common.primaryColor, selectedBackgroundColor: Common.backgroundColor)
                        break
                    case 1:
                        item.setFAIcon(icon: .FANewspaperO, size: CGSize(width: 30.0, height: 30.0), orientation: UIImage.Orientation.down, textColor: Common.tertiaryColor, backgroundColor: Common.backgroundColor, selectedTextColor: Common.primaryColor, selectedBackgroundColor: Common.backgroundColor)
                        break
                    case 2:
                        item.setFAIcon(icon: .FADollar, size: CGSize(width: 30.0, height: 30.0), orientation: UIImage.Orientation.down, textColor: Common.tertiaryColor, backgroundColor: Common.backgroundColor, selectedTextColor: Common.primaryColor, selectedBackgroundColor: Common.backgroundColor)
                        break
                    case 3:
                        item.setFAIcon(icon: .FAComment, size: CGSize(width: 30.0, height: 30.0), orientation: UIImage.Orientation.down, textColor: Common.tertiaryColor, backgroundColor: Common.backgroundColor, selectedTextColor: Common.primaryColor, selectedBackgroundColor: Common.backgroundColor)
                        break
                    default:
                        item.setFAIcon(icon: .FAUser, size: CGSize(width: 30.0, height: 30.0), orientation: UIImage.Orientation.down, textColor: Common.tertiaryColor, backgroundColor: Common.backgroundColor, selectedTextColor: Common.primaryColor, selectedBackgroundColor: Common.backgroundColor)
                        break
                    }
                }
            }
        }
    }
    
    static func setAppColors() {
        UITabBar.appearance().barTintColor = Common.secondaryColor
        UITabBar.appearance().tintColor = Common.tertiaryColor
        
        UINavigationBar.appearance().barTintColor = Common.tertiaryColor
        UINavigationBar.appearance().isTranslucent = true
        // Navigation bar text
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: Common.primaryColor]
        
        let placeholderAttributes = [NSAttributedString.Key.foregroundColor: Common.primaryColor, NSAttributedString.Key.font.rawValue: UIFont.systemFont(ofSize: UIFont.systemFontSize)] as! [NSAttributedString.Key: Any]
        let attributedPlaceholder: NSAttributedString = NSAttributedString(string: "Search Airya", attributes: placeholderAttributes)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = attributedPlaceholder
        // Cancel button tinit
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = Common.primaryColor
        // Search text color
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: Common.tertiaryColor]
    }
    
}

extension String {
    private static let __firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
    private static let __serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
    private static let __emailRegex = __firstpart + "@" + __serverpart + "[A-Za-z]{2,6}"
    
    public var isEmail: Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", type(of:self).__emailRegex)
        return predicate.evaluate(with: self)
    }
    
    public var validUsername: Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", type(of:self).__firstpart)
        return predicate.evaluate(with: self)
    }
}

struct Validators {
    
    func validateEmail(text: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: text)
    }
    
    func validatePassword(passwordText: String, confirmationText: String) -> Bool {
        let passwordCount: Int = 6
        
        guard (!passwordText.isEmpty && !confirmationText.isEmpty) &&
            (passwordText == confirmationText) &&
            (passwordText.count >= passwordCount && confirmationText.count >= passwordCount) else {
                return false
        }
        return true
    }
    
    func validateUsername(text: String) -> Bool {
        return text.validUsername
    }
}
