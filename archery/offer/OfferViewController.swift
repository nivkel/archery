//
//  OfferViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/24/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OfferViewController: BaseViewController {
    
    var userPayload: [String: String]!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var priceTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var sendOfferButton: UIButton!
    @IBOutlet weak var shipToMeButton: UIButton!
    @IBAction func sendOfferButtonAction(_ sender: UIButton) {
        var dataSource: ChatDataSource!
        var message: ChatTextMessageModel!
        
        ChatServicesManager.shared.getUserPayload().get { userPayload in
            let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Sending Offer", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .gray, textColor: .white)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            self.userPayload = userPayload
        }.then { userId in
            ChatServicesManager.shared.handleConversationCreation(post: self.post, subject: "buy", senderName: self.userPayload["username"])
        }.get { conversationId in
            guard let userId = self.userPayload["sub"], let username = self.userPayload["username"], let offer = self.priceTextView.text else {
                return
            }
            dataSource = ChatDataSource(count: 0, pageSize: 1, conversationId: conversationId)
            message = dataSource.makeTextMessage(UUID().uuidString, text: "\(username) offering \(offer)", senderId: userId, isIncoming: false, status: .sending, date: Date())
        }.then { conversationId in
            ChatServicesManager.shared.addMessage(message: message, conversationId: conversationId, text: message.text)
        }.done { messageId in
            debugPrint("addMessage messageId\(messageId)")
            Common.showNoticeMessage(title: "Offer", message: "Offer sent!", theme: .success, style: .center)
        }.ensure {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }.catch { error in
            debugPrint("addMessage error\(error)")
        }
    }
    @IBAction func shipToMeButtonAction(_ sender: UIButton) {
        var dataSource: ChatDataSource!
        var message: ChatTextMessageModel!
        
        ChatServicesManager.shared.getUserPayload().get { userPayload in
            let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Sending message", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .gray, textColor: .white)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            self.userPayload = userPayload
        }.then { userId in
            ChatServicesManager.shared.handleConversationCreation(post: self.post, subject: "buy", senderName: self.userPayload["username"])
        }.get { conversationId in
            guard let userId = self.userPayload["sub"], let username = self.userPayload["username"], let title = self.post.title else {
                return
            }
            dataSource = ChatDataSource(count: 0, pageSize: 1, conversationId: conversationId)
            message = dataSource.makeTextMessage(UUID().uuidString, text: "\(username) requesting to ship this item \(title)", senderId: userId, isIncoming: false, status: .sending, date: Date())
        }.then { conversationId in
            ChatServicesManager.shared.addMessage(message: message, conversationId: conversationId, text: message.text)
        }.done { messageId in
            debugPrint("addMessage messageId\(messageId)")
            Common.showNoticeMessage(title: "Shipping", message: "Shipping message sent!", theme: .success, style: .center)
        }.ensure {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }.catch { error in
            debugPrint("addMessage error\(error)")
        }
    }
    
    var post: Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sendOfferButton.layer.cornerRadius = 10.0
        shipToMeButton.layer.cornerRadius = 10.0
        postImageView.setFAIconWithName(icon: .FAFileImageO, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
        
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(OfferViewController.close))
        closeButton.tintColor = Common.primaryColor
        self.navigationItem.rightBarButtonItem = closeButton
        
        self.navigationItem.title = self.post.title
        
        setupKeyboardHandling(textField: nil, textView: self.priceTextView, scrollView: self.scrollView)
        
        let height = self.view.frame.size.height - 110
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.postLabel.text = self.post.title
        self.priceTextView.text = self.post.price
        self.postImageView.clipsToBounds = true
        
        if let file = self.post.file {
            postImageView.kf.indicatorType = .activity
            AWSS3Manager.shared.downloadImage(imageView: postImageView, key: file.key) { (complete) in
                debugPrint("downloadImage:\(complete)")
            }
        }
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OfferViewController {
    override func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: (textView.superview?.frame.origin.y)!), animated: true)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        let allowedCharacters = CharacterSet.decimalDigits.union (CharacterSet (charactersIn: "."))
        let characterSet = CharacterSet(charactersIn: updatedText)
        let validCharacters = allowedCharacters.isSuperset(of: characterSet)
        
        let maxLength = 10
        let newString = updatedText as NSString
        let validLength = newString.length <= maxLength
        let charge = String(newString)
        debugPrint("price string :\(charge)")
        
        if validCharacters && validLength {
            
        }
        
        return validCharacters && validLength
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        print("CharChange:\(textView.text.count)")
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        print("didEnd")
    }
}
