//
//  LocationServicesManager.swift
//  archery
//
//  Created by Kelvin Lee on 1/4/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import CoreLocation

class LocationServicesManager {
    static let shared = LocationServicesManager()
    
    func getCoordinateFrom(location : String) -> CLLocation? {
        guard let latitude = Double(location.split(separator: ",").first!), let longitude = Double(location.split(separator: ",").last!) else {
            return nil
        }
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        return location
    }
    
    func getCityFromCoordinate(location: String, completion: (@escaping (_ city: String) -> Void)) {
        if let location = getCoordinateFrom(location: location) {
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
                
                placemarks?.forEach { (placemark) in
                    var title = ""
                    var subtitle = ""
                    if let state = placemark.administrativeArea, let county = placemark.subAdministrativeArea {
                        title = "\(county), \(state)"
                    }
                    
                    if let city = placemark.locality, let neighborhood = placemark.subLocality {
                        subtitle = "\(neighborhood), \(city)"
                    }
                    completion(subtitle)
                }
            })
        } else {
            completion("")
        }
        
    }
}
