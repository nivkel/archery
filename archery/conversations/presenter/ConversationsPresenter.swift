//
//  ConversationsPresenter.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class ConversationsPresenter: ViewToPresenterConversationsProtocol {
    var view: PresenterToViewConversationsProtocol?
    
    var interactor: PresenterToInteractorConversationsProtocol?
    
    var router: PresenterToRouterConversationsProtocol?
    
    func startFetchConversations(limit: Int?, nextToken: String?) {
        interactor?.fetchConversations(limit: limit, nextToken: nextToken)
    }
    
    func showChatroomController(navigationController: UINavigationController) {
        router?.pushChatroomController(navigationController: navigationController)
    }
    

}

extension ConversationsPresenter: InteractorToPresenterConversationsProtocol {
    func conversationsFetchSuccess(response: [UserConversation], nextToken: String?) {
        view?.conversations(response: response, nextToken: nextToken)
    }
    
    func conversationsFetchFailed() {
        view?.showError()
    }
    
    
}
