//
//  ConversationsRouter.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class ConversationsRouter: PresenterToRouterConversationsProtocol {
    static func createModule() -> ConversationsViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        
        let presenter: ViewToPresenterConversationsProtocol & InteractorToPresenterConversationsProtocol = ConversationsPresenter()
        let interactor: PresenterToInteractorConversationsProtocol = ConversationsInteractor()
        let router:PresenterToRouterConversationsProtocol = ConversationsRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func pushChatroomController(navigationController: UINavigationController) {
        
    }
    

}
