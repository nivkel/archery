//
//  ConversationsInteractor.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import PromiseKit

class ConversationsInteractor: PresenterToInteractorConversationsProtocol {
    var presenter: InteractorToPresenterConversationsProtocol?
    
    func fetchConversations(limit: Int?, nextToken: String?) {
        firstly {
            ChatServicesManager.shared.getUserConversations()
        }.get { userConversations in
            debugPrint("userConversations:\(userConversations)")
            self.presenter?.conversationsFetchSuccess(response: userConversations, nextToken: nil)
        }.catch { error in
            debugPrint("listConversations error:\(error)")
            self.presenter?.conversationsFetchFailed()
        }
    }
    

}
