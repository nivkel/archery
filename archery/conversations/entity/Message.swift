//
//  Message.swift
//  archery
//
//  Created by Kelvin Lee on 12/9/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSAppSync
import Chatto
import ChattoAdditions

class Message {
    var id: GraphQLID
    var content: String
    var conversationId: GraphQLID
    var createdAt: String
    var sender: GraphQLID
    var senderName: String
    var isSent: Bool
    
    init(id: GraphQLID, content: String, conversationId: GraphQLID, createdAt: String, sender: GraphQLID, senderName: String, isSent: Bool) {
        self.id = id
        self.content = content
        self.conversationId = conversationId
        self.createdAt = createdAt
        self.sender = sender
        self.senderName = senderName
        self.isSent = isSent
    }
}

extension Message: ChatItemProtocol {
    var type: ChatItemType {
        return TextMessageModel<MessageModel>.chatItemType
    }
    
    var uid: String {
        return self.id
    }
    
    
}
