//
//  Conversation.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import AWSAppSync

class Conversation {
    var id: GraphQLID
    var subject: String
    var createdAt: String
    
    init(id: GraphQLID, subject: String, createdAt: String) {
        self.id = id
        self.subject = subject
        self.createdAt = createdAt
    }
}

extension Conversation: ListDiffable {
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? Conversation {
            return subject == object.subject
        }
        return false
    }
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
}
