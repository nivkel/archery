//
//  UserConversation.swift
//  archery
//
//  Created by Kelvin Lee on 12/1/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import AWSAppSync

class UserConversation {
    
    var userId: GraphQLID
    var postId: GraphQLID
    var conversationId: GraphQLID
    var subject: String
    var conversation = [Conversation]()
    
    var userProfile: String?
    var username: String?
    var postTitle: String?
    var postPrice: String?
    var postAuthor: String?
    var senderName: String?
    var postFileKey: String?
    var createdAt: String?
    
    init(userId: GraphQLID, postId: GraphQLID, conversationId: GraphQLID, subject: String, userProfile: String?, username: String?, postTitle: String?, postPrice: String?, postAuthor: String?, senderName: String?, postFileKey: String?, createdAt: String?) {
        self.userId = userId
        self.postId = postId
        self.conversationId = conversationId
        self.subject = subject
        self.userProfile = userProfile
        self.username = username
        self.postTitle = postTitle
        self.postPrice = postPrice
        self.postAuthor = postAuthor
        self.senderName = senderName
        self.postFileKey = postFileKey
        self.createdAt = createdAt
    }

    init(emptyTitle: String) {
        self.userId = "empty"
        self.postId = "empty"
        self.conversationId = "empty"
        self.subject = emptyTitle
    }
}

extension UserConversation: ListDiffable {
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? UserConversation {
            return userId == object.userId
        }
        return false
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return conversationId as NSObjectProtocol
    }
}
