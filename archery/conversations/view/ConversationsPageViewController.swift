//
//  ConversationsPageViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/22/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

class ConversationsPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pages = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
        
        let buyConversationsViewController = ConversationsRouter.createModule()
        buyConversationsViewController.type = .buy
        let sellConversationsViewController = ConversationsRouter.createModule()
        sellConversationsViewController.type = .sell
        
        let notificationsViewController = ConversationsRouter.createModule()
        notificationsViewController.type = .notifications
        
        self.pages.append(buyConversationsViewController)
        self.pages.append(sellConversationsViewController)
//        self.pages.append(notificationsViewController)

        self.setViewControllers([buyConversationsViewController], direction: .forward, animated: true, completion: nil)
        
        self.navigationItem.title = "Airya"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.parent?.navigationItem.rightBarButtonItem = nil
    }
    
    func goToBuyingPage() {
        self.setViewControllers([pages[0]], direction: .reverse, animated: true, completion: nil)
    }
    
    func goToSellingPage() {
        self.setViewControllers([pages[1]], direction: .forward, animated: true, completion: nil)
    }
    
    func goToNotificationsPage() {
        self.setViewControllers([pages[2]], direction: .forward, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case 0:
            return nil
        default:
            return self.pages[currentIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = pages.index(of: viewController) else {
            return nil
        }
        switch currentIndex {
        case (self.pages.count - 1):
            return nil
        default:
            return self.pages[currentIndex + 1]
        }
    }


}
