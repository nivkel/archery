//
//  ConversationsViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit
import BadgeSwift

enum ConversationsType: String {
    case buy = "buy"
    case sell = "sell"
    case notifications = "notifications"
}

class ConversationsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func buyingButtonAction(_ sender: UIButton) {
        guard let pageViewController = self.parent as? ConversationsPageViewController else {
            return
        }
        pageViewController.goToBuyingPage()
    }
    @IBAction func sellingButtonAction(_ sender: UIButton) {
        guard let pageViewController = self.parent as? ConversationsPageViewController else {
            return
        }
        pageViewController.goToSellingPage()
    }
    @IBAction func notificationsButtonAction(_ sender: UIButton) {
        guard let pageViewController = self.parent as? ConversationsPageViewController else {
            return
        }
        pageViewController.goToNotificationsPage()
    }
    
    @IBOutlet weak var buyingButton: UIButton!
    @IBOutlet weak var sellingButton: UIButton!
    @IBOutlet weak var notificationsButton: UIButton!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = Common.backgroundColor
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(ConversationsViewController.handleRefresh), for: .valueChanged)
        return refreshControl
    }()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 4)
    }()
    
    var type: ConversationsType = .buy
    
    var presenter: ViewToPresenterConversationsProtocol?
    
    var userConversations = [UserConversation]()
    var nextToken: String?
    
    var buyBadge = BadgeSwift()
    var sellBadge = BadgeSwift()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let layout = ListCollectionViewLayout(stickyHeaders: true, scrollDirection: .vertical, topContentInset: 2.0, stretchToEdge: true)
//        layout.minimumLineSpacing = 2.0
//        layout.minimumInteritemSpacing = 2.0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
        self.adapter.delegate = self
        
        self.collectionView.addSubview(self.refreshControl)
        
        self.buyingButton.layer.cornerRadius = 10.0
        self.sellingButton.layer.cornerRadius = 10.0
        self.notificationsButton.layer.cornerRadius = 10.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        debugPrint("convo type:\(self.type)")
        switch self.type {
        case .buy:
            self.buyingButton.backgroundColor = Common.primaryColor
            self.buyingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.sellingButton.backgroundColor = UIColor.darkGray
            self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.notificationsButton.backgroundColor = UIColor.darkGray
            self.notificationsButton.setTitleColor(Common.primaryColor, for: .normal)
            break
        case .sell:
            self.buyingButton.backgroundColor = UIColor.darkGray
            self.buyingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.sellingButton.backgroundColor = Common.primaryColor
            self.notificationsButton.backgroundColor = UIColor.darkGray
            self.notificationsButton.setTitleColor(Common.tertiaryColor, for: .normal)
            break
        case .notifications:
            self.buyingButton.backgroundColor = UIColor.darkGray
            self.buyingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.sellingButton.setTitleColor(Common.tertiaryColor, for: .normal)
            self.sellingButton.backgroundColor = UIColor.darkGray
            self.notificationsButton.backgroundColor = Common.primaryColor
            self.notificationsButton.setTitleColor(Common.tertiaryColor, for: .normal)
            break
        }
        
        DispatchQueue.main.async {
            self.createBuyBadge(view: self.buyingButton)
            self.createSellBadge(view: self.sellingButton)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter?.startFetchConversations(limit: nil, nextToken: nil)
        
        switch self.type {
        case .buy:
            clearBuyBadge()
            break
        case .sell:
            clearSellBadge()
            break
        case .notifications:
            break
        }
    }
    
    @objc func handleRefresh() {
        debugPrint("Refresh")
        
        
    }
    
    private func clearSellBadge() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.sellBadge.alpha = 0.1
            }, completion: { (complete) in
                UserDefaults.standard.set(0, forKey: "sellSubject")
                self.sellBadge.removeFromSuperview()
                Common.setTotalTabBarBadge(application: UIApplication.shared)
            })
        }
    }
    
    private func clearBuyBadge() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.buyBadge.alpha = 0.1
            }, completion: { (complete) in
                UserDefaults.standard.set(0, forKey: "buySubject")
                self.buyBadge.removeFromSuperview()
                Common.setTotalTabBarBadge(application: UIApplication.shared)
            })
        }
    }
    
    private func createBuyBadge(view: UIView) {
        let count = UserDefaults.standard.integer(forKey: "buySubject")
        if count == 0 {
            return
        }
        buyBadge.removeFromSuperview()
        view.addSubview(buyBadge)
        self.buyBadge.alpha = 1.0
        configureBadge(buyBadge, count: "\(count)")
        positionBadge(buyBadge, view: view)
    }
    
    private func createSellBadge(view: UIView) {
        let count = UserDefaults.standard.integer(forKey: "sellSubject")
        if count == 0 {
            return
        }
        sellBadge.removeFromSuperview()
        view.addSubview(sellBadge)
        self.sellBadge.alpha = 1.0
        configureBadge(sellBadge, count: "\(count)")
        positionBadge(sellBadge, view: view)
    }
    
    private func configureBadge(_ badge: BadgeSwift, count: String) {
        // Text
        badge.text = count
        
        // Insets
        badge.insets = CGSize(width: 4, height: 4)
        
        // Font
        badge.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        
        // Text color
        badge.textColor = UIColor.white
        
        // Badge color
        badge.badgeColor = UIColor.red
        
        // Shadow
        badge.shadowOpacityBadge = 0.5
        badge.shadowOffsetBadge = CGSize(width: 0, height: 0)
        badge.shadowRadiusBadge = 1.0
        badge.shadowColorBadge = UIColor.black
        
        // No shadow
        badge.shadowOpacityBadge = 0
        
        // Border width and color
        badge.borderWidth = 1.0
        badge.borderColor = UIColor.red
    }
    
    private func positionBadge(_ badge: UIView, view: UIView) {
        badge.translatesAutoresizingMaskIntoConstraints = false
        var constraints = [NSLayoutConstraint]()
        
        // Center the badge vertically in its container
        constraints.append(NSLayoutConstraint(
            item: badge,
            attribute: NSLayoutConstraint.Attribute.bottom,
            relatedBy: NSLayoutConstraint.Relation.equal,
            toItem: view,
            attribute: NSLayoutConstraint.Attribute.bottom,
            multiplier: 1, constant: 8)
        )
        
        // Center the badge horizontally in its container
        constraints.append(NSLayoutConstraint(
            item: badge,
            attribute: NSLayoutConstraint.Attribute.right,
            relatedBy: NSLayoutConstraint.Relation.equal,
            toItem: view,
            attribute: NSLayoutConstraint.Attribute.right,
            multiplier: 1, constant: 10)
        )
        
        view.addConstraints(constraints)
    }

}

extension ConversationsViewController: PresenterToViewConversationsProtocol {
    func conversations(response: [UserConversation], nextToken: String?) {
        
        var conversations = [UserConversation]()
        
        switch self.type {
        case .buy:
            let buyConversations = response.filter { $0.subject == "buy"}
            conversations = buyConversations
            break
        case .sell:
            let sellConversations = response.filter { $0.subject == "sell"}
            conversations = sellConversations
            break
        case .notifications:
            break
        }
        
        if conversations.isEmpty {
            conversations = [UserConversation(emptyTitle: "You don't have any \(self.type.rawValue) messages")]
        }
        
        self.nextToken = nextToken

        self.userConversations = conversations
        
        self.adapter.performUpdates(animated: true, completion: { (finished) in
            
        })
        self.refreshControl.endRefreshing()
    }
    
    func showError() {
        debugPrint("Fetch error")
    }
    
    
}

extension ConversationsViewController: IGListAdapterDelegate {
    func listAdapter(_ listAdapter: ListAdapter, willDisplay object: Any, at index: Int) {
        debugPrint("willDisplay index:\(index)")
        
        if self.userConversations.count - 1 == index {
            if let nextToken = self.nextToken {
                debugPrint("willDisplay index:\(index) pagination token:")
//                self.presenter?.startFetchPosts(limit: 10, nextToken: nextToken)
            }
        }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, didEndDisplaying object: Any, at index: Int) {
        
    }
}

extension ConversationsViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return self.userConversations
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        default:
            return ConversationsSectionController(type: self.type)
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let customView = SkeletonView(frame: self.collectionView.frame)
//        customView.loadingIndicator.startAnimating()
        
        return customView
    }
}
