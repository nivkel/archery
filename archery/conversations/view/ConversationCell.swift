//
//  ConversationCell.swift
//  archery
//
//  Created by Kelvin Lee on 1/19/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class ConversationCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 8.0
        titleLabel.layer.cornerRadius = 10.0
        priceLabel.layer.cornerRadius = 6.0
        titleLabel.clipsToBounds = true
        priceLabel.clipsToBounds = true
        postImageView.layer.cornerRadius = 8.0
        postImageView.clipsToBounds = true
        
        iconImageView.setFAIconWithName(icon: .FATags, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
        iconImageView.layer.cornerRadius = 8.0
        iconImageView.clipsToBounds = true
    }
    
    func setup(s3Object: AWSS3Object) {
        guard let key = s3Object.key else {
            return
        }
        let width = Int(self.postImageView.frame.size.width) * 2
        let height = Int(self.postImageView.frame.size.height) * 2
        debugPrint("imageView size height:\(height), width:\(width)")
        let fitInResolution = "\(width)x\(height)/"
        let urlString = AWSS3Object.imageHandlerUrl + fitInResolution + "\(key)"
        debugPrint("s3 urlString:\(urlString)")
        guard let url = URL(string: urlString) else {
            return
        }
        debugPrint("s3 url:\(url)")
        let imageResource = ImageResource(downloadURL: url)
        let placeHolder = UIImage(icon: .FAFileImageO, size: CGSize(width: 100.0, height: 100.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor)
        self.postImageView.kf.indicatorType = .activity
        self.postImageView.kf.setImage(with: imageResource, placeholder: placeHolder, options: [.transition(.fade(0.2))], progressBlock: { (received, total) in
            debugPrint("Progress received:\(received), total:\(total)")
        }) { (result) in
            switch result {
            case .success(let value):
                // The image was set to image view:
                print(value.image)
                self.postImageView.image = value.image
                // From where the image was retrieved:
                // - .none - Just downloaded.
                // - .memory - Got from memory cache.
                // - .disk - Got from disk cache.
                print(value.cacheType)
                debugPrint("Image result:\(value.image), cache:\(value.cacheType)")
                // The source object which contains information like `url`.
                print(value.source)
            case .failure(let error):
                print("error \(error)") // The error happens
            }
        }
    }
}
