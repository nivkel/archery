//
//  ConversationsSectionController.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import IGListKit

class ConversationsSectionController: ListSectionController {

    var userConversation: UserConversation!
    
    var type: ConversationsType
    
    init(type: ConversationsType) {
        self.type = type
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard var width = self.collectionContext?.containerSize.width else {
            return CGSize(width: 0, height: 100)
        }
        if userConversation.postId == "empty" {
            return CGSize(width: width, height: 200.0)
        }
//        width = floor(width / 2)
        return CGSize(width: width, height: 90.0)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = self.collectionContext!.dequeueReusableCell(withNibName: ConversationCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! ConversationCell
        
        if userConversation.postId == "empty" {
            let emptyCell = self.collectionContext!.dequeueReusableCell(withNibName: EmptyCell.reuseIdentifier, bundle: Bundle.main, for: self, at: index) as! EmptyCell
            emptyCell.titleLabel.text = userConversation.subject
            emptyCell.titleLabel.textColor = Common.secondaryColor
            emptyCell.iconImageView.setFAIconWithName(icon: .FAEnvelopeOpen, textColor: Common.secondaryColor, orientation: .up, backgroundColor: Common.backgroundColor, size: CGSize(width: 50.0, height: 50.0))
            return emptyCell
        }
        if let title = userConversation.postTitle, let price = userConversation.postPrice {
            cell.titleLabel.text = title
            cell.priceLabel.text = "$\(price)"
            debugPrint("userConversation date:\(userConversation.createdAt)")
            if let createdAt = userConversation.createdAt {
                cell.timeStampLabel.text = Common.createdAtRelativeDate(value: createdAt) + " ago"
            }
            
            switch self.type {
            case .buy:
                if let author = userConversation.postAuthor {
                    cell.authorLabel.text = author
                    cell.userImageView.setImageForName(Common.usernameInitials(value: author), gradientColors: (Common.primaryColor, UIColor.white), circular: true, textAttributes: nil)
                } else {
                    cell.authorLabel.text = "Post author"
                    cell.userImageView.setImageForName(Common.usernameInitials(value: "Post author"), gradientColors: (Common.primaryColor, UIColor.white), circular: true, textAttributes: nil)
                }
                debugPrint("buy author:\(userConversation.postAuthor)")
                break
            case .sell:
                if let sender = userConversation.senderName {
                    cell.authorLabel.text = sender
                    cell.userImageView.setImageForName(Common.usernameInitials(value: sender), gradientColors: (Common.primaryColor, UIColor.white), circular: true, textAttributes: nil)
                } else {
                    cell.authorLabel.text = "Convo sender"
                    cell.userImageView.setImageForName(Common.usernameInitials(value: "Convo sender"), gradientColors: (Common.primaryColor, UIColor.white), circular: true, textAttributes: nil)
                }
                debugPrint("sell username:\(userConversation.username)")
                break
            case .notifications:
                break
            }
            
        } else {
            cell.titleLabel.text = "title"
            cell.priceLabel.text = "$999"
            cell.authorLabel.text = "author"
        }
        
        if let key = userConversation.postFileKey {
            let s3Object = AWSS3Object(key: key, bucket: "", region: "")
            cell.setup(s3Object: s3Object)
        } else {
            cell.postImageView.setFAIconWithName(icon: .FAPhoto, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
        }
        
//        cell.postImageView.setFAIconWithName(icon: .FAPhoto, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
//        cell.postImageView.layer.cornerRadius = 8.0
//        cell.postImageView.clipsToBounds = true
        
//        cell.iconImageView.setFAIconWithName(icon: .FATags, textColor: Common.primaryColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 50.0, height: 50.0))
//        cell.iconImageView.layer.cornerRadius = 8.0
//        cell.iconImageView.clipsToBounds = true
        
//        cell.userImageView.setFAIconWithName(icon: .FAUserCircle, textColor: .darkGray, orientation: .up, backgroundColor: .clear, size: CGSize(width: 20.0, height: 20.0))
//        cell.userImageView.layer.cornerRadius = 20.0
//        cell.userImageView.clipsToBounds = true
        
        return cell
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func didUpdate(to object: Any) {
        self.userConversation = object as? UserConversation
    }
    
    override func didSelectItem(at index: Int) {
        if userConversation.postId == "empty" {
            return
        }
        
        let chatViewController = ChatRouter.createModule()
        switch type {
        case .buy:
            if let author = userConversation.postAuthor {
                chatViewController.titleValue = author
            }
            break
        case .sell:
            if let sender = userConversation.senderName {
                chatViewController.titleValue = sender
            }
            break
        case .notifications:
            break
        }
        let dataSource = ChatDataSource(count: 0, pageSize: 50, conversationId: self.userConversation.conversationId)
        chatViewController.dataSource = dataSource
        chatViewController.conversationId = self.userConversation.conversationId
        
        guard let conversationsViewController = self.viewController as? ConversationsViewController else {
            return
        }
        conversationsViewController.navigationController?.pushViewController(chatViewController, animated: true)
        
    }

}
