//
//  ConversationsProtocol.swift
//  archery
//
//  Created by Kelvin Lee on 11/8/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit

protocol ViewToPresenterConversationsProtocol: class {
    var view: PresenterToViewConversationsProtocol? {get set}
    var interactor: PresenterToInteractorConversationsProtocol? {get set}
    var router: PresenterToRouterConversationsProtocol? {get set}
    func startFetchConversations(limit: Int?, nextToken: String?)
    func showChatroomController(navigationController:UINavigationController)
}

protocol PresenterToViewConversationsProtocol: class {
    func conversations(response: [UserConversation], nextToken: String?)
    func showError()
}

protocol PresenterToRouterConversationsProtocol: class {
    static func createModule() -> ConversationsViewController
    func pushChatroomController(navigationController:UINavigationController)
}

protocol PresenterToInteractorConversationsProtocol: class {
    var presenter:InteractorToPresenterConversationsProtocol? {get set}
    func fetchConversations(limit: Int?, nextToken: String?)
}

protocol InteractorToPresenterConversationsProtocol: class {
    func conversationsFetchSuccess(response: [UserConversation], nextToken: String?)
    func conversationsFetchFailed()
}
