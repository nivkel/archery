//
//  BaseLoginViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/30/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit

class BaseLoginViewController: UIViewController {
    
    private var baseScrollView: UIScrollView!
    
    enum TextFieldTagSignIn: Int {
        case email
        case password
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(BaseLoginViewController.close))
        self.navigationItem.rightBarButtonItem = closeButton
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 100)
    }
    
    func setupKeyboardHandling(textFields: [UITextField], scrollView: UIScrollView) {
        self.baseScrollView = scrollView
        
        for textField in textFields {
            textField.delegate = self
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        let toolbar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 30.0))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 30.0, height: 30.0), orientation: .up, textColor: UIColor.black, backgroundColor: Common.backgroundColor), style: .done, target: self.view, action: #selector(self.view.endEditing(_:)))
        closeButton.tintColor = UIColor.black
        toolbar.setItems([flexSpace, closeButton], animated: false)
        toolbar.sizeToFit()
        
        for textField in textFields {
            textField.inputAccessoryView = toolbar
        }
    }
    
    @objc private func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let padding: CGFloat = 80.0
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            baseScrollView.contentInset = UIEdgeInsets.zero
        } else {
            baseScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        baseScrollView.scrollIndicatorInsets = baseScrollView.contentInset
    }

    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension BaseLoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("title text:\(String(describing: textField.text))")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
