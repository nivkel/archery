//
//  PhoneViewController.swift
//  archery
//
//  Created by Kelvin Lee on 3/28/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import PhoneNumberKit

class PhoneViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        if phoneTextField.isValidNumber {
            let signUpViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            signUpViewController.phone = phoneTextField.nationalNumber
            self.navigationController?.pushViewController(signUpViewController, animated: true)
        } else {
            Common.showNoticeMessage(title: "", message: "Enter a valid number", theme: .warning, style: .top)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(PhoneViewController.close))
        self.navigationItem.rightBarButtonItem = closeButton
        self.navigationController?.navigationBar.barTintColor = .white
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(named: "BarBackground"), for: .default)
        navigationBar.shadowImage = UIImage()
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 100)
        
        phoneTextField.delegate = self
        phoneTextField.maxDigits = 10
        logoImageView.clipsToBounds = true
        logoImageView.setFAIconWithName(icon: .FAPhone, textColor: .black, orientation: .up, backgroundColor: .white, size: CGSize(width: 100.0, height: 100.0))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        phoneTextField.becomeFirstResponder()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        return
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        return
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}
