//
//  PasswordResetViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/25/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import SwiftMessages
import AWSMobileClient

class PasswordResetViewController: BaseLoginViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var sendResetButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    @IBAction func sendResetButtonAction(_ sender: UIButton) {
        if let username = usernameTextField.text {
            AWSMobileClient.sharedInstance().forgotPassword(username: username) { (forgotPasswordResult, error) in
                if let forgotPasswordResult = forgotPasswordResult {
                    switch(forgotPasswordResult.forgotPasswordState) {
                    case .confirmationCodeSent:
                        print("Confirmation code sent via \(forgotPasswordResult.codeDeliveryDetails!.deliveryMedium) to: \(forgotPasswordResult.codeDeliveryDetails!.destination!)")
                        DispatchQueue.main.async {
                            Common.showNoticeMessage(title: "", message: "Confirmation code sent via \(forgotPasswordResult.codeDeliveryDetails!.deliveryMedium) to: \(forgotPasswordResult.codeDeliveryDetails!.destination!)", theme: .success, style: .center)
                            self.resetPasswordButton.isHidden = false
                        }
                    default:
                        print("Error: Invalid case.")
                    }
                } else if let error = error {
                    print("Error occurred: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "Confirmation code error \(error.localizedDescription)", theme: .error, style: .center)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                Common.showNoticeMessage(title: "", message: "Fields incomplete", theme: .error, style: .center)
            }
        }
    }
    
    @IBAction func resetPasswordButtonAction(_ sender: UIButton) {
        if let username = usernameTextField.text, let newPassword = passwordTextField.text, let code = confirmPasswordTextField.text {
            AWSMobileClient.sharedInstance().confirmForgotPassword(username: username, newPassword: newPassword, confirmationCode: code) { (forgotPasswordResult, error) in
                if let forgotPasswordResult = forgotPasswordResult {
                    switch(forgotPasswordResult.forgotPasswordState) {
                    case .done:
                        print("Password changed successfully")
                        DispatchQueue.main.async {
                            Common.showNoticeMessage(title: "", message: "Password changed successfully", theme: .success, style: .center)
                        }
                    default:
                        print("Error: Could not change password.")
                    }
                } else if let error = error {
                    print("Error occurred: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "Password changed error \(error.localizedDescription)", theme: .error, style: .center)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                Common.showNoticeMessage(title: "", message: "Fields incomplete", theme: .error, style: .center)
            }
        }
    }
    
    var username: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = Common.primaryColor
        preferredContentSize = CGSize(width: 300, height: 400)
        self.setupKeyboardHandling(textFields: [self.usernameTextField, self.passwordTextField, self.confirmPasswordTextField], scrollView: self.scrollView)
        
        self.usernameTextField.text = self.username
        
        self.navigationController?.navigationBar.barTintColor = .white
        
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(named: "BarBackground"), for: .default)
        navigationBar.shadowImage = UIImage()
    }
    

}
