//
//  SignUpViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/25/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSMobileClient
import SwiftMessages

class SignUpViewController: BaseLoginViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        guard let values = handleTextFields() else {
            return
        }
        let username = values["username"]!
        let email = values["email"]!
        let password = values["password"]!
        let phoneNumber = self.phone!
        AWSMobileClient.sharedInstance().signUp(username: username,
                                                password: password,
                                                userAttributes: ["email": email/*, "phone_number": "+\(phoneNumber)"*/]) { (signUpResult, error) in
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                    AWSMobileClient.sharedInstance().signIn(username: username, password: password, completionHandler: { (result, error) in
                        print("User signIn: \(result), error:\(error)")
//                        self.showMain()
                        Common.showOnboard()
                    })
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                    self.showConfirm(values: values)
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                if let error = error as? AWSMobileClientError {
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                        DispatchQueue.main.async {
                            Common.showNoticeMessage(title: "", message: "Username exists", theme: .warning, style: .top)
                        }
                    default:
                        DispatchQueue.main.async {
                            Common.showNoticeMessage(title: "", message: "Signup error \(error)", theme: .warning, style: .top)
                        }
                        break
                    }
                }
                print("\(error.localizedDescription)")
            }
        }
    }
    
    var phone: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.logoImageView.setFAIconWithName(icon: .FALeaf, textColor: Common.primaryColor, orientation: .up, backgroundColor: .black, size: CGSize(width: 100.0, height: 100.0))
        self.navigationController?.navigationBar.tintColor = Common.primaryColor
        self.navigationController?.navigationBar.barTintColor = .white
        
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(named: "BarBackground"), for: .default)
        navigationBar.shadowImage = UIImage()
        
        self.setupKeyboardHandling(textFields: [usernameTextField, emailTextField, passwordTextField, confirmPasswordTextField], scrollView: self.scrollView)
        
        self.confirmPasswordTextField.isHidden = true
        
        debugPrint("SignUp phone:\(self.phone)")
    }
    
    func showMain() {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
            Common.showMain(mainNavigationController: rootViewController)
        }
    }
    
    func showConfirm(values: [String: String]) {
        let username = values["username"]!
        let email = values["email"]!
        let password = values["password"]!
        DispatchQueue.main.async {
            let confirmViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmViewController") as! ConfirmViewController
            confirmViewController.username = username
            confirmViewController.email = email
            confirmViewController.password = password
            let navigationController = UINavigationController(rootViewController: confirmViewController)
            let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
            segue.configure(layout: .bottomCard)
            segue.perform()
        }
    }
    
    func handleTextFields() -> [String: String]? {
        guard let usernameText = self.usernameTextField.text else {
            return nil
        }
        guard let emailText = emailTextField.text else {
            return nil
        }
        guard let passwordText = passwordTextField.text else {
            return nil
        }
//        guard let confirmPasswordText = confirmPasswordTextField.text else {
//            return nil
//        }
        let trimmedEmailText = emailText.trimmingCharacters(in: .whitespaces)
        let trimmedPasswordText = passwordText.trimmingCharacters(in: .whitespaces)
//        let trimmedConfirmPasswordText = confirmPasswordText.trimmingCharacters(in: .whitespaces)
        let trimmedUsernameText = usernameText.trimmingCharacters(in: .whitespaces)
        
        let validator = Validators()
        let isUsernameValid = validator.validateUsername(text: trimmedUsernameText)
        let isEmailValid = trimmedEmailText.isEmail
//        let isPasswordValid = validator.validatePassword(passwordText: trimmedPasswordText, confirmationText: trimmedConfirmPasswordText)
//        print("isEmail:\(isEmailValid) isPassword:\(isPasswordValid)")
//        print("email:\(trimmedEmailText) password:\(trimmedPasswordText), confirmation:\(trimmedConfirmPasswordText)")
        
        if !isUsernameValid {
            Common.showStatusMessage(title: "Incomplete", message: "Username not valid", theme: .error, style: .bottom)
            return nil
        }
        if !isEmailValid {
            Common.showStatusMessage(title: "Incomplete", message: "Email not valid", theme: .error, style: .bottom)
            return nil
        }
//        if !isPasswordValid {
//            Common.showStatusMessage(title: "Incomplete", message: "Password not valid", theme: .error, style: .bottom)
//            return nil
//        }
        
        guard isEmailValid /*&& isPasswordValid*/ && isUsernameValid else {
            print("Email, password not valid")
            return nil
        }
        return ["email": trimmedEmailText, "password": trimmedPasswordText, "username": trimmedUsernameText]
    }
}
