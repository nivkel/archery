//
//  ConfirmViewController.swift
//  archery
//
//  Created by Kelvin Lee on 1/30/19.
//  Copyright © 2019 Kelvin Lee. All rights reserved.
//

import UIKit
import AWSMobileClient
import NVActivityIndicatorView

class ConfirmViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmationTextField: UITextField!
    
    @IBOutlet weak var confirmationButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    
    @IBAction func resendButtonAction(_ sender: UIButton) {
        if let username = usernameTextField.text {
            AWSMobileClient.sharedInstance().resendSignUpCode(username: username, completionHandler: { (result, error) in
                if let signUpResult = result {
                    print("A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)", theme: .success, style: .center)
                    }
                } else if let error = error {
                    print("\(error.localizedDescription)")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "Error sending code \(error)", theme: .error, style: .center)
                    }
                }
            })
        }
    }
    
    @IBAction func confirmationButtonAction(_ sender: UIButton) {
        self.confirmSignUp()
    }
    var username: String!
    var email: String!
    var password: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let closeButton = UIBarButtonItem(image: UIImage(icon: .FAClose, size: CGSize(width: 40.0, height: 40.0), orientation: .up, textColor: Common.primaryColor, backgroundColor: Common.backgroundColor), style: .done, target: self, action: #selector(ConfirmViewController.close))
        self.navigationItem.rightBarButtonItem = closeButton
        
        preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - 100)
        
        usernameTextField.text = username
        emailTextField.text = email
        passwordTextField.text = password
        
        self.navigationController?.navigationBar.barTintColor = .white
        
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(named: "BarBackground"), for: .default)
        navigationBar.shadowImage = UIImage()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func signIn() {
        let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Logging in", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        AWSMobileClient.sharedInstance().signIn(username: self.username, password: self.password) { (signInResult, error) in
            if let error = error  {
                print("loginButtonAction: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    Common.showNoticeMessage(title: "", message: "Login error \(error.localizedDescription)", theme: .error, style: .top)
                }
            } else if let signInResult = signInResult {
                switch (signInResult.signInState) {
                case .signedIn:
                    print("User is signed in.")
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                            Common.showOnboard()
                        })
                    }
                case .smsMFA:
                    print("SMS message sent to \(signInResult.codeDetails!.destination!)")
                default:
                    print("Sign In needs info which is not et supported.")
                }
            }
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }
    }
    
    func confirmSignUp() {
        AWSMobileClient.sharedInstance().confirmSignUp(username: self.username, confirmationCode: confirmationTextField.text!) { (signUpResult, error) in
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                            self.signIn()
                        })
                    }
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "You're not verified", theme: .warning, style: .top)
                    }
                case .unknown:
                    print("Unexpected case")
                    DispatchQueue.main.async {
                        Common.showNoticeMessage(title: "", message: "Unknown sign up result", theme: .warning, style: .top)
                    }
                }
            } else if let error = error {
                print("\(error.localizedDescription)")
                DispatchQueue.main.async {
                    Common.showNoticeMessage(title: "", message: "Sign up error \(error)", theme: .warning, style: .top)
                }
            }
        }
    }

}
