//
//  SignInViewController.swift
//  archery
//
//  Created by Kelvin Lee on 11/12/18.
//  Copyright © 2018 Kelvin Lee. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AWSMobileClient
import SwiftMessages
import NVActivityIndicatorView

class SignInViewController: BaseLoginViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBAction func forgotPasswordButtonAction(_ sender: UIButton) {
        let passwordResetViewController = UIStoryboard(name:"Main",bundle: Bundle.main).instantiateViewController(withIdentifier: "PasswordResetViewController") as! PasswordResetViewController
        passwordResetViewController.username = self.usernameTextField.text
        let navigationController = UINavigationController(rootViewController: passwordResetViewController)
        
        let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
        segue.configure(layout: .bottomCard)
        segue.perform()
    }
    
    var fbLoginButton: FBSDKLoginButton!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBAction func loginButtonAction(_ sender: UIButton) {
//        handleUserPoolSignInFlowStart()
        let activityData = ActivityData(size: CGSize(width: 100, height: 100), message: "Logging in", messageFont: UIFont(name: "Avenir", size: 25.0), messageSpacing: 2.0, type: .orbit, color: Common.primaryColor, padding: 1.0, displayTimeThreshold: 1, minimumDisplayTime: 1, backgroundColor: .clear, textColor: .white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        AWSMobileClient.sharedInstance().signIn(username: self.usernameTextField.text!, password: self.passwordTextField.text!) { (signInResult, error) in
            if let error = error  {
                print("loginButtonAction: \(error.localizedDescription)")
                self.showConfirm()
            } else if let signInResult = signInResult {
                switch (signInResult.signInState) {
                case .signedIn:
                    print("User is signed in.")
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                            Common.showOnboard()
//                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                            let rootViewController = appDelegate.window?.rootViewController as! UINavigationController
//                            Common.showMain(mainNavigationController: rootViewController)
                        })
                    }
                case .smsMFA:
                    print("SMS message sent to \(signInResult.codeDetails!.destination!)")
                default:
                    print("Sign In needs info which is not et supported.")
                }
            }
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }

//        AWSCognitoUserPoolsSignInProvider.sharedInstance().setInteractiveAuthDelegate(self)
        
//        AWSSignInManager.sharedInstance().login(signInProviderKey: AWSCognitoUserPoolsSignInProvider.sharedInstance().identityProviderName) { (result, error) in
//            debugPrint("AWSSignInManager result:\(result), error:\(error)")
//        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        fbLoginButton = FBSDKLoginButton(frame: self.facebookButton.frame)
//        fbLoginButton.readPermissions = ["public_profile", "email"]
//        fbLoginButton.delegate = self
//        self.view.addSubview(fbLoginButton)
        self.navigationController?.navigationBar.tintColor = Common.primaryColor
        self.logoImageView.setFAIconWithName(icon: .FALeaf, textColor: Common.primaryColor, orientation: .up, backgroundColor: .black, size: CGSize(width: 100.0, height: 100.0))
        self.facebookButton.isHidden = true
        self.setupKeyboardHandling(textFields: [self.usernameTextField, self.passwordTextField, self.emailTextField], scrollView: self.scrollView)
        
        self.navigationController?.navigationBar.barTintColor = .white
        
        let navigationBar = navigationController!.navigationBar
        navigationBar.setBackgroundImage(UIImage(named: "BarBackground"), for: .default)
        navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.usernameTextField.text = "johnsmarket1"//"test11"
        self.passwordTextField.text = "Test123456"
        self.emailTextField.text = "ummuxirraw-2533@yopmail.com"//"unukabex-8327@yopmail.com"
    }
    
    func showConfirm() {
        DispatchQueue.main.async {
            let confirmViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfirmViewController") as! ConfirmViewController
            if let username = self.usernameTextField.text, let email = self.emailTextField.text, let password = self.passwordTextField.text {
                confirmViewController.username = username
                confirmViewController.email = email
                confirmViewController.password = password
            }
            let navigationController = UINavigationController(rootViewController: confirmViewController)
            let segue = SwiftMessagesSegue(identifier: nil, source: self, destination: navigationController)
            segue.configure(layout: .bottomCard)
            segue.perform()
        }
    }
}

extension SignInViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        debugPrint("login result:\(result.token) :\(result)")
        guard result.token != nil else {
            return
        }
        guard let fbToken = result.token.tokenString else {
            return
        }
        debugPrint("FB login didComplete token:\(fbToken), error:\(error)")
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
            debugPrint("FBSDKGraphRequest conn:\(connection), result:\(result), error:\(error)")
        })
        
        AWSMobileClient.sharedInstance().federatedSignIn(providerName: IdentityProvider.facebook.rawValue, token: fbToken) { (userState, error)  in
            if let error = error {
                print("Federated Sign In failed: \(error.localizedDescription)")
            }
            debugPrint("federatedSignIn:\(userState?.rawValue)")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        debugPrint("FB didLogOut")
    }
}
