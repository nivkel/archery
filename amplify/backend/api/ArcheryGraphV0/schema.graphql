schema {
  query: Query
  mutation: Mutation
  subscription: Subscription
}

type Query {
  getPosts(limit: Int, nextToken: String): PostConnection
  getMessagesForConversation(after: String, conversationId: ID!, first: Int): MessageConnection
	getUserConversation(userId: ID!, postId: ID!): UserConversationsConnection
	me: User
	getUserScore(userId: ID!): User
	getMappingType(userId: ID!, type: String!, filter: String): MappingConnection
	getLikeType(userId: ID!, postId: ID!, type: String!): MappingConnection
	getLikesForPost(postId: ID!, type: String!): MappingConnection
	getLikesForUser(userId: ID!, type: String!): MappingConnection
}

type Mutation {
  addPost(id: ID!, title: String, price: String, condition: String, content: String): Post
  addPostWithFiles(input: CreatePostWithFilesInput!, file: S3ObjectInput, file2: S3ObjectInput, file3: S3ObjectInput, file4: S3ObjectInput): Post
	updateUserPost(input: UpdateUserPostInput!, file: S3ObjectInput, file2: S3ObjectInput, file3: S3ObjectInput, file4: S3ObjectInput): Post
	addConversation(id: ID!): Conversation
	addUserConversation(userId: ID!, conversationId: ID!, postId: ID!, subject: String!, senderName: String, postFileKey: String): UserConversations
	addMessage(id: ID!, conversationId: ID!, content: String, createdAt: String!): Message
	addLikeType(userId: ID!, postId: ID!, type: String!): Mapping
	removeLikeType(id: ID!): Mapping
}

type Subscription {
  addedPost: Post
  @aws_subscribe(mutations: ["addPost"])

	subscribeToNewMessage(conversationId: ID!): Message
	@aws_subscribe(mutations: ["addMessage"])
}

type Mapping @model {
	id: ID!
	userId: ID!
	type: String!
	postId: ID
	post: Post
}

type MappingConnection {
	mappings: [Mapping]
}

type Post @model {
  id: ID!
  title: String
  price: String
	salePrice: String
	location: String
	category: String
  condition: String
  content: String
	tags: String
	isSold: Boolean
	isPublished: Boolean
	createdAt: String
	updatedAt: String
	promotedAt: String
	promotedCount: String
	dateSold: String
	isNegotiable: Boolean
	willShip: Boolean
	willMeet: Boolean
	userId: ID!
	searchField: String
	user: User
	username: String
  file: S3Object
	file2: S3Object # Till AWS supports arrays
	file3: S3Object
	file4: S3Object
	typename: String
}

type PostConnection {
  posts: [Post]
  nextToken: String
}

type S3Object {
  bucket: String!
  key: String!
  region: String!
}

input S3ObjectInput {
  bucket: String!
  key: String!
  region: String!
  localUri: String!
  mimeType: String!
}

input CreatePostWithFilesInput {
  title: String
  price: String
	salePrice: String
	location: String
	category: String
  condition: String
  content: String
	tags: String
	isSold: Boolean
	isPublished: Boolean
	isNegotiable: Boolean
	willShip: Boolean
	willMeet: Boolean
	userId: ID
	username: String # Use this till iOS client supports nested models
}

input UpdateUserPostInput {
	id: ID
	title: String
  price: String
	salePrice: String
	location: String
	category: String
  condition: String
  content: String
	tags: String
	dateSold: String
	promotedAt: String
	promotedCount: String
	isSold: Boolean
	isPublished: Boolean
	isNegotiable: Boolean
	willShip: Boolean
	willMeet: Boolean
}

# Chat Models

type Conversation @model {
	createdAt: String
	id: ID!
	messages(after: String, first: Int): MessageConnection
}

type Message @model {
  #  The author object. Note: `authorId` is only available because we list it in `extraAttributes` in `Conversation.messages`
	author: User
	#  The message content.
	content: String!
	#  The id of the Conversation this message belongs to. This is the table primary key.
	conversationId: ID!
	#  The message timestamp. This is also the table sort key.
	createdAt: String
	#  Generated id for a message -- read-only
	id: ID!
	#  Flag denoting if this message has been accepted by the server or not.
	isSent: Boolean
	recipient: User
	sender: String
	senderName: String
}

type MessageConnection {
	messages: [Message] @connection
	nextToken: String
}

type User @model {
	#  A unique identifier for the user.
	cognitoId: ID!
	#  A user's enrolled Conversations. This is an interesting case. This is an interesting pagination case.
	conversations(after: String, first: Int): UserConversationsConnection
	#  Generated id for a user. read-only
	id: ID!
	#  Get a users messages by querying a GSI on the Messages table.
	messages(after: String, first: Int): MessageConnection
	#  The username
	username: String!
	email: String!
	location: String
	profileImage: String
	# is the user registered?
	verified: Boolean
	phoneVerified: Boolean
	deviceToken: String
	score: String
	storeName: String
	bio: String
	facebookId: String
}

type ConversationsConnection {
	nextToken: String
	conversations: [Conversation] @connection
}

type UserConversations @model {
	# associated: [UserConversations]
	conversation: Conversation @connection
	conversationId: ID!
	user: User
	userProfile: String # Use this till iOS client supports nested models
	username: String # Use this till iOS client supports nested models
	userId: ID!
	post: Post # Nested models doesn't work
	postTitle: String # Use this till iOS client supports nested models
	postFileKey: String
	postPrice: String # Use this till iOS client supports nested models
	postAuthor: String # Use this till iOS client supports nested models
	postIsSold: Boolean # Use this till iOS client supports nested models
	postId: ID!
	subject: String!
	senderName: String
	createdAt: String
}

type UserConversationsConnection {
	nextToken: String
	userConversations: [UserConversations] @connection
}